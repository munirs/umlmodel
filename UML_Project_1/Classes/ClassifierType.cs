﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UML_Project_1.Classes
{
    public class ClassifierType
    {
        public String className { get; set; }
        public String classID { get; set; }
        public ClassifierType(String className, String classID)
        {
            this.className =className;
            this.classID = classID;
        }
    }
}