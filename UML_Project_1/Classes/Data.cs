﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AForge;

using AForge.Imaging;
using AForge.Imaging.Filters;
using AForge.Math.Geometry;

using System.Drawing;
using System.Drawing.Imaging;
using System.Reflection;
using System.Drawing.Drawing2D;


using System.IO;

namespace UML_Project_1.Classes
{
    public class Data
    {
        public Data()
        {

        }
        public string OCR(string fileToOCR)
        {
            String result = null;
            try
            {

                MODI.Document md = new MODI.Document();

                // md.OnOCRProgress += new MODI._IDocumentEvents_OnOCRProgressEventHandler(this);

                md.Create(fileToOCR);


                md.OCR(MODI.MiLANGUAGES.miLANG_ENGLISH, true, true);

                MODI.Image img = (MODI.Image)md.Images[0];

                MODI.Layout layout = img.Layout;

                layout = img.Layout;

                result = layout.Text;
                // md.OnOCRProgress += new MODI._IDocumentEvents_OnOCRProgressEventHandler(this.ShowProgress);
                md.Close(false);


            }
            catch (Exception e)
            {
                //MessageBox.Show(e.Message);
                result = null;
            }
            return result;
        }

        public String CropImageRectangle(System.Drawing.Image cropimg, int cropx, int cropy, int cropwidth, int cropheight, String strPath)
        {
            String tempfile = null;
            System.Drawing.Image img1;
            // crop image and extract text
            // int cropx = Math.Min(corners[0].X, corners[2].X);
            // int cropy = Math.Min(corners[0].Y, corners[2].Y);
            // int cropwidth = Math.Abs((corners[0].X - corners[1].X));
            // int cropheight = Math.Abs((corners[0].Y - corners[3].Y));
            Bitmap source = new Bitmap(cropimg);// pictureBox.Image);//@"C:\tulips.jpg"
            // for (int i = 0; i < 300; i = i + 100)

            Rectangle section = new Rectangle(new System.Drawing.Point(cropx, cropy), new Size(cropwidth, cropheight));

            Bitmap CroppedImage = CropImage(source, section);
            img1 = CroppedImage;
            //strPath = //@"C:\Users\Munir\Documents\Visual Studio 2012\Projects\UML_Project_1\UML_Project_1\munir11.jpg";
            img1 = ScaleCropImage(img1);
            tempfile = SaveImageWeb(img1, strPath);//SaveImage(img1);
            // pictureBox3.Update();
            ////Text = Text + " \n " + data.OCR(tempfile); // ocr text
            // MessageBox.Show(OCR(tempfile));
            //strPath = "";
            return tempfile;
            // end crop image
        }

        public Bitmap CropImage(Bitmap source, Rectangle section)
        {
            // An empty bitmap which will hold the cropped image
            Bitmap bmp = new Bitmap(section.Width, section.Height);

            Graphics g = Graphics.FromImage(bmp);

            // Draw the given area (section) of the source image
            // at location 0,0 on the empty bitmap (bmp)
            g.DrawImage(source, 0, 0, section, GraphicsUnit.Pixel);
            // g.Dispose();
            return bmp;
        }

        public System.Drawing.Image ScaleCropImage(System.Drawing.Image img)
        {


            /*  System.Drawing.Image im = ScaleByPercent(img, 200);//250
              Bitmap image = new Bitmap(img, new Size(im.Width, im.Height));//pictureBox2.Size.Width, pictureBox2.Size.Height));


              // create filter
             BilateralSmoothing filter = new BilateralSmoothing();
              filter.KernelSize = 3;//7// Kernel Size (n) - this is an odd number > 1 (3, 5, 7, ...). It limits the set of pixels which take part in computation. 
              filter.SpatialFactor = 5;//10 Spatial Factor - the greater the value is the more blurry effect you'll get in domains, i.e. in areas of the same color.
              filter.ColorFactor = 20;//60 Color Factor - the greater the number is, the greater are the domains and less contrast edges will get blurred.
              filter.ColorPower = 0.5;
              // apply the filter
              filter.ApplyInPlace(image);

              Sharpen filters = new Sharpen();
              // apply the filter
              filters.ApplyInPlace(image);


              return (System.Drawing.Image)image;*/
            /////////// henry
            ConservativeSmoothing conSmoothing = new ConservativeSmoothing();
            AForge.Imaging.Filters.GaussianSharpen gauSharpen = new AForge.Imaging.Filters.GaussianSharpen();
            /////

            System.Drawing.Image im = ScaleByPercent(img, 247);//250
            Bitmap image = new Bitmap(img, new Size(im.Width, im.Height));//pictureBox2.Size.Width, pictureBox2.Size.Height));

            //MessageBox.Show(im.Width.s() +" ="+im.Height.ToString());
            //// henry
            Grayscale filter5 = new Grayscale(0.2125, 0.7154, 0.0721);
            Bitmap grayImage = filter5.Apply(image);
            AForge.Imaging.Filters.Threshold threshold = new AForge.Imaging.Filters.Threshold(140);
            image = threshold.Apply(grayImage);
            //////
            // create filter
            BilateralSmoothing filter = new BilateralSmoothing();
            filter.KernelSize = 3;//3// Kernel Size (n) - this is an odd number > 1 (3, 5, 7, ...). It limits the set of pixels which take part in computation. 
            filter.SpatialFactor = 2;//5 Spatial Factor - the greater the value is the more blurry effect you'll get in domains, i.e. in areas of the same color.
            filter.ColorFactor = 25;//20 Color Factor - the greater the number is, the greater are the domains and less contrast edges will get blurred.
            filter.ColorPower = 0.1;//.5
            // apply the filter
            // filter.ApplyInPlace(image);
            /////////
            for (int f = 0; f <= 2; f++)
            {
                //   image = gauSharpen.Apply(image);
                filter.ApplyInPlace(image);
                // image = gauSharpen.Apply(image);
                // conSmoothing.ApplyInPlace(image);
            }
            ////////
            // Sharpen filters = new Sharpen();
            // apply the filter
            //filters.ApplyInPlace(image);
            //////////////////
            for (int s = 0; s <= 5; s++)
            {
                //conSmoothing.ApplyInPlace(image);
                image = gauSharpen.Apply(image);/////////////////////////////////
                //PictureBox1.Image = newImage_Shape01;
                //PictureBox1.Update();
                // 'newImage_Shape01 = filter6.Apply(newImage_Shape01)
            }
            ///////////////////
            return (System.Drawing.Image)image;
        }

        static System.Drawing.Image ScaleByPercent(System.Drawing.Image imgPhoto, int Percent)
        {
            float nPercent = ((float)Percent / 100);

            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            int sourceX = 0;
            int sourceY = 0;

            int destX = 0;
            int destY = 0;
            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap bmPhoto = new Bitmap(destWidth, destHeight,
                                     PixelFormat.Format24bppRgb);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution,
                                    imgPhoto.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(imgPhoto,
                new Rectangle(destX, destY, destWidth, destHeight),
                new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);

            grPhoto.Dispose();
            return bmPhoto;
        }

        public String SaveImageWeb(System.Drawing.Image imagefile, String strPath)
        {
            Data data = new Data();
            string file = null;
            MemoryStream stream = new MemoryStream();
            byte[] image = data.imageToByteArray(imagefile);
            stream.Write(image, 0, image.Length);
            Bitmap bitmap = new Bitmap(stream);

            //string root = Server.MapPath(filename + ".jpg");
            string folder = strPath.Remove(strPath.LastIndexOf('\\') + 1);
            string filename = strPath.Substring(strPath.LastIndexOf('\\') + 1);
            string ab = data.GetUniqueFilename(folder, filename);
            bitmap.Save(folder + ab);//@"C:\Users\Munir\Documents\Visual Studio 2012\Projects\UML_Project_1\UML_Project_1\munir11.jpg");
            bitmap.Dispose();
            file = folder + ab;// strPath;//@"C:\Users\Munir\Documents\Visual Studio 2012\Projects\UML_Project_1\UML_Project_1\munir11.jpg";*/
            /*Directory.CreateDirectory("images");
            string path = Path.Combine(Environment.CurrentDirectory, @"images\image.jpg");
           //imagefile.(path, System.Drawing.Imaging.ImageFormat.Jpeg);
            file = @"images\image.jpg";
            */

            //DELETE AFTER 
            // System.IO.File.Delete(folder+ab);

            return file;
            /*Directory.CreateDirectory("images");
           string path = Path.Combine(Environment.CurrentDirectory, @"images\image.jpg"; 
           theImage.Save(path, System.Drawing.Imaging.ImageFormat.Jpeg);?
           */


        }

        public byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            byte[] byteArray = new byte[0];
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            ms.Close();
            byteArray = ms.ToArray();
            return byteArray;
        }
        public string GetUniqueFilename(string folder, string postedFileName)
        {
            // string postedFileName = UMLFileUpload.PostedFile.FileName;
            //string fileExtension = postedFileName.Substring(postedFileName.LastIndexOf('.') + 1);

            string fileExtension = Path.GetExtension(postedFileName);
            int index = 2;

            while (File.Exists(string.Format("{0}\\{1}", folder, postedFileName)))
            {
                if (index == 2)
                    postedFileName =
                        string.Format("{0} ({1}).{2}",
                                      postedFileName.Substring(0, postedFileName.LastIndexOf('.')),
                                      index,
                                      fileExtension);
                else
                    postedFileName =
                        string.Format("{0} ({1}).{2}",
                                      postedFileName.Substring(0, postedFileName.LastIndexOf(' ')),
                                      index,
                                      fileExtension);
                index++;
            }

            return postedFileName;
        }
        public static Boolean IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                //Don't change FileAccess to ReadWrite, 
                //because if a file is in readOnly, it fails.
                stream = file.Open
                (
                    FileMode.Open,
                    FileAccess.Read,
                    FileShare.None
                );
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }
        /// This function is used to delete all files inside a folder 
        public void CleanFiles(String strPath)
        {

            if (Directory.Exists(strPath))
            {
                //System.IO.File.Create(FullFName).Close();
                var directory = new DirectoryInfo(strPath);
                foreach (FileInfo file in directory.GetFiles())
                {
                    if (!IsFileLocked(file)) file.Delete();
                }
            }
        }

        public Boolean  SearchSubstring(string orginStr, string substr) {

            bool match ;

            for (int i = 0; i < orginStr.Length - substr.Length + 1; ++i)
            {
                match = true;
                for (int j = 0; j < substr.Length; ++j)
                {
                    if (orginStr[i + j] != substr[j])
                    {
                        match = false;
                        break;
                    }
                }
                if (match) return true;
            }

            return false;
        }

    }
}