﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AForge;

using AForge.Imaging;
using AForge.Imaging.Filters;
using AForge.Math.Geometry;

using System.Reflection;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;

namespace UML_Project_1.Classes
{
    public class Detect_H_Dashed_Line
    {
        System.Drawing.Image image1;
        System.Drawing.Image image11;
        Bitmap newImage103;
        protected string strPath { get; set; }

        public Detect_H_Dashed_Line(String strPath)//System.Drawing.Image image1, System.Drawing.Image image11, Bitmap newImage103
        {
            this.strPath = strPath;
        }
        public void detect_H_Dash_Line()
        {
            image1 = System.Drawing.Image.FromFile(strPath);
            image11 = System.Drawing.Image.FromFile(strPath);
            //Set_H_Solid_Line();
            //Set_H_Dash_Line();
            Bitmap newImage = new Bitmap(image1);
            newImage103 = new Bitmap(image11);
            dynamic P1 = default(System.Drawing.Point);
            dynamic p2 = default(System.Drawing.Point);
            System.Drawing.Point P3 = new System.Drawing.Point();

            string str = "";
            Graphics graphics = Graphics.FromImage(newImage);
            Graphics graphics1 = Graphics.FromImage(newImage103);
            SolidBrush brush = new SolidBrush(Color.Blue);
            Pen pen = new Pen(brush);
            Pen pen1 = new Pen(brush, 5);

            for (int i = 0; i <= (newImage.Width - 1); i++)
            {
                for (int j = 0; j <= (newImage.Height - 1); j++)
                {
                    if (newImage.GetPixel(i, j).Name == "ffffffff")
                    {
                        //Boolean endOfLine = false;
                        //int position = i + 1;
                        for (int k = i + 1; k <= (newImage.Width - 1); k++)
                        {
                            //Boolean endOfLine = false;
                            //If input_Matrix_check <= 495 Then
                            if ((newImage.GetPixel(k, j).Name != "ffffffff" && newImage.GetPixel(k + 1, j).Name != "ffffffff" && newImage.GetPixel(k + 2, j).Name != "ffffffff" && newImage.GetPixel(k + 4, j).Name != "ffffffff") || (k == newImage.Width - 1 && newImage.GetPixel(k, j).Name == "ffffffff"))
                            {


                                if (k > i + 50 && isThisLine(i, j, k, newImage) == true)
                                {
                                    P1.X = i;
                                    P1.Y = j;
                                    p2.X = k - 1;
                                    //'p2.X = k
                                    p2.Y = j;

                                    //'####################### Line Matrix ###################

                                    //line_Matrix(input_Line_Matrix, 0) = P1.X
                                    //line_Matrix(input_Line_Matrix, 1) = P1.Y
                                    //line_Matrix(input_Line_Matrix, 2) = p2.X
                                    //line_Matrix(input_Line_Matrix, 3) = p2.Y
                                    //line_Matrix(input_Line_Matrix, 4) = 0 '' 0 means stright line, and 1 is dash line
                                    //input_Line_Matrix += 1

                                    //'####################### Line Matrix ###################


                                    graphics.DrawLine(pen1, P1, p2);
                                    graphics1.DrawLine(pen1, P1, p2);
                                    image1 = newImage;
                                    ////PictureBox1.Image = newImage;
                                    ////PictureBox1.Update();
                                    break; // TODO: might not be correct. Was : Exit For

                                }
                                else
                                {
                                    if (k > i + 10 && isThisLine(i, j, k, newImage) == true)
                                    {
                                        bool check_small_h_Line = false;
                                        check_small_h_Line = true;
                                        for (int a = i; a <= k - 1; a++)
                                        {
                                            if (newImage.GetPixel(a, j).Name != "ffffffff")
                                            {
                                                check_small_h_Line = false;
                                                break; // TODO: might not be correct. Was : Exit For
                                            }

                                        }

                                        dynamic d = 0;
                                        //' to ensure that i-4 not < 0
                                        if (i - 4 < 0)
                                            d = i;
                                        else
                                            d = i - 4;
                                        if (check_small_h_Line & newImage.GetPixel(d, j).Name == "ff000000" & newImage.GetPixel(k, j).Name == "ff000000")
                                            check_small_h_Line = false;

                                        if (check_small_h_Line)
                                        {
                                            //'MsgBox(newImage.GetPixel(i - 2, j).Name.ToString + "+++++" + newImage.GetPixel(k, j).Name.ToString)

                                            //line_Matrix_check(input_Matrix_check, 0) = i
                                            //line_Matrix_check(input_Matrix_check, 1) = j
                                            //line_Matrix_check(input_Matrix_check, 2) = k - 1
                                            //line_Matrix_check(input_Matrix_check, 3) = j
                                            //line_Matrix_check(input_Matrix_check, 4) = 0 '' 0 means stright line, and 1 is dash line
                                            //input_Matrix_check += 1
                                        }
                                    }

                                    break; // TODO: might not be correct. Was : Exit For
                                }


                            }
                            //'End If
                        }

                    }
                    //'If newImage.GetPixel(i, j).Name = "ffffffff" Then newImage.SetPixel(i, j, Color.Red)
                    //'If newImage.GetPixel(i, j).Name <> "ffffffff" Then
                    //'If newImage.GetPixel(i, j).Name <> "ff000000" Then
                    //'str = str + Environment.NewLine + newImage.GetPixel(i, j).Name.ToString
                    ///'Exit For
                    //'End If


                }
            }
            //'TextBox3.Text = str
            image1 = newImage;
            image11 = newImage103;
            ////PictureBox1.Image = newImage;
            ////PictureBox1.Update();
            ////PictureBox11.Image = newImage103;
            ////PictureBox11.Update();
            //PictureBox1.Update();
            //PictureBox11.Update();
            //  Remove();
            //remove_Boxes();

        }

        private void Set_H_Dash_Line()
        {

            Bitmap newImg = new Bitmap(image1);


            AForge.Imaging.Filters.Grayscale filter = new AForge.Imaging.Filters.Grayscale(0.11, 0.59, 0.3);
            newImg = filter.Apply(newImg);

            ////PictureBox1.Image = newImg;
            ////PictureBox1.Update();


            AForge.Imaging.Filters.Threshold filter4 = new AForge.Imaging.Filters.Threshold(210);
            //'30.10.2012 ''Update: back to active it to detect dash lines
            newImg = filter4.Apply(newImg);
            //'30.10.2012 '' back to active it to detect dash lines

            ////PictureBox1.Image = newImg;
            ////PictureBox1.Update();

            AForge.Imaging.Filters.SobelEdgeDetector filter1 = new AForge.Imaging.Filters.SobelEdgeDetector();
            newImg = filter1.Apply(newImg);


            // Dilatation filter2 = new Dilatation();
            // newImg = filter2.Apply(newImg);

            ////PictureBox1.Image = newImg;
            ////PictureBox1.Update();
            Dilatation filter2 = new Dilatation();
            for (int d = 0; d < 3; d++)
            {
                newImg = filter2.Apply(newImg);
            }

            image1 = newImg;
            ////PictureBox1.Image = newImg;
            ////PictureBox1.Update();


            //'################################################################################################
            //'################################################################################################

            Bitmap newImage8 = new Bitmap(image1);

            //'TextBox3.Text = ""
            for (int i = 0; i <= (newImage8.Width - 1); i++)
            {
                for (int j = 0; j <= (newImage8.Height - 1); j++)
                {
                    if (newImage8.GetPixel(i, j).Name != "ffffffff")
                        newImage8.SetPixel(i, j, Color.Black);
                    //'If newImage.GetPixel(i, j).Name = "ffffffff" Then newImage.SetPixel(i, j, Color.Red)
                    //'If newImage.GetPixel(i, j).Name <> "ffffffff" Then
                    //'TextBox1.Text = newImage.GetPixel(i, j).Name
                    //'End If

                }
            }
            ////PictureBox1.Image = newImage8;
            ////PictureBox1.Update();
            Bitmap newImage2 = new Bitmap(newImage8);
            AForge.Imaging.Filters.Grayscale filter8 = new AForge.Imaging.Filters.Grayscale(0.11, 0.59, 0.3);
            newImage2 = filter8.Apply(newImage2);
            //'Dim filter4 As Filters.CannyEdgeDetector = New Filters.CannyEdgeDetector()
            //'newImage2 = filter4.Apply(newImage2)
            //'######################################################################################
            //'######################################################################################

            Erosion filter3 = new Erosion();

            //newImage2 = filter3.Apply(newImage2);
            //newImage2 = filter3.Apply(newImage2);

            for (int e = 0; e < 2; e++)
            {
                newImage2 = filter3.Apply(newImage2);
            }


            //'Dim filter4 As Opening = New Opening()
            //'newImage2 = filter4.Apply(newImage2)

            image1 = newImage2;
            ////PictureBox1.Image = newImage2;
            ////PictureBox1.Update();

            Bitmap newImage9 = new Bitmap(image1);

            //'TextBox3.Text = ""


            for (int i = 0; i <= (newImage9.Width - 1); i++)
            {
                for (int j = 0; j <= (newImage9.Height - 1); j++)
                {
                    if (newImage9.GetPixel(i, j).Name != "ff000000")
                        newImage9.SetPixel(i, j, Color.White);
                    //'If newImage.GetPixel(i, j).Name = "ffffffff" Then newImage.SetPixel(i, j, Color.Red)
                    //'If newImage.GetPixel(i, j).Name <> "ffffffff" Then
                    //'TextBox1.Text = newImage.GetPixel(i, j).Name
                    //'End If

                }
            }



            image1 = newImage9;
            ////PictureBox1.Image = newImage9;
            ////PictureBox1.Update();
            //newImage102 = newImage9


            //'$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ 16.01.2013
            //'$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ 
        }

        public Boolean isThisLine(int i, int j, int k, Bitmap newImage)
        {
            int wPixel = 0;
            int wPixelSec = 0;
            int bPixel = 0;
            int bPixelSec = 0;
            int b = 0;
            int c = 0;
            int cr = 0;
            int e = 0;
            int eSec = 0;
            int eThird = 0;

            int wPixel_01 = 0;
            int wPixel_01Sec = 0;
            int bPixel_01 = 0;
            int bPixel_01Sec = 0;
            int b01 = 0;
            int e01 = 0;
            int e01Sec = 0;
            int e01Third = 0;
            int startP = 0;
            int endP = 0;
            int fromWhite = 0;
            int toWhite = 0;
            int sumB = 0;
            int sumW = 0;
            int countW = 0;
            int countWR = 0;
            int maxCount = 0;
            Boolean allSame = true;
            Boolean allSameR = true;
            Boolean isLine = false;

            for (b = i; b < k; b++)
            {
                if (newImage.GetPixel(b, j).Name == "ffffffff")
                {
                    wPixel = wPixel + 1;

                }
                else
                {
                    for (e = b; e < k; e++)
                    {
                        if (newImage.GetPixel(e, j).Name != "ffffffff")
                        {
                            bPixel = bPixel + 1;

                        }
                        else
                        {
                            fromWhite = e;
                            for (eSec = e; eSec < k; eSec++)
                            {
                                if (newImage.GetPixel(eSec, j).Name == "ffffffff")
                                {
                                    wPixelSec = wPixelSec + 1;

                                }
                                else
                                {
                                    startP = eSec;
                                    for (eThird = eSec; eThird < k; eThird++)
                                    {
                                        if (newImage.GetPixel(eThird, j).Name != "ffffffff")
                                        {
                                            bPixelSec = bPixelSec + 1;

                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                            break;////
                        }
                    }
                    break;
                }
            }

            for (b01 = k - 1; b01 >= b; b01--)
            {
                if (newImage.GetPixel(b01, j).Name == "ffffffff")
                {
                    wPixel_01 = wPixel_01 + 1;

                }
                else
                {
                    for (e01 = b01; e01 >= e; e01--)
                    {
                        if (newImage.GetPixel(e01, j).Name != "ffffffff")
                        {
                            bPixel_01 = bPixel_01 + 1;

                        }
                        else
                        {
                            toWhite = e01;
                            for (e01Sec = e01; e01Sec >= eSec; e01Sec--)
                            {
                                if (newImage.GetPixel(e01Sec, j).Name == "ffffffff")
                                {
                                    wPixel_01Sec = wPixel_01Sec + 1;

                                }
                                else
                                {
                                    endP = e01Sec;
                                    for (e01Third = e01Sec; e01Third >= i; e01Third--)
                                    {
                                        if (newImage.GetPixel(e01Third, j).Name != "ffffffff")
                                        {
                                            bPixel_01Sec = bPixel_01Sec + 1;

                                        }
                                        else
                                        {
                                            break;
                                        }

                                    }
                                    break;
                                }

                            }
                            break;//////
                        }
                    }
                    break;
                }
            }


            for (c = fromWhite; c <= toWhite; c++)
            {
                if (newImage.GetPixel(c, j).Name == "ffffffff")
                {
                    countW = countW + 1;

                }
                else
                {

                    if (wPixelSec == countW)
                    {
                        // maxCount = maxCount + 1;
                        for (int temp = c; temp <= toWhite; temp++)
                        {
                            if (newImage.GetPixel(temp, j).Name == "ffffffff")
                            {
                                countW = 0;
                                c = temp;
                                break;
                            }
                        }


                    }
                    else
                    {
                        // MessageBox.Show("count " +countW+" = "+wPixelSec);
                        allSame = false;
                        countW = 0;
                        break;
                    }

                }
                if (c > (toWhite - fromWhite) / 4)
                {
                    break;
                }
            }

            for (cr = toWhite; cr >= fromWhite; cr--)
            {
                if (newImage.GetPixel(cr, j).Name == "ffffffff")
                {
                    countWR = countWR + 1;

                }
                else
                {

                    if (wPixel_01Sec == countWR)
                    {
                        // maxCount = maxCount + 1;
                        for (int tempR = cr; tempR >= fromWhite; tempR--)
                        {
                            if (newImage.GetPixel(tempR, j).Name == "ffffffff")
                            {
                                countWR = 0;
                                cr = tempR;
                                break;
                            }
                        }


                    }
                    else
                    {
                        // MessageBox.Show("count " +countWR+" = "+wPixel_01Sec);
                        allSameR = true;// this is a problem to be fixed @henry
                        countWR = 0;
                        break;
                    }

                }
                if (cr < (toWhite - (toWhite - fromWhite) / 4))
                {
                    break;
                }
            }

            //MessageBox.Show("count " +wPixelSec+"="+wPixel_01Sec+"and "+wPixel+"="+wPixel_01);
            if (bPixel == 0 || bPixel_01 == 0 || wPixelSec == 0 || wPixel_01Sec == 0)
            {

            }
            else if (wPixelSec == wPixel_01Sec && bPixelSec == bPixel_01Sec && bPixel == bPixel_01 && wPixelSec + bPixel + bPixelSec == wPixel_01Sec + bPixel_01 + bPixel_01Sec && allSame == true && allSameR == true)
            {
                isLine = true;
                // MessageBox.Show("count " + newImage.GetPixel(b, j - 1).Name + "-" + wPixel + "-" + wPixel01 +"isLine "+isLine);
            }

            return isLine;
        }

        public String SaveDrawnImage()
        {
            Data data = new Data();
            string file = null;


            MemoryStream stream = new MemoryStream();
            byte[] image = data.imageToByteArray(image11);
            stream.Write(image, 0, image.Length);
            Bitmap bitmap = new Bitmap(stream);

            //string root = Server.MapPath(filename + ".jpg");
            string folder = strPath.Remove(strPath.LastIndexOf('\\') + 1);
            string filename = "alu.jpg";//strPath.Substring(strPath.LastIndexOf('\\') + 1);
            //string ab = GetUniqueFilename(folder, filename);
            bitmap.Save(folder + filename);//@"C:\Users\Munir\Documents\Visual Studio 2012\Projects\UML_Project_1\UML_Project_1\munir11.jpg");
            bitmap.Dispose();
            file = strPath;//@"C:\Users\Munir\Documents\Visual Studio 2012\Projects\UML_Project_1\UML_Project_1\munir11.jpg";*/
            /*Directory.CreateDirectory("images");
            string path = Path.Combine(Environment.CurrentDirectory, @"images\image.jpg");
           //imagefile.(path, System.Drawing.Imaging.ImageFormat.Jpeg);
            file = @"images\image.jpg";
            */
            return file;
            /*Directory.CreateDirectory("images");
           string path = Path.Combine(Environment.CurrentDirectory, @"images\image.jpg"; 
           theImage.Save(path, System.Drawing.Imaging.ImageFormat.Jpeg);?
           */


        }

    }
}