﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AForge;

using AForge.Imaging;
using AForge.Imaging.Filters;
using AForge.Math.Geometry;

using System.Reflection;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;

namespace UML_Project_1.Classes
{
    public class Detect_H_Solid_Line
    {
        public System.Drawing.Image image1;
        public System.Drawing.Image image11;
        Bitmap newImage103;
        public string strPath { get; set; }

        public Detect_H_Solid_Line(String strPath)//System.Drawing.Image image1, System.Drawing.Image image11, Bitmap newImage103
        {
            this.strPath = strPath;
        }

        public void check_arrow()
        {
            image1 = System.Drawing.Image.FromFile(strPath);
            image11 = System.Drawing.Image.FromFile(strPath);

            /*System.Drawing.Image img = PictureBox1.Image;
            PictureBox1.Image = ScaleByPercent(img, 200);
            PictureBox1.Update();*/
            Set_H_Solid_Line();

            Bitmap newImage = new Bitmap(image1);//PictureBox1.Image
            newImage103 = new Bitmap(image11);//PictureBox11.Image
            dynamic P1 = default(System.Drawing.Point);
            dynamic p2 = default(System.Drawing.Point);
            System.Drawing.Point P3 = new System.Drawing.Point();

            //'TextBox3.Text = ""

            string str = "";
            Graphics graphics = Graphics.FromImage(newImage);
            Graphics graphics1 = Graphics.FromImage(newImage103);
            SolidBrush brush = new SolidBrush(Color.Red);
            Pen pen = new Pen(brush);
            Pen pen1 = new Pen(brush, 5);

            ///bool checkonce = false;

            for (int i = 0; i <= (newImage.Width - 1); i++)
            {
                for (int j = 0; j <= (newImage.Height - 1); j++)
                {
                    if (newImage.GetPixel(i, j).Name == "ffffffff")
                    {
                        for (int k = i + 1; k <= (newImage.Width - 1); k++)
                        {

                            //If input_Matrix_check <= 495 Then
                            if (newImage.GetPixel(k, j).Name != "ffffffff" | (k == newImage.Width - 1 & newImage.GetPixel(k, j).Name == "ffffffff"))
                            {
                                if (k > i + 100)
                                {
                                    P1.X = i;
                                    P1.Y = j;
                                    p2.X = k - 1;
                                    //'p2.X = k
                                    p2.Y = j;

                                    // draw the solid arrow
                                    //'####################### Line Matrix ###################
                                    for (int m = k; m < k + 15; m++)
                                    {
                                        bool trU = false;
                                        bool trL = false;
                                        int tempU = 0;
                                        int tempL = 0;
                                        for (int n = j - 15; j - 15 > 0 && n < j; n++)
                                        {
                                            if (newImage.GetPixel(m, n).Name == "ffffffff")
                                            {
                                                //newImage.SetPixel(m, n, Color.Green);
                                                trU = true;
                                                tempU = n;
                                            }
                                            else
                                                newImage.SetPixel(m, n, Color.Black);
                                        }
                                        for (int n = j + 1; n < j + 15; n++)
                                        {
                                            if (newImage.GetPixel(m, n).Name == "ffffffff")
                                            {
                                                //newImage.SetPixel(m, n, Color.Green);
                                                trL = true;
                                                tempL = n;
                                            }
                                            else
                                                newImage.SetPixel(m, n, Color.Black);
                                        }
                                        if (trU && trL)
                                        {
                                            newImage.SetPixel(m, tempU, Color.Green);
                                            newImage.SetPixel(m, tempL, Color.Green);
                                            newImage.SetPixel(m - 1, tempU - 1, Color.Green);
                                            newImage.SetPixel(m - 1, tempL - 1, Color.Green);
                                            newImage.SetPixel(m + 1, tempU + 1, Color.Green);
                                            newImage.SetPixel(m + 1, tempL + 1, Color.Green);
                                        }

                                    }
                                    // end of arrow

                                    // start/single arrow
                                    for (int m = i + 2; m < i + 15; m++)
                                    {
                                        bool trU = false;
                                        bool trL = false;
                                        int tempU = 0;
                                        int tempL = 0;
                                        for (int n = j - 15; j - 15 > 0 && n < j; n++)
                                        {
                                            if (newImage.GetPixel(m, n).Name == "ffffffff")
                                            {
                                                // newImage.SetPixel(m, n, Color.Yellow);
                                                trU = true;
                                                tempU = n;
                                            }
                                            else
                                                newImage.SetPixel(m, n, Color.Black);
                                        }
                                        for (int n = j + 1; n < j + 15; n++)
                                        {
                                            if (newImage.GetPixel(m, n).Name == "ffffffff")
                                            {
                                                //newImage.SetPixel(m, n, Color.Yellow);
                                                trL = true;
                                                tempL = n;
                                            }
                                            else
                                                newImage.SetPixel(m, n, Color.Black);
                                        }
                                        if (trU && trL)
                                        {
                                            for (int l = 0; l < 10; l++)
                                            {
                                                newImage.SetPixel(m, tempU + l, Color.Yellow);
                                                newImage.SetPixel(m, tempL + l, Color.Yellow);
                                                /* newImage.SetPixel(m - 1, tempU - 1+l, Color.Yellow);
                                                 newImage.SetPixel(m - 1, tempL - 1+l, Color.Yellow);
                                                 newImage.SetPixel(m + 1, tempU + 1, Color.Yellow);
                                                 newImage.SetPixel(m + 1, tempL + 1, Color.Yellow);*/
                                            }
                                        }

                                    }

                                    graphics.DrawLine(pen1, P1, p2);
                                    graphics1.DrawLine(pen1, P1, p2);
                                    image1 = newImage;////PictureBox1.Image = newImage;
                                    ////PictureBox1.Update(); //Will never be displayed

                                    break; // TODO: might not be correct. Was : Exit For
                                }
                                else
                                {
                                    if (k > i + 10)
                                    {
                                        bool check_small_h_Line = false;
                                        check_small_h_Line = true;
                                        for (int a = i; a <= k - 1; a++)
                                        {
                                            if (newImage.GetPixel(a, j).Name != "ffffffff")
                                            {
                                                check_small_h_Line = false;
                                                break; // TODO: might not be correct. Was : Exit For
                                            }

                                        }

                                        dynamic d = 0;
                                        //' to ensure that i-4 not < 0
                                        if (i - 4 < 0)
                                            d = i;
                                        else
                                            d = i - 4;
                                        if (check_small_h_Line & newImage.GetPixel(d, j).Name == "ff000000" & newImage.GetPixel(k, j).Name == "ff000000")
                                            check_small_h_Line = false;

                                        if (check_small_h_Line)
                                        {
                                            //'MsgBox(newImage.GetPixel(i - 2, j).Name.ToString + "+++++" + newImage.GetPixel(k, j).Name.ToString)

                                            //line_Matrix_check(input_Matrix_check, 0) = i
                                            //line_Matrix_check(input_Matrix_check, 1) = j
                                            //line_Matrix_check(input_Matrix_check, 2) = k - 1
                                            //line_Matrix_check(input_Matrix_check, 3) = j
                                            //line_Matrix_check(input_Matrix_check, 4) = 0 '' 0 means stright line, and 1 is dash line
                                            //input_Matrix_check += 1
                                        }
                                    }

                                    break; // TODO: might not be correct. Was : Exit For
                                }


                            }
                            //'End If
                        }

                    }
                    //'If newImage.GetPixel(i, j).Name = "ffffffff" Then newImage.SetPixel(i, j, Color.Red)
                    //'If newImage.GetPixel(i, j).Name <> "ffffffff" Then
                    //'If newImage.GetPixel(i, j).Name <> "ff000000" Then
                    //'str = str + Environment.NewLine + newImage.GetPixel(i, j).Name.ToString
                    ///'Exit For
                    //'End If


                }
            }
            //'TextBox3.Text = str
            image1 = newImage;////PictureBox1.Image = newImage;
            ////PictureBox1.Update();
            image11 = newImage103;////PictureBox11.Image = newImage103;
            ////PictureBox11.Update();// To be updated in the final stage
            remove_H_Solid_Line();
            //Remove();
            //remove_Boxes();
        }
        public void Set_H_Solid_Line()
        {

            Bitmap newImg = new Bitmap(image1);////PictureBox1.Image


            AForge.Imaging.Filters.Grayscale filter = new AForge.Imaging.Filters.Grayscale(0.11, 0.59, 0.3);
            newImg = filter.Apply(newImg);

            image1 = newImg;////PictureBox1.Image = newImg;
            ////PictureBox1.Update();


            AForge.Imaging.Filters.Threshold filter4 = new AForge.Imaging.Filters.Threshold(1);//////210
            //'30.10.2012 ''Update: back to active it to detect dash lines
            newImg = filter4.Apply(newImg);
            //'30.10.2012 '' back to active it to detect dash lines

            image1 = newImg;////PictureBox1.Image = newImg;
            ////PictureBox1.Update();

            AForge.Imaging.Filters.SobelEdgeDetector filter1 = new AForge.Imaging.Filters.SobelEdgeDetector();
            newImg = filter1.Apply(newImg);

            image1 = newImg;////PictureBox1.Image = newImg;
            ////PictureBox1.Update();
            /*
                        Dilatation filter2 = new Dilatation();
                        newImg = filter2.Apply(newImg);

                        PictureBox1.Image = newImg;
                        PictureBox1.Update();
             */

            //'################################################################################################
            //'################################################################################################

            Bitmap newImage8 = new Bitmap(image1);////PictureBox1.Image

            //'TextBox3.Text = ""
            for (int i = 0; i <= (newImage8.Width - 1); i++)
            {
                for (int j = 0; j <= (newImage8.Height - 1); j++)
                {
                    if (newImage8.GetPixel(i, j).Name != "ffffffff")
                        newImage8.SetPixel(i, j, Color.Black);
                    //'If newImage.GetPixel(i, j).Name = "ffffffff" Then newImage.SetPixel(i, j, Color.Red)
                    //'If newImage.GetPixel(i, j).Name <> "ffffffff" Then
                    //'TextBox1.Text = newImage.GetPixel(i, j).Name
                    //'End If

                }
            }
            image1 = newImage8;////PictureBox1.Image = newImage8;
            ////PictureBox1.Update();
            Bitmap newImage2 = new Bitmap(newImage8);
            AForge.Imaging.Filters.Grayscale filter8 = new AForge.Imaging.Filters.Grayscale(0.11, 0.59, 0.3);
            newImage2 = filter8.Apply(newImage2);
            //'Dim filter4 As Filters.CannyEdgeDetector = New Filters.CannyEdgeDetector()
            //'newImage2 = filter4.Apply(newImage2)
            //'######################################################################################
            //'######################################################################################

            /*
             Erosion filter3 = new Erosion();
             newImage2 = filter3.Apply(newImage2);
             newImage2 = filter3.Apply(newImage2);
             */

            //'Dim filter4 As Opening = New Opening()
            //'newImage2 = filter4.Apply(newImage2)

            image1 = newImage2;////PictureBox1.Image = newImage2;
            ////PictureBox1.Update();

            Bitmap newImage9 = new Bitmap(image1);//PictureBox1.Image

            //'TextBox3.Text = ""


            for (int i = 0; i <= (newImage9.Width - 1); i++)
            {
                for (int j = 0; j <= (newImage9.Height - 1); j++)
                {
                    if (newImage9.GetPixel(i, j).Name != "ff000000")
                        newImage9.SetPixel(i, j, Color.White);
                    //'If newImage.GetPixel(i, j).Name = "ffffffff" Then newImage.SetPixel(i, j, Color.Red)
                    //'If newImage.GetPixel(i, j).Name <> "ffffffff" Then
                    //'TextBox1.Text = newImage.GetPixel(i, j).Name
                    //'End If

                }
            }




            image1 = newImage9;////PictureBox1.Image = newImage9;
            ////PictureBox1.Update();
            //newImage102 = newImage9


            //'$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ 16.01.2013
            //'$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ 
        }
        public void remove_H_Solid_Line()
        {
            Bitmap newImage = new Bitmap(image1);//PictureBox1.Image
            // newImage103 = new Bitmap(PictureBox11.Image);
            dynamic P1 = default(System.Drawing.Point);
            dynamic p2 = default(System.Drawing.Point);
            // System.Drawing.Point P3 = new System.Drawing.Point();

            //'TextBox3.Text = ""

            // string str = "";
            Graphics graphics = Graphics.FromImage(newImage);
            // Graphics graphics1 = Graphics.FromImage(newImage103);
            SolidBrush brush = new SolidBrush(Color.Black);////////////////////////////////
            Pen pen = new Pen(brush);
            Pen pen1 = new Pen(brush, 5);

            for (int i = 0; i <= (newImage.Width - 1); i++)
            {
                for (int j = 0; j <= (newImage.Height - 1); j++)
                {
                    if (newImage.GetPixel(i, j).Name == "ffff0000")
                    {
                        for (int k = i + 1; k <= (newImage.Width - 1); k++)
                        {
                            //If input_Matrix_check <= 495 Then
                            if (newImage.GetPixel(k, j).Name != "ffff0000" | (k == newImage.Width - 1 & newImage.GetPixel(k, j).Name == "ffff0000"))
                            {
                                if (k > i + 100)
                                {
                                    P1.X = i;
                                    P1.Y = j;
                                    p2.X = k - 1;
                                    //'p2.X = k
                                    p2.Y = j;

                                    //'####################### Line Matrix ###################

                                    //line_Matrix(input_Line_Matrix, 0) = P1.X
                                    //line_Matrix(input_Line_Matrix, 1) = P1.Y
                                    //line_Matrix(input_Line_Matrix, 2) = p2.X
                                    //line_Matrix(input_Line_Matrix, 3) = p2.Y
                                    //line_Matrix(input_Line_Matrix, 4) = 0 '' 0 means stright line, and 1 is dash line
                                    //input_Line_Matrix += 1

                                    //'####################### Line Matrix ###################


                                    graphics.DrawLine(pen1, P1, p2);
                                    // graphics1.DrawLine(pen1, P1, p2);

                                    image1 = newImage;////PictureBox1.Image = newImage;
                                    ////PictureBox1.Update();

                                    break; // TODO: might not be correct. Was : Exit For
                                }
                                else
                                {
                                    if (k > i + 10)
                                    {
                                        bool check_small_h_Line = false;
                                        check_small_h_Line = true;
                                        for (int a = i; a <= k - 1; a++)
                                        {
                                            if (newImage.GetPixel(a, j).Name != "ffff0000")
                                            {
                                                check_small_h_Line = false;
                                                break; // TODO: might not be correct. Was : Exit For
                                            }

                                        }

                                        dynamic d = 0;
                                        //' to ensure that i-4 not < 0
                                        if (i - 4 < 0)
                                            d = i;
                                        else
                                            d = i - 4;
                                        if (check_small_h_Line & newImage.GetPixel(d, j).Name == "ff000000" & newImage.GetPixel(k, j).Name == "ff000000")
                                            check_small_h_Line = false;

                                        if (check_small_h_Line)
                                        {
                                            //'MsgBox(newImage.GetPixel(i - 2, j).Name.ToString + "+++++" + newImage.GetPixel(k, j).Name.ToString)

                                            //line_Matrix_check(input_Matrix_check, 0) = i
                                            //line_Matrix_check(input_Matrix_check, 1) = j
                                            //line_Matrix_check(input_Matrix_check, 2) = k - 1
                                            //line_Matrix_check(input_Matrix_check, 3) = j
                                            //line_Matrix_check(input_Matrix_check, 4) = 0 '' 0 means stright line, and 1 is dash line
                                            //input_Matrix_check += 1
                                        }
                                    }

                                    break; // TODO: might not be correct. Was : Exit For
                                }


                            }
                            //'End If
                        }

                    }
                    //'If newImage.GetPixel(i, j).Name = "ffffffff" Then newImage.SetPixel(i, j, Color.Red)
                    //'If newImage.GetPixel(i, j).Name <> "ffffffff" Then
                    //'If newImage.GetPixel(i, j).Name <> "ff000000" Then
                    //'str = str + Environment.NewLine + newImage.GetPixel(i, j).Name.ToString
                    ///'Exit For
                    //'End If


                }
            }
            //'TextBox3.Text = str
            // remove_Solid_H_Line();
            image1 = newImage;////PictureBox1.Image = newImage;
            ////PictureBox1.Update();
            // PictureBox11.Image = newImage103;///////////////////////
            //PictureBox11.Update();/////////////////
            //PictureBox1.Update();
            //Set_H_Dash_Line();/////////////////////////////////
            // PictureBox11.Update();///////////////////
            //  Remove();
            //remove_Boxes();


        }

        public String SaveDrawnImage()
        {
            Data data = new Data();
            string file = null;


            MemoryStream stream = new MemoryStream();
            byte[] image = data.imageToByteArray(image11);
            stream.Write(image, 0, image.Length);
            Bitmap bitmap = new Bitmap(stream);

            //string root = Server.MapPath(filename + ".jpg");
            string folder = strPath.Remove(strPath.LastIndexOf('\\') + 1);
            string filename = "alu.jpg";//strPath.Substring(strPath.LastIndexOf('\\') + 1);
            //string ab = GetUniqueFilename(folder, filename);
            bitmap.Save(folder + filename);//@"C:\Users\Munir\Documents\Visual Studio 2012\Projects\UML_Project_1\UML_Project_1\munir11.jpg");
            bitmap.Dispose();
            file = strPath;//@"C:\Users\Munir\Documents\Visual Studio 2012\Projects\UML_Project_1\UML_Project_1\munir11.jpg";*/
            /*Directory.CreateDirectory("images");
            string path = Path.Combine(Environment.CurrentDirectory, @"images\image.jpg");
           //imagefile.(path, System.Drawing.Imaging.ImageFormat.Jpeg);
            file = @"images\image.jpg";
            */
            return file;
            /*Directory.CreateDirectory("images");
           string path = Path.Combine(Environment.CurrentDirectory, @"images\image.jpg"; 
           theImage.Save(path, System.Drawing.Imaging.ImageFormat.Jpeg);?
           */


        }

    }
}