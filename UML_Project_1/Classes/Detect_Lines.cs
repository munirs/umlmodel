﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AForge;

using AForge.Imaging;
using AForge.Imaging.Filters;
using AForge.Math.Geometry;

using System.Reflection;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;

namespace UML_Project_1.Classes
{
    public class Detect_Lines
    {
        public System.Drawing.Image image1;
        public System.Drawing.Image image11;
        public System.Drawing.Image imgRectangle;
        Bitmap newImage103;
        public static string pathImg1;
        protected string strPathImg1 { get; set; }
        protected string strPathImg2 { get; set; }
        String imgServerPath;
        String tempfile;
        Data data = new Data();
        List<Messages> messages = new List<Messages>();
        List<Lifelines> objLink;
        List<Lifelines> lifelines = new List<Lifelines>();
        int msgIntID = 0;

        String msgName = "Not Captured";
        int startX = 0;
        int startY = 0;
        int endX = 0;
        int endY = 0;
        int msgW = 0;
        int msgH = 0;
        String startName = "Unknown";
        String endName = "Unknown";
        String type = "un-specified";
        int topObjPointY = 0;
        int lefMostObjP = 0;
        int rightMostObjP = 0;

        public Detect_Lines(String strPathImg1, string strPathImg2, System.Drawing.Image image1, System.Drawing.Image image11, String imgServerPath, List<Lifelines> objLink)
        {
            this.strPathImg1 = strPathImg1;
            this.strPathImg2 = strPathImg2;
            this.image1 = image1;
            this.image11 = image11;
            this.imgServerPath = imgServerPath;
            this.objLink = objLink;
        }
        public string getImagePath()
        {
            return pathImg1;
        }

        public List<Messages> getMessages()
        {
            return messages;
        }
        public List<Lifelines> getLifelineMissObj()
        {
            return lifelines;
        }
        public void detect_H_Solid_Line()
        {
            topObjPointY = topObjCorner(objLink);
            lefMostObjP = leftMostObjCorner(objLink);
            rightMostObjP = rightMostObjCorner(objLink);
            msgIntID = objLink.Count;

            /*System.Drawing.Image img = PictureBox1.Image;
            PictureBox1.Image = ScaleByPercent(img, 200);
            PictureBox1.Update();*/

            Bitmap newimage10 = new Bitmap(System.Drawing.Image.FromFile(imgServerPath));
            AForge.Imaging.Filters.Grayscale filter1 = new AForge.Imaging.Filters.Grayscale(0.11, 0.59, 0.3);
            newimage10 = filter1.Apply(newimage10);
            imgRectangle = newimage10;//image for croping messages on the lines
            Set_H_Solid_Line();//prepare image for solid line detection

            Bitmap newImage = new Bitmap(image1);//PictureBox1.Image
            newImage103 = new Bitmap(image11);//PictureBox11.Image
            dynamic P1 = default(System.Drawing.Point);
            dynamic p2 = default(System.Drawing.Point);
            // System.Drawing.Point P3 = new System.Drawing.Point();

            //'TextBox3.Text = ""

            // string str = "";
            Graphics graphics = Graphics.FromImage(newImage);
            Graphics graphics1 = Graphics.FromImage(newImage103);
            SolidBrush brush = new SolidBrush(Color.Red);
            Pen pen = new Pen(brush);
            Pen pen1 = new Pen(brush, 5);

            ///bool checkonce = false;

            for (int i = 0; i <= (newImage.Width - 1); i++)
            {
                for (int j = 0; j <= (newImage.Height - 1); j++)
                {
                    if (newImage.GetPixel(i, j).Name == "ffffffff")
                    {
                        for (int k = i + 1; k <= (newImage.Width - 1); k++)
                        {

                            //If input_Matrix_check <= 495 Then
                            if (newImage.GetPixel(k, j).Name != "ffffffff" | (k == newImage.Width - 1 & newImage.GetPixel(k, j).Name == "ffffffff"))
                            {
                                if (k > i + 100 && isNotBorderLine(newImage, i, j, k) == true)
                                {

                                    P1.X = i;
                                    P1.Y = j;
                                    p2.X = k - 1;
                                    //'p2.X = k
                                    p2.Y = j;

                                    // draw the solid arrow
                                    //'####################### Line Matrix ###################
                                    int messageType = checkArrow(newImage103, i, j, k, true);

                                    graphics.DrawLine(pen1, P1, p2);
                                    graphics1.DrawLine(pen1, P1, p2);
                                    image1 = newImage;////PictureBox1.Image = newImage;
                                    ////PictureBox1.Update(); //Will never be displayed
                                    messageData(i, j, k, messageType);//Store data for XMI
                                    break; // TODO: might not be correct. Was : Exit For
                                }
                                else
                                {
                                    if (k > i + 10 && isNotBorderLine(newImage, i, j, k) == true)
                                    {
                                        bool check_small_h_Line = false;
                                        check_small_h_Line = true;
                                        for (int a = i; a <= k - 1; a++)
                                        {
                                            if (newImage.GetPixel(a, j).Name != "ffffffff")
                                            {
                                                check_small_h_Line = false;
                                                break; // TODO: might not be correct. Was : Exit For
                                            }

                                        }

                                        dynamic d = 0;
                                        //' to ensure that i-4 not < 0
                                        if (i - 4 < 0)
                                            d = i;
                                        else
                                            d = i - 4;
                                        if (check_small_h_Line & newImage.GetPixel(d, j).Name == "ff000000" & newImage.GetPixel(k, j).Name == "ff000000")
                                            check_small_h_Line = false;

                                        if (check_small_h_Line)
                                        {
                                            //'MsgBox(newImage.GetPixel(i - 2, j).Name.ToString + "+++++" + newImage.GetPixel(k, j).Name.ToString)

                                            //line_Matrix_check(input_Matrix_check, 0) = i
                                            //line_Matrix_check(input_Matrix_check, 1) = j
                                            //line_Matrix_check(input_Matrix_check, 2) = k - 1
                                            //line_Matrix_check(input_Matrix_check, 3) = j
                                            //line_Matrix_check(input_Matrix_check, 4) = 0 '' 0 means stright line, and 1 is dash line
                                            //input_Matrix_check += 1
                                        }

                                    }
                                    break; // TODO: might not be correct. Was : Exit For
                                }


                            }
                            //'End If
                        }
                    }
                    else if (newImage.GetPixel(i, j).Name == "ffff0000")
                    {
                        j = j + 5;// Skip duplicates in thick lines
                    }
                    //'If newImage.GetPixel(i, j).Name = "ffffffff" Then newImage.SetPixel(i, j, Color.Red)
                    //'If newImage.GetPixel(i, j).Name <> "ffffffff" Then
                    //'If newImage.GetPixel(i, j).Name <> "ff000000" Then
                    //'str = str + Environment.NewLine + newImage.GetPixel(i, j).Name.ToString
                    ///'Exit For
                    //'End If


                }
            }
            //'TextBox3.Text = str
            image1 = newImage;////PictureBox1.Image = newImage;
            ////PictureBox1.Update();
            image11 = newImage103;////PictureBox11.Image = newImage103;
            ////PictureBox11.Update();// To be updated in the final stage
            remove_H_Solid_Line();
            detect_H_Dash_Line();
            setMissingLiflines();
            //


            //  objLink.AddRange(lifelines);
            //Remove();
            //remove_Boxes();

            // Save and Display
            pathImg1 = SaveDrawnImage(image11, "LineImage1.jpg");// "Rect.jpg";
            pathImg1 = pathImg1.Substring(pathImg1.LastIndexOf('\\') + 1);


        }
        public void setMissingLiflines()
        {
            List<Messages> messagesLine = messages;//OrderBy(o => o.msgStartX).ToList();
            int y = 0, arx = 0;
            List<int> stor = new List<int>();
            List<string> stor1 = new List<string>();
            List<string> stor2 = new List<string>();
            List<int> stor3 = new List<int>();
            List<int> stor4 = new List<int>();

            for (int j = 0; j < messagesLine.Count; j++) // y+1
            {
                int ab2 = messagesLine[j].endObjCenter;
                int ab3 = messagesLine[j].startObjCenter;

                if (messagesLine[j].startObjName == "Unknown" && messagesLine[j].endObjName != "Unknown")
                {
                    if (Math.Abs(messagesLine[j].msgEndX - ab2) < 30)
                    {
                        stor.Add(messagesLine[j].msgStartX);
                        stor1.Add(messagesLine[j].startObjName);
                    }
                    else
                    {
                        stor.Add(messagesLine[j].msgEndX);
                        stor1.Add(messagesLine[j].startObjName);
                    }

                }
                if (messagesLine[j].endObjName == "Unknown" && messagesLine[j].startObjName != "Unknown")//(Math.Abs(messagesLine[arx].msgEndX - messagesLine[j].msgEndX) < 15)
                {

                    if (Math.Abs(messagesLine[j].msgStartX - ab3) < 30)
                    {
                        stor2.Add(messagesLine[j].endObjName);
                        stor3.Add(messagesLine[j].msgEndX);
                    }
                    else
                    {
                        stor2.Add(messagesLine[j].endObjName);
                        stor3.Add(messagesLine[j].msgStartX);
                    }
                    // arx = j;
                }

            }


            String objID = "UMLObject.";

            int temp1 = -1;
            stor.AddRange(stor3);
            stor.Sort();

            if (stor.Count > 1)
            {
                for (int i = 1; i < stor.Count; i++)
                {
                    if (Math.Abs(stor[arx] - stor[i]) < 40)
                    {
                        temp1 = stor[arx];

                    }
                    else
                    {
                        stor4.Add(stor[arx]);
                        arx = i;
                        temp1 = -1;
                    }



                }
                if (temp1 != -1)
                {
                    stor4.Add(temp1);
                }
                else if (temp1 == -1)
                {
                    stor4.Add(stor[arx]);
                }
                for (int j = 0; j < stor4.Count; j++)
                {

                    int xc = Int32.Parse(stor4[j].ToString());
                    Lifelines ok = new Lifelines("UnRecognized", xc, topObjPointY, 200, 200, xc, 500, objID + "" + (100 + j));
                    lifelines.Add(ok);

                }

            }
            else if (stor.Count == 1)
            {

                stor4.Add(stor[0]);
                int xc = Int32.Parse(stor4[0].ToString());
                Lifelines ok = new Lifelines("UnRecognized", xc, topObjPointY, 200, 200, xc, 500, objID + "" + (100));
                lifelines.Add(ok);
            }

            foreach (Lifelines missingObj in lifelines)
            {
                foreach (Messages allMsgs in messagesLine)
                {
                    if (allMsgs.startObjName == "Unknown" && allMsgs.endObjName != "Unknown")
                    {
                        if (missingObj.objCenter >= allMsgs.msgStartX - 30 && missingObj.objCenter <= allMsgs.msgStartX + 30 || missingObj.objCenter >= allMsgs.msgEndX - 30 && missingObj.objCenter <= allMsgs.msgEndX + 30)
                        {
                            allMsgs.startObjID = missingObj.objID;
                        }
                    }

                    if (allMsgs.startObjName != "Unknown" && allMsgs.endObjName == "Unknown")
                    {
                        if (missingObj.objCenter >= allMsgs.msgStartX - 30 && missingObj.objCenter <= allMsgs.msgStartX + 30 || missingObj.objCenter >= allMsgs.msgEndX - 30 && missingObj.objCenter <= allMsgs.msgEndX + 30)
                        {
                            allMsgs.endObjID = missingObj.objID;
                        }
                    }
                }
            }
            messages = messagesLine;
        }
        public int checkArrow(Bitmap newImage, int i, int j, int k, Boolean isSolid)
        {
            int endArrowType = 0;
            int beginArrowType = 0;

            endArrowType = EndArrow(newImage, (k - 5), (j - 7));
            try
            {
                if (endArrowType == 1 && beginArrowType == 0)
                {
                    // solid arrow to right
                    for (int l = 0; l < 20; l++)
                    {
                        newImage.SetPixel(k, j + l, Color.Green);
                        newImage.SetPixel(k + 1, j + l, Color.Green);
                        newImage103.SetPixel(k, j + l, Color.Green);
                        newImage103.SetPixel(k + 1, j + l, Color.Green);

                    }
                }
                else if (endArrowType == 2 && beginArrowType == 0)
                {
                    // Open arrow to right
                    for (int l = 0; l < 20; l++)
                    {

                        newImage.SetPixel(k, j + l, Color.Brown);
                        newImage.SetPixel(k + 1, j + l, Color.Brown);
                        newImage103.SetPixel(k, j + l, Color.Brown);
                        newImage103.SetPixel(k + 1, j + l, Color.Brown);
                    }
                }
                else
                {
                    // 0 nothing
                }

                //
                beginArrowType = BeginningArrow(newImage, i - 5, j - 7);
                if (beginArrowType == 3 && endArrowType == 0)
                {
                    // solid arrow to the left
                    // isSolid = false;//leo
                    for (int l = 0; l < 20; l++)
                    {

                        newImage.SetPixel(i, j + l, Color.Green);
                        newImage.SetPixel(i + 1, j + l, Color.Green);
                        newImage103.SetPixel(i, j + l, Color.Green);
                        newImage103.SetPixel(i + 1, j + l, Color.Green);
                    }
                }
                else if (beginArrowType == 4 && endArrowType == 0)
                {
                    // Open arrow to the left
                    // isSolid = false;// leo
                    for (int l = 0; l < 20; l++)
                    {

                        newImage.SetPixel(i, j + l, Color.Brown);
                        newImage.SetPixel(i, j + l, Color.Brown);
                        newImage103.SetPixel(i, j + l, Color.Brown);
                        newImage103.SetPixel(i + 1, j + l, Color.Brown);

                    }
                }
                else
                {
                    // 0 nothing 
                    /* if (isSolid == true)
                     {
                         // Assume solid to right
                         for (int l = 0; l < 20; l++)
                         {

                             newImage.SetPixel(k, j + l, Color.Green);
                             newImage.SetPixel(k + 1, j + l, Color.Green);
                             newImage103.SetPixel(k, j + l, Color.Green);
                             newImage103.SetPixel(k + 1, j + l, Color.Green);

                         }
                     }
                     else if(isSolid==false)
                     {
                         // Assume oppen arrow to the left
                         for (int l = 0; l < 20; l++)
                         {

                             newImage.SetPixel(i, j + l, Color.Brown);
                             newImage.SetPixel(i, j + l, Color.Brown);
                             newImage103.SetPixel(i, j + l, Color.Brown);
                             newImage103.SetPixel(i + 1, j + l, Color.Brown);

                         }
                     }*/

                }

            }
            catch
            {

            }
            return setMsgTypeAndDirection(beginArrowType, endArrowType, isSolid);
        }
        public int setMsgTypeAndDirection(int beginArrowType, int endArrowType, Boolean isSolid)
        {
            if (isSolid == true && endArrowType == 0 && beginArrowType == 0)
            {
                //Solid, arrow un-detected
                type = "CallAction";//to be chenged
                return -2;
            }
            if (isSolid == false && endArrowType == 0 && beginArrowType == 0)
            {
                //Dashed, arrow un-detected
                type = "ReturnAction";//to be changed
                return -1;
            }
            if (isSolid == true && endArrowType == 1)
            {
                //Solid line, solid Arrow to the right
                type = "CallAction";
                return 2;
            }
            if (isSolid == true && endArrowType == 2)
            {
                //Solid line, open Arrow to the right
                type = "SendAction";
                return 2;
            }
            if (isSolid == true && beginArrowType == 3)
            {
                //Solid line, solid Arrow to the left
                type = "CallAction";
                return 1;
            }
            if (isSolid == true && beginArrowType == 4)
            {
                //Solid line, open Arrow to the left
                type = "SendAction";
                return 1;
            }
            if (isSolid == false && (endArrowType == 1 || endArrowType == 2))
            {
                //Dashed line, Open or solid arrow to the right
                type = "ReturnAction";//CreateAction
                return 4;
            }
            else if (isSolid == false && (beginArrowType == 3 || beginArrowType == 4))
            {
                //Dashed line, Open or solid arrow to the left
                type = "ReturnAction";
                return 3;
            }

            return 0;
        }
        public int BeginningArrow(Bitmap newImage, int k, int j)
        {
            int YN = 0;
            int countUpper = 0;
            /* int countLower = 0;
             bool singleEndarrowU = false;
             bool singleEndarrowL = false;*/
            System.Drawing.Image img;
            Bitmap source = new Bitmap(newImage);
            Rectangle section = new Rectangle(new System.Drawing.Point(k, j), new Size(20, 16));
            Data data = new Data();
            Bitmap CroppedImage = data.CropImage(source, section);
            img = CroppedImage;
            img = data.ScaleCropImage(img);

            Bitmap newImage1 = new Bitmap(img);
            AForge.Imaging.Filters.Grayscale filter = new AForge.Imaging.Filters.Grayscale(0.11, 0.59, 0.3);
            newImage1 = filter.Apply(newImage1);
            AForge.Imaging.Filters.Threshold filter4 = new AForge.Imaging.Filters.Threshold(1);
            //'30.10.2012 ''Update: back to active it to detect dash lines
            newImage1 = filter4.Apply(newImage1);
            //pictureBox3.Image = newImage1;
            // pictureBox3.Update();
            ///bool st = false;
            int c = 0;
            for (int m = 20; m > 0; m--)
            {
                // bool trU = false;
                // bool trL = false;
                int tempU = 0;
                int tempL = 0;
                try
                {
                    for (int n = 0; n < 15; n++)//j - 15 > 0 && 
                    {
                        // MessageBox.Show(newImage1.GetPixel(m, n).Name);
                        if (newImage1.GetPixel(m, n).Name == "ff000000" && (newImage1.GetPixel(m - 1, n + 1).Name == "ff000000" || newImage1.GetPixel(m - 2, n + 2).Name == "ff000000")
                            && (newImage1.GetPixel(m - 3, n + 3).Name == "ff000000" || newImage1.GetPixel(m - 4, n + 4).Name == "ff000000"))
                        //(newImage1.GetPixel(m, n).Name == "ff000000" && (newImage1.GetPixel(m - 1, n + 1).Name == "ff000000" || newImage1.GetPixel(m - 2, n + 2).Name == "ff000000"))
                        {

                            //newImage.SetPixel(m, n, Color.Green);
                            if (newImage1.GetPixel(m, n + 1).Name == "ff000000" && newImage1.GetPixel(m, n + 2).Name == "ff000000" && newImage1.GetPixel(m, n + 3).Name == "ff000000")//(countUpper > 3 && c != 100)// && newImage1.GetPixel(m, n - 3).Name != "ff000000")
                            {
                                // solid end arrow
                                // trU = true;
                                tempU = n;
                                // singleEndarrowU = false;
                                // st = true;
                                c = 3;
                                break;
                            }
                            else if (newImage1.GetPixel(m, n + 1).Name != "ff000000" || newImage1.GetPixel(m, n + 2).Name != "ff000000")// && ( newImage1.GetPixel(m-1, n ).Name != "ff000000" || newImage1.GetPixel(m-2, n ).Name != "ff000000"))
                            {
                                // single end arrow;
                                //  trU = false;
                                tempU = n;
                                c = 100;
                                // countUpper = 0;
                                break;//
                            }
                            else { }
                            // countUpper++;
                        }
                        else
                        {
                            //newImage.SetPixel(m, n, Color.Black);
                            //countUpper = 0;
                        }
                    }

                }
                catch (ArgumentOutOfRangeException)
                {
                }
                if (c == 3 && c != 100)// && trL == true)
                {

                    YN = 3;
                    break;

                }
                if (c == 100) //singleEndarrowU == true singleEndarrowL == true &&
                {
                    YN = 4;
                    break;

                }

            }

            return YN;

        }
        public int EndArrow(Bitmap newImage, int k, int j)
        {
            int YN = 0;
            int countUpper = 0;
            /* int countLower = 0;
             bool singleEndarrowU = false;
             bool singleEndarrowL = false;*/
            System.Drawing.Image img;
            Bitmap source = new Bitmap(newImage);
            Rectangle section = new Rectangle(new System.Drawing.Point(k, j), new Size(15, 15));
            Data data = new Data();

            Bitmap CroppedImage = data.CropImage(source, section);
            img = CroppedImage;
            img = new Bitmap(img);
            img = data.ScaleCropImage(img);

            Bitmap newImage1 = new Bitmap(img);
            AForge.Imaging.Filters.Grayscale filter = new AForge.Imaging.Filters.Grayscale(0.11, 0.59, 0.3);
            newImage1 = filter.Apply(newImage1);
            AForge.Imaging.Filters.Threshold filter4 = new AForge.Imaging.Filters.Threshold(1);
            //'30.10.2012 ''Update: back to active it to detect dash lines
            newImage1 = filter4.Apply(newImage1);
            //pictureBox3.Image = newImage1;
            //pictureBox3.Update();
            //  bool st = false;
            int c = 0;
            for (int m = 0; m < 15; m++)
            {
                // bool trU = false;
                // bool trL = false;
                int tempU = 0;
                int tempL = 0;
                try
                {
                    for (int n = 0; n < 15; n++)//j - 15 > 0 && 
                    {
                        // MessageBox.Show(newImage1.GetPixel(m, n).Name); //newImage1.GetPixel(m+1, n+1).Name == "ff000000" ||
                        if (newImage1.GetPixel(m, n).Name == "ff000000" && (newImage1.GetPixel(m + 1, n + 1).Name == "ff000000" || newImage1.GetPixel(m + 2, n + 2).Name == "ff000000")
                            && (newImage1.GetPixel(m + 3, n + 3).Name == "ff000000" || newImage1.GetPixel(m + 4, n + 4).Name == "ff000000"))// && newImage1.GetPixel(m, n - 2).Name != "ff000000")
                        {
                            //newImage.SetPixel(m, n, Color.Green);
                            if (newImage1.GetPixel(m, n + 1).Name == "ff000000" && newImage1.GetPixel(m, n + 2).Name == "ff000000" && newImage1.GetPixel(m, n + 3).Name == "ff000000")//countUpper > 4 && c != 100  )
                            {
                                // solid end arrow
                                tempU = n;
                                c = 3;
                                break;
                            }
                            else if (newImage1.GetPixel(m, n + 2).Name != "ff000000" || newImage1.GetPixel(m, n - 2).Name != "ff000000")
                            {
                                // single end arrow;
                                tempU = n;
                                c = 100;
                                countUpper = 0;
                                break;

                            }
                            countUpper++;
                        }
                        else
                        {
                            //newImage.SetPixel(m, n, Color.Black);
                            countUpper = 0;
                        }
                    }

                }
                catch (ArgumentOutOfRangeException)
                {
                }
                if (c == 3 && c != 100)// && trL == true)
                {

                    YN = 1;
                    break;

                }
                if (c == 100) //singleEndarrowU == true singleEndarrowL == true &&
                {
                    YN = 2;
                    break;

                }

            }

            return YN;

        }
        public String SaveDrawnImage(System.Drawing.Image imagefile, string str)
        {
            string file = null;
            Data data = new Data();

            MemoryStream stream = new MemoryStream();
            byte[] image = data.imageToByteArray(imagefile);
            stream.Write(image, 0, image.Length);
            Bitmap bitmap = new Bitmap(stream);

            //string root = Server.MapPath(filename + ".jpg");
            string folder = strPathImg1.Remove(strPathImg1.LastIndexOf('\\') + 1);
            string filename = str;// "alu.jpg";//strPath.Substring(strPath.LastIndexOf('\\') + 1);
            string ab = data.GetUniqueFilename(folder, filename);
            bitmap.Save(folder + ab);//@"C:\Users\Munir\Documents\Visual Studio 2012\Projects\UML_Project_1\UML_Project_1\munir11.jpg");
            bitmap.Dispose();
            file = strPathImg1;//@"C:\Users\Munir\Documents\Visual Studio 2012\Projects\UML_Project_1\UML_Project_1\munir11.jpg";*/
            /*Directory.CreateDirectory("images");
            string path = Path.Combine(Environment.CurrentDirectory, @"images\image.jpg");
           //imagefile.(path, System.Drawing.Imaging.ImageFormat.Jpeg);
            file = @"images\image.jpg";
            */
            return folder + ab;
            /*Directory.CreateDirectory("images");
           string path = Path.Combine(Environment.CurrentDirectory, @"images\image.jpg"; 
           theImage.Save(path, System.Drawing.Imaging.ImageFormat.Jpeg);?
           */


        }
        public void detect_H_Dash_Line()
        {
            //Set_H_Solid_Line();
            //Set_H_Dash_Line();
            Bitmap newImage = new Bitmap(image1);
            newImage103 = new Bitmap(image11);
            dynamic P1 = default(System.Drawing.Point);
            dynamic p2 = default(System.Drawing.Point);
            //System.Drawing.Point P3 = new System.Drawing.Point();

            // string str = "";
            Graphics graphics = Graphics.FromImage(newImage);
            Graphics graphics1 = Graphics.FromImage(newImage103);
            SolidBrush brush = new SolidBrush(Color.Blue);
            Pen pen = new Pen(brush);
            Pen pen1 = new Pen(brush, 5);

            for (int i = 0; i <= (newImage.Width - 1); i++)
            {
                for (int j = 0; j <= (newImage.Height - 1); j++)
                {
                    if (newImage.GetPixel(i, j).Name == "ffffffff")
                    {
                        Boolean endOfLine = false;
                        //int position = i + 1
                        for (int k = i + 1; k <= (newImage.Width - 1); k++)
                        {
                            //If input_Matrix_check <= 495 Then
                            // has some problem, it creats parameter <width ??? exception
                            try
                            {
                                endOfLine = newImage.GetPixel(k, j).Name != "ffffffff" && newImage.GetPixel(k + 1, j).Name != "ffffffff" && newImage.GetPixel(k + 2, j).Name != "ffffffff" && newImage.GetPixel(k + 4, j).Name != "ffffffff";
                            }
                            catch (ArgumentOutOfRangeException argOut)
                            {
                                endOfLine = true;
                            }
                            if (endOfLine || (k == newImage.Width - 1 && newImage.GetPixel(k, j).Name == "ffffffff"))
                            {
                                if (k > i + 100 && isThisLine(i, j, k, newImage) == true && isNotBorderLine(newImage, i, j, k) == true)
                                {
                                    P1.X = i;
                                    P1.Y = j;
                                    p2.X = k - 1;
                                    //'p2.X = k
                                    p2.Y = j;

                                    //'####################### Line Matrix ###################

                                    //line_Matrix(input_Line_Matrix, 0) = P1.X
                                    //line_Matrix(input_Line_Matrix, 1) = P1.Y
                                    //line_Matrix(input_Line_Matrix, 2) = p2.X
                                    //line_Matrix(input_Line_Matrix, 3) = p2.Y
                                    //line_Matrix(input_Line_Matrix, 4) = 0 '' 0 means stright line, and 1 is dash line
                                    //input_Line_Matrix += 1

                                    //'####################### Line Matrix ###################

                                    int messageType = checkArrow(newImage103, i, j, k, false);
                                    graphics.DrawLine(pen1, P1, p2);
                                    graphics1.DrawLine(pen1, P1, p2);
                                    image1 = newImage;
                                    ////PictureBox1.Image = newImage;
                                    ////PictureBox1.Update();
                                    messageData(i, j, k, messageType);//Store data for XMI
                                    break; // TODO: might not be correct. Was : Exit For

                                }
                                else
                                {
                                    if (k > i + 10 && isThisLine(i, j, k, newImage) == true && isNotBorderLine(newImage, i, j, k) == true)
                                    {
                                        bool check_small_h_Line = false;
                                        check_small_h_Line = true;
                                        for (int a = i; a <= k - 1; a++)
                                        {
                                            if (newImage.GetPixel(a, j).Name != "ffffffff")
                                            {
                                                check_small_h_Line = false;
                                                break; // TODO: might not be correct. Was : Exit For
                                            }

                                        }

                                        dynamic d = 0;
                                        //' to ensure that i-4 not < 0
                                        if (i - 4 < 0)
                                            d = i;
                                        else
                                            d = i - 4;
                                        if (check_small_h_Line & newImage.GetPixel(d, j).Name == "ff000000" & newImage.GetPixel(k, j).Name == "ff000000")
                                            check_small_h_Line = false;

                                        if (check_small_h_Line)
                                        {
                                            //'MsgBox(newImage.GetPixel(i - 2, j).Name.ToString + "+++++" + newImage.GetPixel(k, j).Name.ToString)

                                            //line_Matrix_check(input_Matrix_check, 0) = i
                                            //line_Matrix_check(input_Matrix_check, 1) = j
                                            //line_Matrix_check(input_Matrix_check, 2) = k - 1
                                            //line_Matrix_check(input_Matrix_check, 3) = j
                                            //line_Matrix_check(input_Matrix_check, 4) = 0 '' 0 means stright line, and 1 is dash line
                                            //input_Matrix_check += 1
                                        }
                                    }

                                    break; // TODO: might not be correct. Was : Exit For
                                }


                            }
                            //'End If
                        }
                    }
                    else if (newImage.GetPixel(i, j).Name == "ff0000ff")
                    {
                        j = j + 5;//Skip duplicates in thick lines
                    }
                    //'If newImage.GetPixel(i, j).Name = "ffffffff" Then newImage.SetPixel(i, j, Color.Red)
                    //'If newImage.GetPixel(i, j).Name <> "ffffffff" Then
                    //'If newImage.GetPixel(i, j).Name <> "ff000000" Then
                    //'str = str + Environment.NewLine + newImage.GetPixel(i, j).Name.ToString
                    ///'Exit For
                    //'End If


                }
            }
            image1 = newImage;
            image11 = newImage103;

        }
        public void messageData(int i, int j, int k, int typeOfMsg)
        {
            String msgID = "UMLStimulus." + (msgIntID = msgIntID + 2);
            String startObjectID = "Unknown";
            String endObjectID = "Unknown";
            int startObjC = 0;
            int endObjC = 0;
            int cropx = i;
            int cropy = j - 20;
            int cropwidth = Math.Abs(k - 1 - i);
            int cropheight = 20;
            tempfile = data.CropImageRectangle(imgRectangle, cropx, cropy, cropwidth, cropheight, strPathImg1);
            String ocrOut = data.OCR(tempfile);
            if (ocrOut != null)
            {
                msgName = ocrOut;
            }
            else
            {
                cropx = i;
                cropy = j + 2;
                cropwidth = Math.Abs(k - 1 - i);
                cropheight = 22;
                tempfile = data.CropImageRectangle(imgRectangle, cropx, cropy, cropwidth, cropheight, strPathImg1);
                ocrOut = data.OCR(tempfile);
                if (ocrOut != null)
                {
                    msgName = ocrOut;
                }
                else
                {
                    msgName = "Not Captured";
                }
            }

            foreach (Lifelines link in objLink)//Identify source and target object of the messages
            {
                if (i > link.pointX && i < link.pointX + link.objWidth)
                {

                    if (typeOfMsg == 2 || typeOfMsg == 4 || typeOfMsg == -2)
                    {
                        startName = link.objName;
                        startObjectID = link.objID;
                        startObjC = link.objCenter;
                    }
                    if (typeOfMsg == 1 || typeOfMsg == 3 || typeOfMsg == -1)
                    {
                        endName = link.objName;
                        endObjectID = link.objID;
                        endObjC = link.objCenter;
                    }
                }


                if ((k - 1 > link.pointX && k - 1 < link.pointX + link.objWidth) || (j >= link.pointY && j <= link.pointY + link.objHeight && k >= link.pointX - 10))
                {

                    if (typeOfMsg == 2 || typeOfMsg == 4 || typeOfMsg == -2)
                    {
                        endName = link.objName;
                        endObjectID = link.objID;
                        endObjC = link.objCenter;

                    }
                    if (typeOfMsg == 1 || typeOfMsg == 3 || typeOfMsg == -1)
                    {
                        startName = link.objName;
                        startObjectID = link.objID;
                        startObjC = link.objCenter;
                    }
                }

            }

            startX = i;
            startY = j;
            endX = k - 1;
            endY = j;
            msgW = endX - startX;
            Messages msgs = new Messages(msgName, startX, startY, endX, endY, msgW, msgH, startName, endName, type, msgID, startObjectID, endObjectID, startObjC, endObjC);
            messages.Add(msgs);
            startName = "Unknown";
            endName = "Unknown";
            startObjectID = "Unknown";
            endObjectID = "Unknown";

        }
        public void detect_Vertical_Line()
        {
            Bitmap newImage = new Bitmap(image1);
            newImage103 = new Bitmap(image11);

            dynamic P1 = default(System.Drawing.Point);
            dynamic p2 = default(System.Drawing.Point);
            //  System.Drawing.Point P3 = new System.Drawing.Point();

            //'TextBox3.Text = ""


            //string str = "";
            Graphics graphics = Graphics.FromImage(newImage);
            Graphics graphics1 = Graphics.FromImage(newImage103);
            SolidBrush brush = new SolidBrush(Color.Yellow);
            Pen pen = new Pen(brush);
            Pen pen1 = new Pen(brush, 5);

            for (int i = 0; i <= (newImage.Width - 1); i++)
            {
                for (int j = 0; j <= (newImage.Height - 1); j++)
                {
                    if (newImage.GetPixel(i, j).Name == "ffffffff")
                    {
                        for (int k = j + 1; k <= (newImage.Height - 1); k++)
                        {
                            //If input_Matrix_check <= 495 Then
                            if (newImage.GetPixel(i, k).Name != "ffffffff")
                            {
                                if (k > j + 20 && k - j < newImage.Height - 200)//65
                                {
                                    P1.X = i;
                                    P1.Y = j;
                                    p2.X = i;
                                    p2.Y = k - 1;

                                    //'####################### Line Matrix ###################

                                    //line_Matrix(input_Line_Matrix, 0) = P1.X
                                    //line_Matrix(input_Line_Matrix, 1) = P1.Y
                                    //line_Matrix(input_Line_Matrix, 2) = p2.X
                                    //line_Matrix(input_Line_Matrix, 3) = p2.Y
                                    //line_Matrix(input_Line_Matrix, 4) = 0 '' 0 means stright line, and 1 is dash line
                                    //input_Line_Matrix += 1

                                    //'####################### Line Matrix ###################

                                    graphics.DrawLine(pen1, P1, p2);
                                    graphics1.DrawLine(pen1, P1, p2);

                                    image1 = newImage;
                                    ////PictureBox1.Image = newImage;
                                    ////PictureBox1.Update();
                                    break; // TODO: might not be correct. Was : Exit For
                                }
                                else
                                {
                                    if (k > j + 10 && k - j < newImage.Height - 200)
                                    {
                                        bool check_small_h_Line = false;
                                        check_small_h_Line = true;
                                        for (int a = j; a <= k - 1; a++)
                                        {
                                            if (newImage.GetPixel(i, a).Name != "ffffffff")
                                            {
                                                check_small_h_Line = false;
                                                break; // TODO: might not be correct. Was : Exit For
                                            }

                                        }

                                        if (check_small_h_Line)
                                        {
                                            //'MsgBox(newImage.GetPixel(k + 1, j).Name.ToString + "+++++" + newImage.GetPixel(i - 1, j).Name.ToString)
                                            //line_Matrix_check(input_Matrix_check, 0) = i
                                            //line_Matrix_check(input_Matrix_check, 1) = j
                                            //line_Matrix_check(input_Matrix_check, 2) = i
                                            //line_Matrix_check(input_Matrix_check, 3) = k - 1
                                            //line_Matrix_check(input_Matrix_check, 4) = 0 '' 0 means stright line, and 1 is dash line
                                            //input_Matrix_check += 1
                                        }
                                    }

                                    break; // TODO: might not be correct. Was : Exit For
                                }


                            }

                            //End If
                        }

                    }
                    //'If newImage.GetPixel(i, j).Name = "ffffffff" Then newImage.SetPixel(i, j, Color.Red)
                    //'If newImage.GetPixel(i, j).Name <> "ffffffff" Then
                    //'If newImage.GetPixel(i, j).Name <> "ff000000" Then
                    //'str = str + Environment.NewLine + newImage.GetPixel(i, j).Name.ToString
                    ///'Exit For
                    //'End If


                }
            }
            //'TextBox3.Text = str
            image1 = newImage;
            image11 = newImage103;
            ////PictureBox1.Image = newImage;
            ////PictureBox1.Update();
            ////PictureBox11.Image = newImage103;
            ////PictureBox11.Update();
            // PictureBox1.Update();
            //PictureBox11.Update();

            // remove_Vline();

        }

        public void Set_H_Solid_Line()
        {

            Bitmap newImg = new Bitmap(image1);////PictureBox1.Image
            AForge.Imaging.Filters.GaussianSharpen filter2 = new AForge.Imaging.Filters.GaussianSharpen();

            for (int s = 0; s <= 2; s++)
            {
                newImg = filter2.Apply(newImg);/////////////////////////////////
            }
            AForge.Imaging.Filters.Grayscale filter = new AForge.Imaging.Filters.Grayscale(0.11, 0.59, 0.3);
            newImg = filter.Apply(newImg);

            image1 = newImg;////PictureBox1.Image = newImg;
            ////PictureBox1.Update();


            AForge.Imaging.Filters.Threshold filter4 = new AForge.Imaging.Filters.Threshold(1);//////210
            //'30.10.2012 ''Update: back to active it to detect dash lines
            newImg = filter4.Apply(newImg);
            //'30.10.2012 '' back to active it to detect dash lines

            image1 = newImg;////PictureBox1.Image = newImg;
            ////PictureBox1.Update();

            AForge.Imaging.Filters.SobelEdgeDetector filter1 = new AForge.Imaging.Filters.SobelEdgeDetector();
            newImg = filter1.Apply(newImg);

            image1 = newImg;////PictureBox1.Image = newImg;
            ////PictureBox1.Update();
            /*
                        Dilatation filter2 = new Dilatation();
                        newImg = filter2.Apply(newImg);

                        PictureBox1.Image = newImg;
                        PictureBox1.Update();
             */

            //'################################################################################################
            //'################################################################################################

            Bitmap newImage8 = new Bitmap(image1);////PictureBox1.Image

            //'TextBox3.Text = ""
            for (int i = 0; i <= (newImage8.Width - 1); i++)
            {
                for (int j = 0; j <= (newImage8.Height - 1); j++)
                {
                    if (newImage8.GetPixel(i, j).Name != "ffffffff")
                        newImage8.SetPixel(i, j, Color.Black);
                    //'If newImage.GetPixel(i, j).Name = "ffffffff" Then newImage.SetPixel(i, j, Color.Red)
                    //'If newImage.GetPixel(i, j).Name <> "ffffffff" Then
                    //'TextBox1.Text = newImage.GetPixel(i, j).Name
                    //'End If

                }
            }
            image1 = newImage8;////PictureBox1.Image = newImage8;
            ////PictureBox1.Update();
            Bitmap newImage2 = new Bitmap(newImage8);
            AForge.Imaging.Filters.Grayscale filter8 = new AForge.Imaging.Filters.Grayscale(0.11, 0.59, 0.3);
            newImage2 = filter8.Apply(newImage2);
            //'Dim filter4 As Filters.CannyEdgeDetector = New Filters.CannyEdgeDetector()
            //'newImage2 = filter4.Apply(newImage2)
            //'######################################################################################
            //'######################################################################################

            /*
             Erosion filter3 = new Erosion();
             newImage2 = filter3.Apply(newImage2);
             newImage2 = filter3.Apply(newImage2);
             */

            //'Dim filter4 As Opening = New Opening()
            //'newImage2 = filter4.Apply(newImage2)

            image1 = newImage2;////PictureBox1.Image = newImage2;
            ////PictureBox1.Update();

            Bitmap newImage9 = new Bitmap(image1);//PictureBox1.Image

            //'TextBox3.Text = ""


            for (int i = 0; i <= (newImage9.Width - 1); i++)
            {
                for (int j = 0; j <= (newImage9.Height - 1); j++)
                {
                    if (newImage9.GetPixel(i, j).Name != "ff000000")
                        newImage9.SetPixel(i, j, Color.White);
                    //'If newImage.GetPixel(i, j).Name = "ffffffff" Then newImage.SetPixel(i, j, Color.Red)
                    //'If newImage.GetPixel(i, j).Name <> "ffffffff" Then
                    //'TextBox1.Text = newImage.GetPixel(i, j).Name
                    //'End If

                }
            }




            image1 = newImage9;////PictureBox1.Image = newImage9;
            ////PictureBox1.Update();
            //newImage102 = newImage9


            //'$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ 16.01.2013
            //'$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ 
        }

        public void Set_H_Dash_Line()
        {

            Bitmap newImg = new Bitmap(image1);


            AForge.Imaging.Filters.Grayscale filter = new AForge.Imaging.Filters.Grayscale(0.11, 0.59, 0.3);
            newImg = filter.Apply(newImg);

            ////PictureBox1.Image = newImg;
            ////PictureBox1.Update();


            AForge.Imaging.Filters.Threshold filter4 = new AForge.Imaging.Filters.Threshold(210);
            //'30.10.2012 ''Update: back to active it to detect dash lines
            newImg = filter4.Apply(newImg);
            //'30.10.2012 '' back to active it to detect dash lines

            ////PictureBox1.Image = newImg;
            ////PictureBox1.Update();

            AForge.Imaging.Filters.SobelEdgeDetector filter1 = new AForge.Imaging.Filters.SobelEdgeDetector();
            newImg = filter1.Apply(newImg);


            // Dilatation filter2 = new Dilatation();
            // newImg = filter2.Apply(newImg);

            ////PictureBox1.Image = newImg;
            ////PictureBox1.Update();
            Dilatation filter2 = new Dilatation();
            for (int d = 0; d < 3; d++)
            {
                newImg = filter2.Apply(newImg);
            }

            image1 = newImg;
            ////PictureBox1.Image = newImg;
            ////PictureBox1.Update();


            //'################################################################################################
            //'################################################################################################

            Bitmap newImage8 = new Bitmap(image1);

            //'TextBox3.Text = ""
            for (int i = 0; i <= (newImage8.Width - 1); i++)
            {
                for (int j = 0; j <= (newImage8.Height - 1); j++)
                {
                    if (newImage8.GetPixel(i, j).Name != "ffffffff")
                        newImage8.SetPixel(i, j, Color.Black);
                    //'If newImage.GetPixel(i, j).Name = "ffffffff" Then newImage.SetPixel(i, j, Color.Red)
                    //'If newImage.GetPixel(i, j).Name <> "ffffffff" Then
                    //'TextBox1.Text = newImage.GetPixel(i, j).Name
                    //'End If

                }
            }
            ////PictureBox1.Image = newImage8;
            ////PictureBox1.Update();
            Bitmap newImage2 = new Bitmap(newImage8);
            AForge.Imaging.Filters.Grayscale filter8 = new AForge.Imaging.Filters.Grayscale(0.11, 0.59, 0.3);
            newImage2 = filter8.Apply(newImage2);
            //'Dim filter4 As Filters.CannyEdgeDetector = New Filters.CannyEdgeDetector()
            //'newImage2 = filter4.Apply(newImage2)
            //'######################################################################################
            //'######################################################################################

            Erosion filter3 = new Erosion();

            //newImage2 = filter3.Apply(newImage2);
            //newImage2 = filter3.Apply(newImage2);

            for (int e = 0; e < 2; e++)
            {
                newImage2 = filter3.Apply(newImage2);
            }


            //'Dim filter4 As Opening = New Opening()
            //'newImage2 = filter4.Apply(newImage2)

            image1 = newImage2;
            ////PictureBox1.Image = newImage2;
            ////PictureBox1.Update();

            Bitmap newImage9 = new Bitmap(image1);

            //'TextBox3.Text = ""


            for (int i = 0; i <= (newImage9.Width - 1); i++)
            {
                for (int j = 0; j <= (newImage9.Height - 1); j++)
                {
                    if (newImage9.GetPixel(i, j).Name != "ff000000")
                        newImage9.SetPixel(i, j, Color.White);
                    //'If newImage.GetPixel(i, j).Name = "ffffffff" Then newImage.SetPixel(i, j, Color.Red)
                    //'If newImage.GetPixel(i, j).Name <> "ffffffff" Then
                    //'TextBox1.Text = newImage.GetPixel(i, j).Name
                    //'End If

                }
            }



            image1 = newImage9;
            ////PictureBox1.Image = newImage9;
            ////PictureBox1.Update();
            //newImage102 = newImage9


            //'$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ 16.01.2013
            //'$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ 
        }

        public void remove_H_Solid_Line()
        {
            Bitmap newImage = new Bitmap(image1);//PictureBox1.Image
            // newImage103 = new Bitmap(PictureBox11.Image);
            dynamic P1 = default(System.Drawing.Point);
            dynamic p2 = default(System.Drawing.Point);
            // System.Drawing.Point P3 = new System.Drawing.Point();

            //'TextBox3.Text = ""

            // string str = "";
            Graphics graphics = Graphics.FromImage(newImage);
            // Graphics graphics1 = Graphics.FromImage(newImage103);
            SolidBrush brush = new SolidBrush(Color.Black);////////////////////////////////
            Pen pen = new Pen(brush);
            Pen pen1 = new Pen(brush, 5);

            for (int i = 0; i <= (newImage.Width - 1); i++)
            {
                for (int j = 0; j <= (newImage.Height - 1); j++)
                {
                    if (newImage.GetPixel(i, j).Name == "ffff0000")
                    {
                        for (int k = i + 1; k <= (newImage.Width - 1); k++)
                        {
                            //If input_Matrix_check <= 495 Then
                            if (newImage.GetPixel(k, j).Name != "ffff0000" | (k == newImage.Width - 1 & newImage.GetPixel(k, j).Name == "ffff0000"))
                            {
                                if (k > i + 100)
                                {
                                    P1.X = i;
                                    P1.Y = j;
                                    p2.X = k - 1;
                                    //'p2.X = k
                                    p2.Y = j;

                                    //'####################### Line Matrix ###################

                                    //line_Matrix(input_Line_Matrix, 0) = P1.X
                                    //line_Matrix(input_Line_Matrix, 1) = P1.Y
                                    //line_Matrix(input_Line_Matrix, 2) = p2.X
                                    //line_Matrix(input_Line_Matrix, 3) = p2.Y
                                    //line_Matrix(input_Line_Matrix, 4) = 0 '' 0 means stright line, and 1 is dash line
                                    //input_Line_Matrix += 1

                                    //'####################### Line Matrix ###################


                                    graphics.DrawLine(pen1, P1, p2);
                                    // graphics1.DrawLine(pen1, P1, p2);

                                    image1 = newImage;////PictureBox1.Image = newImage;
                                    ////PictureBox1.Update();

                                    break; // TODO: might not be correct. Was : Exit For
                                }
                                else
                                {
                                    if (k > i + 10)
                                    {
                                        bool check_small_h_Line = false;
                                        check_small_h_Line = true;
                                        for (int a = i; a <= k - 1; a++)
                                        {
                                            if (newImage.GetPixel(a, j).Name != "ffff0000")
                                            {
                                                check_small_h_Line = false;
                                                break; // TODO: might not be correct. Was : Exit For
                                            }

                                        }

                                        dynamic d = 0;
                                        //' to ensure that i-4 not < 0
                                        if (i - 4 < 0)
                                            d = i;
                                        else
                                            d = i - 4;
                                        if (check_small_h_Line & newImage.GetPixel(d, j).Name == "ff000000" & newImage.GetPixel(k, j).Name == "ff000000")
                                            check_small_h_Line = false;

                                        if (check_small_h_Line)
                                        {
                                            //'MsgBox(newImage.GetPixel(i - 2, j).Name.ToString + "+++++" + newImage.GetPixel(k, j).Name.ToString)

                                            //line_Matrix_check(input_Matrix_check, 0) = i
                                            //line_Matrix_check(input_Matrix_check, 1) = j
                                            //line_Matrix_check(input_Matrix_check, 2) = k - 1
                                            //line_Matrix_check(input_Matrix_check, 3) = j
                                            //line_Matrix_check(input_Matrix_check, 4) = 0 '' 0 means stright line, and 1 is dash line
                                            //input_Matrix_check += 1
                                        }
                                    }

                                    break; // TODO: might not be correct. Was : Exit For
                                }


                            }
                            //'End If
                        }

                    }
                    //'If newImage.GetPixel(i, j).Name = "ffffffff" Then newImage.SetPixel(i, j, Color.Red)
                    //'If newImage.GetPixel(i, j).Name <> "ffffffff" Then
                    //'If newImage.GetPixel(i, j).Name <> "ff000000" Then
                    //'str = str + Environment.NewLine + newImage.GetPixel(i, j).Name.ToString
                    ///'Exit For
                    //'End If


                }
            }
            //'TextBox3.Text = str
            // remove_Solid_H_Line();
            image1 = newImage;////PictureBox1.Image = newImage;
            ////PictureBox1.Update();
            // PictureBox11.Image = newImage103;///////////////////////
            //PictureBox11.Update();/////////////////
            //PictureBox1.Update();
            //Set_H_Dash_Line();/////////////////////////////////
            // PictureBox11.Update();///////////////////
            //  Remove();
            //remove_Boxes();


        }

        public Boolean isThisLine(int i, int j, int k, Bitmap newImage)
        {
            int wPixel = 0;
            int wPixelSec = 0;
            int bPixel = 0;
            int bPixelSec = 0;
            int b = 0;
            int c = 0;
            int cr = 0;
            int e = 0;
            int eSec = 0;
            int eThird = 0;

            int wPixel_01 = 0;
            int wPixel_01Sec = 0;
            int bPixel_01 = 0;
            int bPixel_01Sec = 0;
            int b01 = 0;
            int e01 = 0;
            int e01Sec = 0;
            int e01Third = 0;
            int startP = 0;
            int endP = 0;
            int fromWhite = 0;
            int toWhite = 0;
            //int sumB = 0;
            // int sumW = 0;
            int countW = 0;
            int countWR = 0;
            // int maxCount = 0;
            Boolean allSame = true;
            Boolean allSameR = true;
            Boolean isLine = false;

            for (b = i; b < k; b++)
            {
                if (newImage.GetPixel(b, j).Name == "ffffffff")
                {
                    wPixel = wPixel + 1;

                }
                else
                {
                    for (e = b; e < k; e++)
                    {
                        if (newImage.GetPixel(e, j).Name != "ffffffff")
                        {
                            bPixel = bPixel + 1;

                        }
                        else
                        {
                            fromWhite = e;
                            for (eSec = e; eSec < k; eSec++)
                            {
                                if (newImage.GetPixel(eSec, j).Name == "ffffffff")
                                {
                                    wPixelSec = wPixelSec + 1;

                                }
                                else
                                {
                                    startP = eSec;
                                    for (eThird = eSec; eThird < k; eThird++)
                                    {
                                        if (newImage.GetPixel(eThird, j).Name != "ffffffff")
                                        {
                                            bPixelSec = bPixelSec + 1;

                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                            break;////
                        }
                    }
                    break;
                }
            }

            for (b01 = k - 1; b01 >= b; b01--)
            {
                if (newImage.GetPixel(b01, j).Name == "ffffffff")
                {
                    wPixel_01 = wPixel_01 + 1;

                }
                else
                {
                    for (e01 = b01; e01 >= e; e01--)
                    {
                        if (newImage.GetPixel(e01, j).Name != "ffffffff")
                        {
                            bPixel_01 = bPixel_01 + 1;

                        }
                        else
                        {
                            toWhite = e01;
                            for (e01Sec = e01; e01Sec >= eSec; e01Sec--)
                            {
                                if (newImage.GetPixel(e01Sec, j).Name == "ffffffff")
                                {
                                    wPixel_01Sec = wPixel_01Sec + 1;

                                }
                                else
                                {
                                    endP = e01Sec;
                                    for (e01Third = e01Sec; e01Third >= i; e01Third--)
                                    {
                                        if (newImage.GetPixel(e01Third, j).Name != "ffffffff")
                                        {
                                            bPixel_01Sec = bPixel_01Sec + 1;

                                        }
                                        else
                                        {
                                            break;
                                        }

                                    }
                                    break;
                                }

                            }
                            break;//////
                        }
                    }
                    break;
                }
            }


            for (c = fromWhite; c <= toWhite; c++)
            {
                if (newImage.GetPixel(c, j).Name == "ffffffff")
                {
                    countW = countW + 1;

                }
                else
                {

                    if (wPixelSec == countW)
                    {
                        // maxCount = maxCount + 1;
                        for (int temp = c; temp <= toWhite; temp++)
                        {
                            c++;
                            if (newImage.GetPixel(temp, j).Name == "ffffffff")
                            {
                                //countW = 0;
                                //c = temp;
                                break;
                            }
                        }


                    }
                    else
                    {
                        // MessageBox.Show("count " +countW+" = "+wPixelSec);
                        allSame = false;
                        //countW = 0;
                        break;
                    }

                }
                if (c > (toWhite - fromWhite) / 5)
                {
                    break;
                }
            }

            for (cr = toWhite; cr >= fromWhite; cr--)
            {
                if (newImage.GetPixel(cr, j).Name == "ffffffff")
                {
                    countWR = countWR + 1;

                }
                else
                {

                    if (wPixel_01Sec == countWR)
                    {
                        // maxCount = maxCount + 1;
                        for (int tempR = cr; tempR >= fromWhite; tempR--)
                        {
                            cr--;
                            if (newImage.GetPixel(tempR, j).Name == "ffffffff")
                            {
                                //countWR = 0;
                                //cr = tempR;
                                break;
                            }
                        }


                    }
                    else
                    {
                        // MessageBox.Show("count " +countWR+" = "+wPixel_01Sec);
                        allSameR = true;// this is a problem to be fixed @henry
                        //countWR = 0;
                        break;
                    }

                }
                if (cr < (toWhite - (toWhite - fromWhite) / 5))
                {
                    break;
                }
            }
            Boolean assumedLine = false;
            if (topObjPointY == 2147483647 || topObjPointY > (newImage.Height / 3))
            {
                assumedLine = true;
            }
            else
            {
                assumedLine = hasAnObject(i, j, k);
            }


            //MessageBox.Show("count " +wPixelSec+"="+wPixel_01Sec+"and "+wPixel+"="+wPixel_01);

            if (wPixelSec == wPixel_01Sec && bPixelSec == bPixel_01Sec && bPixel == bPixel_01 && wPixelSec + bPixel + bPixelSec == wPixel_01Sec + bPixel_01 + bPixel_01Sec && allSame == true && allSameR == true && assumedLine == true)
            {
                isLine = true;
                // MessageBox.Show("count " + newImage.GetPixel(b, j - 1).Name + "-" + wPixel + "-" + wPixel01 +"isLine "+isLine);
            }

            return isLine;
        }
        public Boolean hasAnObject(int i, int j, int k)
        {
            Boolean hasObject = false;
            int startPoint = 0;
            int startPoint1 = 0;
            int endPoint = 0;
            int endPoint1 = 0;
            foreach (Lifelines objs in objLink)
            {

                if (i > objs.pointX && i < objs.pointX + objs.objWidth && objs.objCenter + 5 >= i)
                {
                    startPoint = objs.pointX;
                    startPoint1 = objs.pointX + objs.objWidth;
                    hasObject = true;
                }
                if (k - 1 > objs.pointX && k - 1 < objs.pointX + objs.objWidth && objs.objCenter - 5 <= k - 1)
                {
                    endPoint = objs.pointX;
                    endPoint1 = objs.pointX + objs.objWidth;
                    hasObject = true;
                }
            }
            if ((i >= startPoint && k - 1 <= startPoint1) || (i >= endPoint && k - 1 <= endPoint1))
            {//Ignore if the line start and ends on same object
                hasObject = false;
            }
            return hasObject;
        }
        public int topObjCorner(List<Lifelines> objData)
        {
            if (objData.Count == 0)
            {
                // throw new InvalidOperationException("Empty list");
            }
            int topEdge = int.MaxValue;
            foreach (Lifelines obj in objData)
            {
                if (obj.pointY < topEdge)
                {
                    topEdge = obj.pointY;
                }
            }
            return topEdge;
        }
        public int leftMostObjCorner(List<Lifelines> objData)
        {
            if (objData.Count == 0)
            {
                // throw new InvalidOperationException("Empty list");
            }
            int leftEdge = int.MaxValue;
            foreach (Lifelines obj in objData)
            {
                if (obj.pointX < leftEdge)
                {
                    leftEdge = obj.pointX;
                }
            }
            return leftEdge;
        }
        public int rightMostObjCorner(List<Lifelines> objData)
        {
            if (objData.Count == 0)
            {
                // throw new InvalidOperationException("Empty list");
            }
            int rightEdge = int.MinValue;
            foreach (Lifelines obj in objData)
            {
                if (obj.pointX + obj.objWidth > rightEdge)
                {
                    rightEdge = obj.pointX + obj.objWidth;
                }
            }
            return rightEdge;
        }
        public Boolean isNotBorderLine(Bitmap newImg, int i, int j, int k)
        {
            Boolean isNotBorder = false;
            if (j > topObjPointY)
            {
                if (j < newImg.Height - (newImg.Height / 4))
                {
                    isNotBorder = true;
                }
                else if (i > lefMostObjP && k < rightMostObjP)
                {
                    isNotBorder = true;
                }
            }
            else if (topObjPointY == 2147483647 || topObjPointY > (newImg.Height / 4))
            {//No object found or top objects are not captured
                isNotBorder = true;
            }
            return isNotBorder;
        }
    }
}