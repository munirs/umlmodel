﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AForge;

using AForge.Imaging;
using AForge.Imaging.Filters;
using AForge.Math.Geometry;

using System.Drawing;
using System.Drawing.Imaging;

using System.Reflection;
using System.Drawing.Drawing2D;
using UML_Project_1;
using System.IO;


namespace UML_Project_1.Classes
{

    public class Detect_Rectangle
    {
        public System.Drawing.Image imgRectangle;
        public System.Drawing.Image Tempimg;
        Bitmap newImage101;

        Bitmap newImage103;
        String getFileName = null;
        public string imgPath;
        string tempfile = null;
        public string globalText = "";

        public static string Text;
        public static string pathimg1;
        public static string pathimg11;
        public static string imgUrl1;
        public static string imgUrl11;
        //Properties. 
        protected string imgText { get; set; }
        protected string strPath { get; set; }
        int numBlob = 0;
        List<Lifelines> lifelines = new List<Lifelines>();
        List<Lifelines> lifelinesActor = new List<Lifelines>();
        Data data = new Data();
        int objIntID = 4;

        public Detect_Rectangle()
        {

            imgText = "Default title";
            strPath = "Default description.";
            //jobLength = new TimeSpan();
        }

        // Instance constructor that has three parameters. 
        public Detect_Rectangle(string desc)
        {
            //this.ID = GetNextID();
            this.imgText = GetText();
            this.strPath = desc;
            // this.jobLength = joblen;
        }
        static Detect_Rectangle()
        {
            Text = null;
        }


        public string GetText()
        {
            return Text;
        }
        public string pathImage1()
        {
            // string pathimg=null;
            return pathimg1;
        }
        public string pathImage11()
        {
            // string pathimg=null;
            return pathimg11;
        }
        public string getImageUrl11()
        {
            // string pathimg=null;
            return imgUrl11;
        }
        public string getImageUrl1()
        {
            // string pathimg=null;
            return imgUrl1;
        }
        public System.Drawing.Image getImage1()
        {
            return imgRectangle;
        }
        public System.Drawing.Image getImage11()
        {
            return Tempimg;
        }
        private void Remove()
        {

            Bitmap newImage = new Bitmap(imgRectangle);//PictureBox1.Image

            for (int f = 0; f <= (newImage.Width - 1); f++)
            {
                for (int j = 0; j <= (newImage.Height - 1); j++)
                {
                    if (newImage.GetPixel(f, j).Name != "ff008000")
                        newImage.SetPixel(f, j, Color.White);

                    //'If newImage.GetPixel(i, j).Name <> "ffffffff" Then
                    //'TextBox1.Text = newImage.GetPixel(i, j).Name
                    //'End If
                }
            }
            //'For i = 0 To (newImage.Width - 1)
            //'For j = 0 To (newImage.Height - 1)
            //'If newImage.GetPixel(i, j).Name = "ff008000" Then newImage.SetPixel(i, j, Color.Blue)
            //'If newImage.GetPixel(i, j).Name <> "ffffffff" Then
            //'TextBox1.Text = newImage.GetPixel(i, j).Name
            //'End If

            //'Next
            //'Next

            AForge.Imaging.Filters.Grayscale filter = new AForge.Imaging.Filters.Grayscale(0.11, 0.59, 0.3);
            newImage = filter.Apply(newImage);
            //PictureBox1.Image = newImage;
            //PictureBox1.Update();
            //'Dim filter1 As Filters.CannyEdgeDetector = New Filters.CannyEdgeDetector()
            //'Dim filter1 As Filters.Edges = New Filters.Edges()
            AForge.Imaging.Filters.SobelEdgeDetector filter1 = new AForge.Imaging.Filters.SobelEdgeDetector();
            newImage = filter1.Apply(newImage);
            imgRectangle = newImage;
            // PictureBox1.Image = newImage;
            //PictureBox1.Update();

            //'CountRectangle()
            // remove_Boxes(newImage);

        }

        private void remove_Boxes()
        {

            Bitmap newImage = new Bitmap(imgRectangle);
            Bitmap newImage1 = new Bitmap(newImage101);


            BlobCounter blobCounter = new BlobCounter(newImage);
            Rectangle[] rects = blobCounter.GetObjectsRectangles();

            //TextBox1.Text = "Number of rectangle: " + rects.Length.ToString

            Graphics g = Graphics.FromImage(newImage1);
            SolidBrush brush;
            if (newImage1.GetPixel(newImage.Width - 1, newImage.Height - 1) == newImage1.GetPixel(newImage.Width - 6, newImage.Height - 6))
            {
                brush = new SolidBrush(newImage1.GetPixel(newImage.Width - 1, newImage.Height - 1));
            }
            else if (newImage1.GetPixel(3, newImage.Height / 2) == newImage1.GetPixel(newImage.Width - 6, newImage.Height / 2))
            {
                brush = new SolidBrush(newImage1.GetPixel(3, newImage.Height / 2));
            }
            else {
                brush = new SolidBrush(newImage1.GetPixel(3, newImage.Height - 4));
            }

            foreach (Rectangle rc in rects)
            {
                //'TextBox1.Text = TextBox1.Text + Environment.NewLine + "Rectangle No. " + i.ToString + ": " + rects(i).Location.ToString
                //'str = AxMiDocView1.RectangleToClient(rc).ToString


                //'Dim pen As Pen = New Pen(Color.AliceBlue)

                // create filled rectangle
                g.FillRectangle(brush, rc.Location.X - 1, rc.Location.Y - 1, rc.Width + 1, rc.Height + 1);
                // PictureBox1.Image = newImage1;
                //PictureBox1.Update();

            }
            imgRectangle = newImage1;
            //  PictureBox1.Image = newImage1;
            // PictureBox1.Update();


            BitmapData bitmapData = newImage1.LockBits(new Rectangle(0, 0, newImage1.Width, newImage1.Height), ImageLockMode.ReadWrite, newImage1.PixelFormat);
            ColorFiltering colorFilter = new ColorFiltering();

            colorFilter.Red = new IntRange(0, 64);
            colorFilter.Green = new IntRange(0, 64);
            colorFilter.Blue = new IntRange(0, 64);
            colorFilter.FillOutsideRange = false;

            colorFilter.ApplyInPlace(bitmapData);

            BlobCounter blobCounter1 = new BlobCounter();

            blobCounter1.FilterBlobs = true;
            blobCounter1.MinHeight = 5;
            blobCounter1.MinWidth = 5;

            blobCounter1.ProcessImage(bitmapData);
            Blob[] blobs = blobCounter1.GetObjectsInformation();
            newImage1.UnlockBits(bitmapData);
            SimpleShapeChecker shapeChecker = new SimpleShapeChecker();

            //'Dim g As Graphics = Graphics.FromImage(newImage1)
            Graphics g1 = Graphics.FromImage(newImage103);

            dynamic blackPen = new Pen(newImage1.GetPixel(newImage.Width - 1, newImage.Height - 1), 4);

            int i = 0;
            int n = blobs.Length;


            //'Dim brush As New SolidBrush(Color.Black)
            SolidBrush brush1; //= new SolidBrush(newImage1.GetPixel(newImage.Width - 1, newImage.Height - 1));
            if (newImage1.GetPixel(newImage.Width - 1, newImage.Height - 1) == newImage1.GetPixel(newImage.Width - 6, newImage.Height - 6))
            {
                brush1 = new SolidBrush(newImage1.GetPixel(newImage.Width - 1, newImage.Height - 1));
            }
            else if (newImage1.GetPixel(3, newImage.Height / 2) == newImage1.GetPixel(newImage.Width - 6, newImage.Height / 2))
            {
                brush1 = new SolidBrush(newImage1.GetPixel(3, newImage.Height / 2));
            }
            else
            {
                brush1 = new SolidBrush(newImage1.GetPixel(3, newImage.Height - 4));
            }
            while (i < n)
            {

                List<IntPoint> edgePoints = blobCounter1.GetBlobsEdgePoints(blobs[i]);

                //DoublePoint center = default(DoublePoint);
                AForge.Point center;
                //double radius = 0;
                float radius = 0;

                //is Circle ??
                if (shapeChecker.IsCircle(edgePoints, out center, out radius))
                {
                    //'g.DrawEllipse(yellowPen, CSng(center.X - radius), CSng(center.Y - radius), CSng(radius * 2), CSng(radius * 2))

                }
                else
                {
                    List<IntPoint> corners = null;

                    // is triangle or quadrilateral
                    if (shapeChecker.IsConvexPolygon(edgePoints, out corners))
                    {
                        //get sub-type
                        PolygonSubType subType = shapeChecker.CheckPolygonSubType(corners);
                        Pen pen = null;

                        if ((subType == PolygonSubType.Trapezoid) | (corners.Count > 4))
                        {
                            //'If corners.Count = 4 Then
                            pen = blackPen;
                            //'pen = whitePen


                            g.DrawPolygon(pen, ToPointsArray(corners));
                            g1.DrawPolygon(pen, ToPointsArray(corners));



                            //'If ((corners(0).X <> 0) And (corners(0).Y <> 0) And (subType = PolygonSubType.Rectangle)) Then
                            //'Dim brush As New SolidBrush(Color.Brown)
                            //'If ((corners(0).X > 10) And (corners(0).Y > 10)) Then
                            g.FillPolygon(brush1, ToPointsArray(corners));
                            imgRectangle = newImage1;
                            //PictureBox1.Image = newImage1;
                            // PictureBox1.Update();

                            //'ElseIf (subType <> PolygonSubType.Unknown And Math.Abs(corners(0).X - corners(1).X) > 20) Then

                            //'To delete Notes, any shape not sequare or rectangle ..

                            //'pen = greenPen
                            //'g.DrawPolygon(pen, ToPointsArray(corners))
                            //'Dim brush As New SolidBrush(Color.Green)
                            //'g.FillPolygon(brush, ToPointsArray(corners))

                            //'End delete notes ...

                        }

                    }
                }



                i += 1;
            }



        }
        //Detect rectangle function
        public int getBlobs()
        {
            return numBlob;
        }
        public void button4_Click()
        {
            Text = ""; // initialization to null

            // path of the file
            imgRectangle = System.Drawing.Image.FromFile(strPath);
            //} else { 
            // string imgfile = 
            // imgRectangle = System.Drawing.Image.FromFile(strPath);//@"C:\Users\Munir\Documents\Visual Studio 2012\Projects\UML_Project_1\UML_Project_1\hello.jpg");//imgRectangle);//1
            //Tempimg = imgRectangle;

            // 'detect rectangle
            Bitmap newimage10 = new Bitmap(imgRectangle);//@"C:\Users\Munir\Documents\Visual Studio 2012\Projects\UML_Project_1\UML_Project_1\Sequence-Diagram-Sample1.png");//imgRectangle);//1
            // tempimg = pbox11
            Tempimg = imgRectangle;

            AForge.Imaging.Filters.Grayscale filter1 = new AForge.Imaging.Filters.Grayscale(0.11, 0.59, 0.3);
            newimage10 = filter1.Apply(newimage10);
            imgRectangle = newimage10;

            newImage101 = new Bitmap(imgRectangle);
            newImage103 = new Bitmap(Tempimg);
            object assembly = this.GetType().Assembly;

            Bitmap newImage_Shape = new Bitmap(imgRectangle);
            Bitmap newImage_Shape01 = new Bitmap(Tempimg);
            Graphics g2 = Graphics.FromImage(newImage_Shape);

            g2.FillRectangle(Brushes.White, 0, 0, newImage_Shape.Width, newImage_Shape.Height);

            AForge.Imaging.Filters.GaussianSharpen filter2 = new AForge.Imaging.Filters.GaussianSharpen();

            for (int s = 0; s <= 4; s++)
            {
                newImage_Shape01 = filter2.Apply(newImage_Shape01);/////////////////////////////////
            }
            AForge.Imaging.Filters.Grayscale filter = new AForge.Imaging.Filters.Grayscale(0.11, 0.59, 0.3);
            newImage_Shape01 = filter.Apply(newImage_Shape01);
            AForge.Imaging.Filters.Threshold filter6 = new AForge.Imaging.Filters.Threshold(200);
            newImage_Shape01 = filter6.Apply(newImage_Shape01);

            Tempimg = newImage_Shape01;
            // PictureBox11.Update();

            Bitmap newImage = new Bitmap(Tempimg);
            BitmapData bitmapData = newImage.LockBits(new Rectangle(0, 0, newImage.Width, newImage.Height), ImageLockMode.ReadWrite, newImage.PixelFormat);
            ColorFiltering colorFilter = new ColorFiltering();
            colorFilter.Red = new IntRange(0, 64);
            colorFilter.Green = new IntRange(0, 64);
            colorFilter.Blue = new IntRange(0, 64);
            colorFilter.FillOutsideRange = false;
            colorFilter.ApplyInPlace(bitmapData);
            BlobCounter blobCounter = new BlobCounter();
            blobCounter.FilterBlobs = true;
            blobCounter.MinHeight = 5;
            blobCounter.MinWidth = 5;
            blobCounter.ProcessImage(bitmapData);
            Blob[] blobs = blobCounter.GetObjectsInformation();// GetObjectsInformation;
            newImage.UnlockBits(bitmapData);
            SimpleShapeChecker shapeChecker = new SimpleShapeChecker();
            Graphics g = Graphics.FromImage(newImage);
            Graphics g1 = Graphics.FromImage(newImage103);
            Pen yellowPen = new Pen(Color.Yellow, 2);
            Pen redPen = new Pen(Color.Red, 4);
            Pen brownPen = new Pen(Color.Brown, 4);
            Pen greenPen = new Pen(Color.Green, 4);
            Pen bluePen = new Pen(Color.Blue, 4);
            Pen whitePen = new Pen(Color.White, 4);
            int i = 0;
            int n = blobs.Length;
            numBlob = blobs.Length;
            SolidBrush brush = new SolidBrush(Color.Green);
            SolidBrush brush1 = new SolidBrush(Color.Brown);
            List<IntPoint> corners = null;


            while ((i < n))
            {
                List<IntPoint> edgePoints = blobCounter.GetBlobsEdgePoints(blobs[i]);
                //DoublePoint center = default(DoublePoint); replaced by AForge.Point center;
                AForge.Point center;
                float radius = 0; // was double
                int cpixel = 0;
                // is Circle ??
                
                if (shapeChecker.IsCircle(edgePoints, out center, out radius))
                {

                    int y = (int)(center.Y + radius);
                    int cent = (int)center.X;
                    for (int c = y; c < 10 + y; c++)
                    {

                        if (newImage.GetPixel(cent, (c + 2)).Name == "ff000000")
                        {
                            cpixel++;
                            if (cpixel > 9)
                            {
                                // add list
                                // check the length of v- line
                                lifelinesData((int)(center.X - 20), (int)(center.Y - 20), 70, 85);// w, h
                                g.DrawEllipse(greenPen, (float)(center.X - radius), (float)(center.Y - radius), (float)(radius * 2), (float)(radius * 2));
                                g1.DrawEllipse(greenPen, (float)(center.X - radius), (float)(center.Y - radius), (float)(radius * 2), (float)(radius * 2));
                                g1.DrawLine(greenPen, (float)(center.X ), (float)(center.Y + radius), (float)(center.X ), (float)(center.Y + radius)+15);
                                break;
                            }
                        }
                        else
                        {
                            cpixel = 0;
                        }
                    }
                    

                }
                else
                {
                    //List<IntPoint> corners = null;
                    //  is triangle or quadrilateral
                    if (shapeChecker.IsConvexPolygon(edgePoints, out corners))
                    {
                        // get sub-type
                        PolygonSubType subType = shapeChecker.CheckPolygonSubType(corners);
                        Pen pen;

                        if (((subType == PolygonSubType.Square)
                                    || (subType == PolygonSubType.Rectangle)))
                        {
                            // 'If corners.Count = 4 Then
                            // 'If corners(0).X = corners(3).X And corners(0).Y = corners(1).Y And corners(2).X = corners(1).X And corners(2).Y = corners(3).Y Then
                            Rectangle rect = new Rectangle(Math.Min(corners[0].X, corners[2].X), Math.Min(corners[0].Y, corners[2].Y), Math.Abs((corners[0].X - corners[1].X)), Math.Abs((corners[0].Y - corners[3].Y)));

                            // crop image and extract text
                            int cropx = Math.Min(corners[0].X, corners[2].X);
                            int cropy = Math.Min(corners[0].Y, corners[2].Y);
                            int cropwidth = Math.Abs((corners[0].X - corners[1].X));
                            int cropheight = Math.Abs((corners[0].Y - corners[3].Y));

                            // CropImageRectangle(imgRectangle, cropx, cropy, cropwidth, cropheight);              


                            if (((((rect.X != 0) || (rect.Y != 0))
                                 && ((rect.Width < (newImage.Width - 40)) || (rect.Height < (newImage.Height-40))))
                                        || (n == 1)))
                            {
                                //textBox2.Text = "";

                                // 'If (subType = PolygonSubType.Square) Or (subType = PolygonSubType.Rectangle) Then
                                if ((Math.Abs(corners[0].X - corners[1].X) > 20) && (Math.Abs(corners[0].X - corners[1].X) < newImage.Width/2) &&
                                    (Math.Abs(corners[0].Y - corners[3].Y) < newImage.Height/2 ))
                                {

                                    try
                                    {
                                        if (corners[0].X - 3 < 0 && corners[0].Y - 3 < 0)
                                        {
                                            if ((newImage.GetPixel(corners[1].X + 3, corners[1].Y - 1).Name == "ffffffff") &&
                                                                         (newImage.GetPixel(corners[2].X + 1, corners[2].Y + 3).Name == "ffffffff" &&
                                                                         newImage.GetPixel(corners[2].X + 3, corners[2].Y + 1).Name == "ffffffff") &&
                                                                         (newImage.GetPixel(corners[3].X - 1, corners[3].Y + 3).Name == "ffffffff"))
                                            {

                                                pen = greenPen;

                                                g.DrawPolygon(pen, ToPointsArray(corners));
                                                g1.DrawPolygon(pen, ToPointsArray(corners));

                                                g.FillPolygon(brush, ToPointsArray(corners));
                                                // PictureBox11.Image = newImage;
                                                // PictureBox11.Update();

                                                lifelinesData(cropx, cropy, cropwidth, cropheight);

                                            }
                                        }
                                        else if (newImage.Width - 3 < corners[1].X && corners[1].Y - 3 < 0)
                                        {
                                            if ((newImage.GetPixel(corners[0].X - 3, corners[0].Y - 1).Name == "ffffffff") &&
                                                                         (newImage.GetPixel(corners[2].X + 1, corners[2].Y + 3).Name == "ffffffff") &&
                                                                         (newImage.GetPixel(corners[3].X - 1, corners[3].Y + 3).Name == "ffffffff" &&
                                                                         newImage.GetPixel(corners[3].X - 3, corners[3].Y + 1).Name == "ffffffff"))
                                            {

                                                pen = greenPen;

                                                g.DrawPolygon(pen, ToPointsArray(corners));
                                                g1.DrawPolygon(pen, ToPointsArray(corners));

                                                g.FillPolygon(brush, ToPointsArray(corners));
                                                //PictureBox11.Image = newImage;
                                                // PictureBox11.Update();*/
                                                lifelinesData(cropx, cropy, cropwidth, cropheight);
                                            }
                                        }
                                        else if (newImage.Width - 3 < corners[2].X && newImage.Height - 3 < corners[2].Y)
                                        {
                                            if ((newImage.GetPixel(corners[0].X - 1, corners[0].Y - 3).Name == "ffffffff" &&
                                                                         newImage.GetPixel(corners[0].X - 3, corners[0].Y - 1).Name == "ffffffff") &&
                                                                             (newImage.GetPixel(corners[1].X + 1, corners[1].Y - 3).Name == "ffffffff") &&
                                                                         (newImage.GetPixel(corners[3].X - 3, corners[3].Y + 1).Name == "ffffffff"))
                                            {

                                                pen = greenPen;

                                                g.DrawPolygon(pen, ToPointsArray(corners));
                                                g1.DrawPolygon(pen, ToPointsArray(corners));

                                                g.FillPolygon(brush, ToPointsArray(corners));
                                                //PictureBox11.Image = newImage;
                                                // PictureBox11.Update();*/
                                                lifelinesData(cropx, cropy, cropwidth, cropheight);

                                            }
                                        }
                                        else if (corners[3].X - 3 < 0 && newImage.Height - 3 < corners[3].Y)
                                        {
                                            if ((newImage.GetPixel(corners[0].X - 1, corners[0].Y - 3).Name == "ffffffff") &&
                                                                                                                    (newImage.GetPixel(corners[1].X + 1, corners[1].Y - 3).Name == "ffffffff" &&
                                                                                                                newImage.GetPixel(corners[1].X + 3, corners[1].Y - 1).Name == "ffffffff") &
                                                                                                                (newImage.GetPixel(corners[2].X + 3, corners[2].Y + 1).Name == "ffffffff"))
                                            {

                                                pen = greenPen;

                                                g.DrawPolygon(pen, ToPointsArray(corners));
                                                g1.DrawPolygon(pen, ToPointsArray(corners));

                                                g.FillPolygon(brush, ToPointsArray(corners));
                                                //PictureBox11.Image = newImage;
                                                //PictureBox11.Update();*/
                                                lifelinesData(cropx, cropy, cropwidth, cropheight);

                                            }
                                        }
                                        else if (corners[0].Y - 3 < 0)
                                        {
                                            if ((newImage.GetPixel(corners[0].X - 3, corners[0].Y - 1).Name == "ffffffff") &&
                                                                                                                (newImage.GetPixel(corners[1].X + 3, corners[1].Y - 1).Name == "ffffffff") &&
                                                                                                                (newImage.GetPixel(corners[2].X + 1, corners[2].Y + 3).Name == "ffffffff" &&
                                                                                                                newImage.GetPixel(corners[2].X + 3, corners[2].Y + 1).Name == "ffffffff") &&
                                                                                                                (newImage.GetPixel(corners[3].X - 1, corners[3].Y + 3).Name == "ffffffff" &&
                                                                                                                newImage.GetPixel(corners[3].X - 3, corners[3].Y + 1).Name == "ffffffff"))
                                            {

                                                pen = greenPen;

                                                g.DrawPolygon(pen, ToPointsArray(corners));
                                                g1.DrawPolygon(pen, ToPointsArray(corners));

                                                g.FillPolygon(brush, ToPointsArray(corners));
                                                //PictureBox11.Image = newImage;
                                                //PictureBox11.Update();*/
                                                lifelinesData(cropx, cropy, cropwidth, cropheight);
                                            }
                                        }
                                        else if (corners[0].X - 3 < 0)
                                        {
                                            if ((newImage.GetPixel(corners[0].X - 1, corners[0].Y - 2).Name == "ffffffff") &&

                                                                                                                    (newImage.GetPixel(corners[1].X + 1, corners[1].Y - 3).Name == "ffffffff" &&
                                                                                                                newImage.GetPixel(corners[1].X + 3, corners[1].Y - 1).Name == "ffffffff") &&
                                                                                                                (newImage.GetPixel(corners[2].X + 1, corners[2].Y + 3).Name == "ffffffff" &&
                                                                                                                newImage.GetPixel(corners[2].X + 3, corners[2].Y + 1).Name == "ffffffff") &&
                                                                                                                (newImage.GetPixel(corners[3].X - 1, corners[3].Y + 3).Name == "ffffffff"))
                                            {

                                                pen = greenPen;

                                                g.DrawPolygon(pen, ToPointsArray(corners));
                                                g1.DrawPolygon(pen, ToPointsArray(corners));

                                                g.FillPolygon(brush, ToPointsArray(corners));
                                                // PictureBox11.Image = newImage;
                                                //PictureBox11.Update();*/
                                                lifelinesData(cropx, cropy, cropwidth, cropheight);
                                            }
                                        }
                                        else if (newImage.Width - 3 < corners[2].X)
                                        {
                                            if ((newImage.GetPixel(corners[0].X - 1, corners[0].Y - 3).Name == "ffffffff" &&
                                                                                                                newImage.GetPixel(corners[0].X - 3, corners[0].Y - 1).Name == "ffffffff") &&
                                                                                                                    (newImage.GetPixel(corners[1].X + 1, corners[1].Y - 3).Name == "ffffffff") &&
                                                                                                                (newImage.GetPixel(corners[2].X + 1, corners[2].Y + 3).Name == "ffffffff") &&
                                                                                                                (newImage.GetPixel(corners[3].X - 1, corners[3].Y + 3).Name == "ffffffff" &&
                                                                                                                newImage.GetPixel(corners[3].X - 3, corners[3].Y + 1).Name == "ffffffff"))
                                            {

                                                pen = greenPen;

                                                g.DrawPolygon(pen, ToPointsArray(corners));
                                                g1.DrawPolygon(pen, ToPointsArray(corners));

                                                g.FillPolygon(brush, ToPointsArray(corners));
                                                //  PictureBox11.Image = newImage;
                                                // PictureBox11.Update();
                                                lifelinesData(cropx, cropy, cropwidth, cropheight);
                                            }
                                        }
                                        else if (newImage.Height - 3 < corners[2].Y)
                                        {
                                            if ((newImage.GetPixel(corners[0].X - 1, corners[0].Y - 2).Name == "ffffffff" &&
                                                                                                                newImage.GetPixel(corners[0].X - 3, corners[0].Y - 1).Name == "ffffffff") &&
                                                                                                                    (newImage.GetPixel(corners[1].X + 1, corners[1].Y - 3).Name == "ffffffff" &&
                                                                                                                newImage.GetPixel(corners[1].X + 3, corners[1].Y - 1).Name == "ffffffff") &&
                                                                                                                (newImage.GetPixel(corners[2].X + 3, corners[2].Y + 1).Name == "ffffffff") &&
                                                                                                                (newImage.GetPixel(corners[3].X - 3, corners[3].Y + 1).Name == "ffffffff"))
                                            {

                                                pen = greenPen;

                                                g.DrawPolygon(pen, ToPointsArray(corners));
                                                g1.DrawPolygon(pen, ToPointsArray(corners));

                                                g.FillPolygon(brush, ToPointsArray(corners));
                                                //PictureBox11.Image = newImage;
                                                //PictureBox11.Update();
                                                lifelinesData(cropx, cropy, cropwidth, cropheight);
                                            }
                                        }
                                        else
                                        {
                                            if ((newImage.GetPixel(corners[0].X - 1, corners[0].Y - 3).Name == "ffffffff" &&
                                                                         newImage.GetPixel(corners[0].X - 3, corners[0].Y - 1).Name == "ffffffff") &&
                                                                             (newImage.GetPixel(corners[1].X + 1, corners[1].Y - 3).Name == "ffffffff" &&
                                                                         newImage.GetPixel(corners[1].X + 3, corners[1].Y - 1).Name == "ffffffff") &&
                                                                         (newImage.GetPixel(corners[2].X + 1, corners[2].Y + 3).Name == "ffffffff" &&
                                                                         newImage.GetPixel(corners[2].X + 3, corners[2].Y + 1).Name == "ffffffff") &&
                                                                         (newImage.GetPixel(corners[3].X - 1, corners[3].Y + 3).Name == "ffffffff" &&
                                                                         newImage.GetPixel(corners[3].X - 3, corners[3].Y + 1).Name == "ffffffff"))
                                            {

                                                pen = greenPen;

                                                g.DrawPolygon(pen, ToPointsArray(corners));
                                                g1.DrawPolygon(pen, ToPointsArray(corners));

                                                g.FillPolygon(brush, ToPointsArray(corners));
                                                //PictureBox11.Image = newImage;
                                                //PictureBox11.Update();*/
                                                lifelinesData(cropx, cropy, cropwidth, cropheight);
                                            }
                                        }
                                    }
                                    catch (ArgumentOutOfRangeException)
                                    {
                                        //MessageBox.Show("Failed to recognise an object, click okay to skip");
                                        pen = greenPen;

                                        g.DrawPolygon(pen, ToPointsArray(corners));
                                        g1.DrawPolygon(pen, ToPointsArray(corners));

                                        g.FillPolygon(brush, ToPointsArray(corners));
                                        //PictureBox11.Image = newImage;
                                        //PictureBox11.Update();*/
                                        ////CropImageRectangle(imgRectangle, cropx, cropy, cropwidth, cropheight);
                                        lifelinesData(cropx, cropy, cropwidth, cropheight);
                                    }


                                }
                            }
                        }


                    }
                }
                i++;
            }
            yellowPen.Dispose();
            redPen.Dispose();
            greenPen.Dispose();
            bluePen.Dispose();
            brownPen.Dispose();
            g.Dispose();

            //
            imgRectangle = newImage;
            Tempimg = newImage103;

            // getting file name
            // put new image to clipboard
            // 'Clipboard.SetDataObject(newImage)
            // and to picture box
            /////// picturebox1 = newImage;
            // PictureBox11.Image = newImage103;
            //PictureBox1.Update();
            // PictureBox11.Update();

            Remove();
            remove_Boxes();
            //black backrd image
                pathimg11 = SaveDrawnImage(Tempimg, "imageRect.jpg");// "Rect.jpg";
                imgUrl11 = pathimg11.Substring(pathimg11.LastIndexOf('\\') + 1);

                pathimg1 = SaveDrawnImage(imgRectangle, "imageRect1.jpg");
                imgUrl1 = pathimg1.Substring(pathimg1.LastIndexOf('\\') + 1);

            // detect_Horizental_Line()
            // detect_vertical_line()*/


        }
        public void lifelinesData(int cropx, int cropy, int cropwidth, int cropheight)
        {
            String objID = "UMLObject." + (++objIntID);
            tempfile = data.CropImageRectangle(imgRectangle, cropx, cropy, cropwidth, cropheight, strPath);
            String ocrOut = data.OCR(tempfile);
            String objectName = null;
            if (ocrOut != null)
            {
                objectName = ocrOut;
            }
            else
            {
                objectName = "Not Captured";
            }

            Lifelines lifeL = new Lifelines(objectName, cropx, cropy, cropwidth, cropheight, cropx + cropwidth / 2, imgRectangle.Height, objID);
            lifelines.Add(lifeL);
        }
        private static PointF[] ToPointsArray(List<IntPoint> points)
        {
            PointF[] array = new PointF[points.Count];
            for (int i = 0, n = points.Count; i < n; i++)
            {
                array[i] = new PointF(points[i].X, points[i].Y);
            }

            return array;
        }
        public List<Lifelines> getLifelines()
        {
            return lifelines;
        }
        public List<Lifelines> getActorLifelines()
        {
            return lifelinesActor;
        }
        public void tt()
        {
            for (int i = 0; i < 5; i++)
            {
                if (i == 2)
                    hello();
                if (i == 4) { hello(); }
            }
        }
        private void hello()
        {
            Text = Text + " ok ";
        }

        public String SaveDrawnImage(System.Drawing.Image imagefile, string str)
        {
            string file = null;

            Data data = new Data();
            MemoryStream stream = new MemoryStream();
            byte[] image = data.imageToByteArray(imagefile);
            stream.Write(image, 0, image.Length);
            Bitmap bitmap = new Bitmap(stream);

            //string root = Server.MapPath(filename + ".jpg");
            string folder = strPath.Remove(strPath.LastIndexOf('\\') + 1);
            string filename = str;// "alu.jpg";//strPath.Substring(strPath.LastIndexOf('\\') + 1);
            string ab = data.GetUniqueFilename(folder, filename);
            bitmap.Save(folder + ab);//@"C:\Users\Munir\Documents\Visual Studio 2012\Projects\UML_Project_1\UML_Project_1\munir11.jpg");
            bitmap.Dispose();
            file = folder + ab;// strPath;//@"C:\Users\Munir\Documents\Visual Studio 2012\Projects\UML_Project_1\UML_Project_1\munir11.jpg";*/
            /*Directory.CreateDirectory("images");
            string path = Path.Combine(Environment.CurrentDirectory, @"images\image.jpg");
           //imagefile.(path, System.Drawing.Imaging.ImageFormat.Jpeg);
            file = @"images\image.jpg";
            */

            return file;// folder + ab;
            /*Directory.CreateDirectory("images");
           string path = Path.Combine(Environment.CurrentDirectory, @"images\image.jpg"; 
           theImage.Save(path, System.Drawing.Imaging.ImageFormat.Jpeg);?
           */


        }


    }
}