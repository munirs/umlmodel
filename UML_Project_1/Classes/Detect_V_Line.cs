﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AForge;

using AForge.Imaging;
using AForge.Imaging.Filters;
using AForge.Math.Geometry;

using System.Reflection;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;

namespace UML_Project_1.Classes
{
    public class Detect_V_Line
    {
        System.Drawing.Image image1;
        System.Drawing.Image image11;
        Bitmap newImage103;
        protected string strPath { get; set; }

        public Detect_V_Line(String strPath) 
        {
            this.strPath = strPath;
        }
        private void detect_Vertical_Line()
        {
            image1 = System.Drawing.Image.FromFile(strPath);
            image11 = System.Drawing.Image.FromFile(strPath);
            Bitmap newImage = new Bitmap(image1);
            newImage103 = new Bitmap(image11);

            dynamic P1 = default(System.Drawing.Point);
            dynamic p2 = default(System.Drawing.Point);
            System.Drawing.Point P3 = new System.Drawing.Point();

            //'TextBox3.Text = ""


            string str = "";
            Graphics graphics = Graphics.FromImage(newImage);
            Graphics graphics1 = Graphics.FromImage(newImage103);
            SolidBrush brush = new SolidBrush(Color.Yellow);
            Pen pen = new Pen(brush);
            Pen pen1 = new Pen(brush, 5);

            for (int i = 0; i <= (newImage.Width - 1); i++)
            {
                for (int j = 0; j <= (newImage.Height - 1); j++)
                {
                    if (newImage.GetPixel(i, j).Name == "ffffffff")
                    {
                        for (int k = j + 1; k <= (newImage.Height - 1); k++)
                        {
                            //If input_Matrix_check <= 495 Then
                            if (newImage.GetPixel(i, k).Name != "ffffffff")
                            {
                                if (k > j + 20 && k - j < newImage.Height - 200)//65
                                {
                                    P1.X = i;
                                    P1.Y = j;
                                    p2.X = i;
                                    p2.Y = k - 1;

                                    //'####################### Line Matrix ###################

                                    //line_Matrix(input_Line_Matrix, 0) = P1.X
                                    //line_Matrix(input_Line_Matrix, 1) = P1.Y
                                    //line_Matrix(input_Line_Matrix, 2) = p2.X
                                    //line_Matrix(input_Line_Matrix, 3) = p2.Y
                                    //line_Matrix(input_Line_Matrix, 4) = 0 '' 0 means stright line, and 1 is dash line
                                    //input_Line_Matrix += 1

                                    //'####################### Line Matrix ###################

                                    graphics.DrawLine(pen1, P1, p2);
                                    graphics1.DrawLine(pen1, P1, p2);

                                    image1 = newImage;
                                    ////PictureBox1.Image = newImage;
                                    ////PictureBox1.Update();
                                    break; // TODO: might not be correct. Was : Exit For
                                }
                                else
                                {
                                    if (k > j + 10 && k - j < newImage.Height - 200)
                                    {
                                        bool check_small_h_Line = false;
                                        check_small_h_Line = true;
                                        for (int a = j; a <= k - 1; a++)
                                        {
                                            if (newImage.GetPixel(i, a).Name != "ffffffff")
                                            {
                                                check_small_h_Line = false;
                                                break; // TODO: might not be correct. Was : Exit For
                                            }

                                        }

                                        if (check_small_h_Line)
                                        {
                                            //'MsgBox(newImage.GetPixel(k + 1, j).Name.ToString + "+++++" + newImage.GetPixel(i - 1, j).Name.ToString)
                                            //line_Matrix_check(input_Matrix_check, 0) = i
                                            //line_Matrix_check(input_Matrix_check, 1) = j
                                            //line_Matrix_check(input_Matrix_check, 2) = i
                                            //line_Matrix_check(input_Matrix_check, 3) = k - 1
                                            //line_Matrix_check(input_Matrix_check, 4) = 0 '' 0 means stright line, and 1 is dash line
                                            //input_Matrix_check += 1
                                        }
                                    }

                                    break; // TODO: might not be correct. Was : Exit For
                                }


                            }

                            //End If
                        }

                    }
                    //'If newImage.GetPixel(i, j).Name = "ffffffff" Then newImage.SetPixel(i, j, Color.Red)
                    //'If newImage.GetPixel(i, j).Name <> "ffffffff" Then
                    //'If newImage.GetPixel(i, j).Name <> "ff000000" Then
                    //'str = str + Environment.NewLine + newImage.GetPixel(i, j).Name.ToString
                    ///'Exit For
                    //'End If


                }
            }
            //'TextBox3.Text = str
            image1 = newImage;
            image11 = newImage103;
            ////PictureBox1.Image = newImage;
            ////PictureBox1.Update();
            ////PictureBox11.Image = newImage103;
            ////PictureBox11.Update();
            // PictureBox1.Update();
            //PictureBox11.Update();

            // remove_Vline();

        }
    }
}