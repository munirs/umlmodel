﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UML_Project_1.Classes
{
    public class Lifelines
    {
        public String objName { get; set; }
        public int pointX { get; set; }
        public int pointY { get; set; }
        public int objWidth { get; set; }
        public int objHeight { get; set; }
        public int objCenter { get; set; }
        public int lifelineHeight { get; set; }
        public String objID { get; set; }
        public Lifelines(String objName, int pointX, int pointY, int objWidth, int objHeight, int objCenter, int lifelineHeight, String objID)
        {
            this.objName = objName;
            this.pointX = pointX;
            this.pointY = pointY;
            this.objWidth = objWidth;
            this.objHeight = objHeight;
            this.objCenter = objCenter;
            this.lifelineHeight = lifelineHeight;
            this.objID = objID;
        }
    }
}