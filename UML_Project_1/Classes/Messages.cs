﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UML_Project_1.Classes
{
    public class Messages
    {
        public String msgName { get; set; }
        public int msgStartX { get; set; }
        public int msgStartY { get; set; }
        public int msgEndX { get; set; }
        public int msgEndY { get; set; }
        public int msgWidth { get; set; }
        public int msgHeight { get; set; }
        public String startObjName { get; set; }
        public String endObjName { get; set; }
        public String msgType { get; set; }
        public String msgID { get; set; }
        public String startObjID { get; set; }
        public String endObjID { get; set; }
        public int startObjCenter { get; set; }
        public int endObjCenter { get; set; }
        public Messages(String msgName, int msgStartX, int msgStartY, int msgEndX, int msgEndY,int msgWidth,int msgHeight, String startObjName, String endObjName, String msgType, String msgID, String startObjID, String endObjID, int startObjCenter, int endObjCenter)
        {
            this.msgName = msgName;
            this.msgStartX = msgStartX;
            this.msgStartY = msgStartY;
            this.msgEndX = msgEndX;
            this.msgEndY = msgEndY;
            this.msgWidth = msgWidth;
            this.msgHeight = msgHeight;
            this.startObjName = startObjName;
            this.endObjName = endObjName;
            this.msgType = msgType;
            this.msgID = msgID;
            this.startObjID = startObjID;
            this.endObjID = endObjID;
            this.startObjCenter = startObjCenter;
            this.endObjCenter = endObjCenter;
        }
    }
}