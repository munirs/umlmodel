﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
namespace UML_Project_1.Classes
{
    public class XmiWriter
    {
       

        public XmiWriter(int id, string firstName, string lastName, int salary)
        {
            /*this._id = id;
            this._firstName = firstName;
            this._lastName = lastName;
            this._salary = salary;*/
        }

        public XmiWriter()
        {
            // TODO: Complete member initialization
        }

        
        public void getObject(XmlWriter writer, String  ab1, String msg1, string msg2, string xmid, int x, Boolean className, string classid)
        {

            writer.WriteStartElement("UML", "ClassifierRole", null);
            writer.WriteAttributeString("xmi.id", xmid);
            writer.WriteAttributeString("name", ab1 ); // name of the rect "Object1"
            writer.WriteAttributeString("visibility", "public");
            writer.WriteAttributeString("isSpecification", "false");
            if(className==true){
            writer.WriteAttributeString("base", classid );
            }
            // if message
            writer.WriteAttributeString("message2", msg1);
            writer.WriteAttributeString("message1", msg2);
            
            writer.WriteAttributeString("isRoot", "false");
            writer.WriteAttributeString("isLeaf", "false");
            writer.WriteAttributeString("isAbstract", "false");

            writer.WriteStartElement("UML", "ClassifierRole.multiplicity", null);
            writer.WriteStartElement("UML", "Multiplicity", null);
            writer.WriteAttributeString("xmi.id", "X."+x);// x.15

            writer.WriteStartElement("UML", "Multiplicity.range", null);

            writer.WriteStartElement("UML", "MultiplicityRange", null);
            writer.WriteAttributeString("xmi.id", "X."+(x+1));//x.16
            writer.WriteAttributeString("lower", "1");
            writer.WriteAttributeString("upper", "1");
            writer.WriteAttributeString("multiplicity", "X."+x);// x.15

            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();

        }

        public void ReturnAction(XmlWriter writer,  string UMLStimulusID, string msg,   string senderxmid, string receiverxmid,string UMLReturnActionID)
        {
           
                writer.WriteStartElement("UML", "Message", null);
                writer.WriteAttributeString("xmi.id", UMLStimulusID);
                writer.WriteAttributeString("name", msg); // name of the rect
                writer.WriteAttributeString("visibility", "public");
                writer.WriteAttributeString("isSpecification", "false");
                writer.WriteAttributeString("sender", senderxmid);
                writer.WriteAttributeString("receiver", receiverxmid);
                writer.WriteAttributeString("interaction", "UMLInteractionInstanceSet.4");
            
                writer.WriteStartElement("UML", "Message.action", null);

                writer.WriteStartElement("UML", "ReturnAction", null);
                writer.WriteAttributeString("xmi.id", UMLReturnActionID );
                writer.WriteAttributeString("name", "");
                writer.WriteAttributeString("visibility", "public");
                writer.WriteAttributeString("isSpecification", "false");
                writer.WriteAttributeString("isAsynchronous", "false");
                writer.WriteAttributeString("stimulus", UMLStimulusID);


                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndElement();
               

        }
        public void CallAction(XmlWriter writer, string UMLStimulusID,  string msg, string senderxmid, string receiverxmid,string UMLCallActionID)
        {

            
            writer.WriteStartElement("UML", "Message", null);
            writer.WriteAttributeString("xmi.id", UMLStimulusID);
            writer.WriteAttributeString("name",  msg); // name of the rect
            writer.WriteAttributeString("visibility", "public");
            writer.WriteAttributeString("isSpecification", "false");
            writer.WriteAttributeString("sender", senderxmid);
            writer.WriteAttributeString("receiver", receiverxmid);
            writer.WriteAttributeString("interaction", "UMLInteractionInstanceSet.4");

            writer.WriteStartElement("UML", "Message.action", null);

            writer.WriteStartElement("UML", "CallAction", null);
            writer.WriteAttributeString("xmi.id", UMLCallActionID );
            writer.WriteAttributeString("name", "");
            writer.WriteAttributeString("visibility", "public");
            writer.WriteAttributeString("isSpecification", "false");
            writer.WriteAttributeString("isAsynchronous", "false");
            writer.WriteAttributeString("stimulus", UMLStimulusID);
            
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();


        }
        public void SendAction(XmlWriter writer, string UMLStimulusID, string msg, string senderxmid, string receiverxmid, string UMLSendActionID)
        {


            writer.WriteStartElement("UML", "Message", null);
            writer.WriteAttributeString("xmi.id", UMLStimulusID);
            writer.WriteAttributeString("name", msg); // name of the rect
            writer.WriteAttributeString("visibility", "public");
            writer.WriteAttributeString("isSpecification", "false");
            writer.WriteAttributeString("sender", senderxmid);
            writer.WriteAttributeString("receiver", receiverxmid);
            writer.WriteAttributeString("interaction", "UMLInteractionInstanceSet.4");

            writer.WriteStartElement("UML", "Message.action", null);

            writer.WriteStartElement("UML", "SendAction", null);
            writer.WriteAttributeString("xmi.id", UMLSendActionID);
            writer.WriteAttributeString("name", "");
            writer.WriteAttributeString("visibility", "public");
            writer.WriteAttributeString("isSpecification", "false");
            writer.WriteAttributeString("isAsynchronous", "false");
            writer.WriteAttributeString("stimulus", UMLStimulusID);
            
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();


        }
        public void CreateAction(XmlWriter writer, string UMLStimulusID, string msg, string senderxmid, string receiverxmid, string UMLCreateActionID)
        {


            writer.WriteStartElement("UML", "Message", null);
            writer.WriteAttributeString("xmi.id", UMLStimulusID);
            writer.WriteAttributeString("name", msg); // name of the rect
            writer.WriteAttributeString("visibility", "public");
            writer.WriteAttributeString("isSpecification", "false");
            writer.WriteAttributeString("sender", senderxmid);
            writer.WriteAttributeString("receiver", receiverxmid);
            writer.WriteAttributeString("interaction", "UMLInteractionInstanceSet.4");

            writer.WriteStartElement("UML", "Message.action", null);

            writer.WriteStartElement("UML", "CreateAction", null);
            writer.WriteAttributeString("xmi.id", UMLCreateActionID);
            writer.WriteAttributeString("name", "");
            writer.WriteAttributeString("visibility", "public");
            writer.WriteAttributeString("isSpecification", "false");
            writer.WriteAttributeString("isAsynchronous", "false");
            writer.WriteAttributeString("stimulus", UMLStimulusID);


            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();


        }
        public void DestroyAction(XmlWriter writer, string UMLStimulusID, string msg, string senderxmid, string receiverxmid, string UMLDestroyActionID)
        {


            writer.WriteStartElement("UML", "Message", null);
            writer.WriteAttributeString("xmi.id", UMLStimulusID);
            writer.WriteAttributeString("name", msg); // name of the rect
            writer.WriteAttributeString("visibility", "public");
            writer.WriteAttributeString("isSpecification", "false");
            writer.WriteAttributeString("sender", senderxmid);
            writer.WriteAttributeString("receiver", receiverxmid);
            writer.WriteAttributeString("interaction", "UMLInteractionInstanceSet.4");

            writer.WriteStartElement("UML", "Message.action", null);

            writer.WriteStartElement("UML", "DestroyAction", null);
            writer.WriteAttributeString("xmi.id", UMLDestroyActionID);
            writer.WriteAttributeString("name", "");
            writer.WriteAttributeString("visibility", "public");
            writer.WriteAttributeString("isSpecification", "false");
            writer.WriteAttributeString("isAsynchronous", "false");
            writer.WriteAttributeString("stimulus", UMLStimulusID);


            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();


        }

        public void classNameAction(XmlWriter writer, string classname,  string xmiID)
        {
           // xmiID = 15 + xmiID;
            writer.WriteStartElement("UML", "Class", null);
            writer.WriteAttributeString("xmi.id", xmiID);//15+i
            writer.WriteAttributeString("name", classname); // name of the rect
           
            writer.WriteAttributeString("visibility", "public");
            writer.WriteAttributeString("isSpecification", "false");
            writer.WriteAttributeString("namespace", "UMLModel.2");
             writer.WriteAttributeString("isRoot", "false");
          writer.WriteAttributeString("isLeaf", "false");
          writer.WriteAttributeString("isAbstract", "false");
           writer.WriteAttributeString("isActive", "false");
            writer.WriteEndElement();
        }
        public void StereotypeAction(XmlWriter writer, string typename, string msg, int xmiID)
        {
            xmiID = 15 + xmiID;
            writer.WriteStartElement("UML", "Stereotype", null);
            writer.WriteAttributeString("xmi.id", "X." + xmiID);//15+i
            writer.WriteAttributeString("name", typename); // name of the rect
            writer.WriteAttributeString("extendedElement", msg);
            writer.WriteEndElement();
        }
        public void DiagramElement(XmlWriter writer, string xmid, string geometry, string style, string UMLObeject)
        {
            
            writer.WriteStartElement("UML", "DiagramElement", null);
              writer.WriteAttributeString("xmi.id", xmid);
              writer.WriteAttributeString("geometry",geometry );
              writer.WriteAttributeString("style", style);
              writer.WriteAttributeString("subject", UMLObeject);
            writer.WriteEndElement();
        }
    }

}