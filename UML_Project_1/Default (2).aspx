﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="index._Default" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1><%: Title %>Modify this template to jump-start your ASP.NET application.</h1>
            </hgroup>
            <p>
                To learn more about ASP.NET, visit <a href="http://asp.net" title="ASP.NET Website">http://asp.net</a>.
                The page features<a href="http://forums.asp.net/18.aspx"> </a><a href="http://forums.asp.net/18.aspx" title="ASP.NET Forum">forums</a>.
            </p>
        </div> 
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
   
     <p>
        <asp:DropDownList ID="DropDownList1" runat="server" Width="180px">
            <asp:ListItem>Class Diagram</asp:ListItem>
            <asp:ListItem>Sequence Diagram</asp:ListItem>
        </asp:DropDownList>
    </p>
    
    <table  style="width: auto; border: #999999 1px solid; table-layout:fixed;">
               <tr>
            <td style="border: #999999 1px solid; margin-top:0px;" class="auto-style12" >  
                <asp:TextBox ID="TextBox1" runat="server" Width="150px">Search</asp:TextBox>
            </td>
            <td style="border: #999999 1px solid; text-align:left" class="auto-style8" >
                  <asp:FileUpload ID="UMLFileUpload" runat="server" Width="266px"  />

                  &nbsp;&nbsp;
                  <asp:Button ID="Upload" runat="server" OnClick="Upload_Click" Text="Go" />
                  <br />
                <asp:Label  ID="Label1"  runat="server"  Font-Bold="True"  ForeColor="#000099"></asp:Label>

            </td>
            <td style="border: #999999 1px solid;" class="auto-style7" ></td>
               <td style="border: #999999 1px solid;" class="auto-style11">
                <asp:Label  ID="Label2"  runat="server"  Font-Bold="True"  ForeColor="#000099"></asp:Label>
                   <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" />

            </td>
        </tr>
        <tr>
            <td style="border: #999999 1px solid; "> 
                 <Div  style ="Height:500px;Width:470px;Overflow:Auto; margin-top:-10px;">         
                <asp:DataList ID="DataList1" runat="server" RepeatColumns="4" RepeatDirection="Horizontal" BackColor="White" Width="55px" Height="501px"  >
                     <ItemTemplate >
                            
                                 <asp:Image ID="Image1" runat="server"  ImageUrl='<%# "ImageHandler.ashx?ImID="+ Eval("Id") %>' Height="85px" Width="85px"/>
                                 <asp:HyperLink ID="HyperLink1" Text='<%# Bind("Image") %>' NavigateUrl='<%# Bind("Image", "~/Images/{0}") %>' runat="server" 
                                   Font-Underline="false" Font-Names="arial" Font-Bold="false" Font-Size="10pt" ForeColor="#336699" Width="100%"/> 
                                                 <!--a href="Edit.aspx?id=<%# "ImageHandler.ashx?ImID="+ Eval("Image") %>">Edit</a-->
                          
                       </ItemTemplate>
                </asp:DataList>
               </div>
                            
                           
            </td>
            <td style="border: #999999 1px solid;" class="auto-style8" >
                <asp:RequiredFieldValidator ID="valFileUpload1" ControlToValidate="UMLFileUpload" ErrorMessage="You must select a file first." runat="server" Display="Dynamic" Font-Bold="True" />
                  <br />
                <asp:CustomValidator ID="valInvalidFile" runat="server" ErrorMessage="The file you uploaded doesn't appear to be a valid image." Display="Dynamic" Font-Bold="True"></asp:CustomValidator>
                <br />
                 <asp:Image ID="Image1" runat="server" Height="425px" style="margin-top: 0px" Width="349px" />
                <br />
                 <br />
                <br />
                <br />
                 <br />
                <asp:CheckBox ID="image_chkBox_local"  Text="Save " runat="server" />
                <asp:CheckBox ID="image_chkBox_Db" runat="server" Text="Upload" TextAlign="Left" />
                
                <br />
            </td>
            <td style="border: #999999 1px solid;" class="auto-style7" > <asp:Button ID="btnResult" runat="server" CommandName="MoveNext" Text=" XML " OnClick="btnResult_Click" Width="45px" /></td>
            <td style="border: #999999 1px solid;" class="auto-style11" >
                <asp:Image ID="Image2" runat="server" Height="423px" Width="325px" />
                <br />
                <br />
                <asp:CheckBox ID="xmi_chkBox_local" runat="server" Text="Save " />
                <br />
                <asp:CheckBox ID="xmi_chkBox_Db" runat="server" Text="Upload" />
                <asp:Button ID="Button3" runat="server" Text="Button" />
                <br />
                <br />
            </td>

        </tr>
    </table>
    <!--<ol class="round">
        <li class="one">
            <h5>Getting Started</h5>
            ASP.NET Web Forms lets you build dynamic websites using a familiar drag-and-drop, event-driven model.
            A design surface and hundreds of controls and components let you rapidly build sophisticated, powerful UI-driven sites with data access.
            <a href="http://go.microsoft.com/fwlink/?LinkId=245146">Learn more…</a>
        </li>
        <li class="two">
            <h5>Add NuGet packages and jump-start your coding</h5>
            NuGet makes it easy to install and update free libraries and tools.
            <a href="http://go.microsoft.com/fwlink/?LinkId=245147">Learn more…</a>
        </li>
        <li class="three">
            <h5>Find Web Hosting</h5>
            You can easily find a web hosting company that offers the right mix of features and price for your applications.
            <a href="http://go.microsoft.com/fwlink/?LinkId=245143">Learn more…</a>
        </li>
    </ol> -->
       
</asp:Content>
<asp:Content ID="Content1" runat="server" contentplaceholderid="HeadContent">
    <style type="text/css">
        .auto-style7 {
            width: 45px;
        }
        .auto-style8 {
            width: 358px;
        }
        .auto-style11 {
            width: 323px;
        }
        .auto-style12 {
            width: 278px;
        }
    </style>
</asp:Content>

