﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="index._Default" %>
 
<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1><%: Title %> Sequence diagram.</h1>
            </hgroup>
            
        </div> 
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
   
          
     <!DOCTYPE HTML>
  
    <html>
    <header>
       
        <style type="text/css">
            #div1, #div2, #div3 {
                float: left;
                width: 450px;
                height: 135px;
                margin: 10px;
                padding: 10px;
                border: 1px solid #aaaaaa;
            }

            #Id {
                height: 485px;
                width: 482px;
            }
      



            .auto-style1 {
                width: 585px;
            }
      



            .auto-style2 {
                width: 541px;
            }
      



            #Img1 {
                width: 164px;
            }
      



        </style>
        <script>
            function allowDrop(ev) {
                ev.preventDefault();
            }

            function drag(ev) {
                ev.dataTransfer.setData("Text", ev.target.id);
            }

            function drop(ev) {
                ev.preventDefault();
                var data = ev.dataTransfer.getData("Text");
               // data.innerHTML = '<img width="500" src="' + ev.target.id + '" />';
                ev.target.appendChild(document.getElementById(data));

               

            }
        </script>
        
    
       
    </header>
    <body>

        <!--div id="div1" ondrop="drop(event)" ondragover="allowDrop(event)">

  <img src="img_w3slogo.gif"  draggable="true" ondragstart="drag(event)" id="drag1" width="88" height="31">

</div-->
        
        
      
        <table border="1">
            <tr >

                <td class="auto-style1" style="flex-align:center; margin-left:15px; " >

                    <asp:DropDownList ID="DropDownList1" runat="server" Width="275px" Height="18px"  >
                        <asp:ListItem>Class Diagram</asp:ListItem>
                        <asp:ListItem>Sequence Diagram</asp:ListItem>
                    </asp:DropDownList>

                </td>
                    
                <td class="auto-style2" >
                  <div>
            
                  <asp:FileUpload ID="UMLFileUpload" runat="server" Width="400px" />

                    <asp:Button ID="Upload" runat="server" OnClick="Upload_Click" Text="Upload" />
                    
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="#000099"></asp:Label>

                   <!-- <asp:RequiredFieldValidator ID="valFileUpload1" ControlToValidate="UMLFileUpload" ErrorMessage="You must select a file first." runat="server" Display="Dynamic" Font-Bold="True" />-->
                    
                    <asp:CustomValidator ID="valInvalidFile" runat="server" ErrorMessage="The file you uploaded doesn't appear to be a valid image." Display="Dynamic" Font-Bold="True"></asp:CustomValidator>
                    </div>
                </td>

            </tr>
            <tr>
                <td class="auto-style1">
                    <div id="div3" style="Height: 510px; Width: 507px; Overflow: Auto; ">
                        <asp:DataList ID="DataList1" runat="server" RepeatColumns="4" RepeatDirection="Horizontal" BackColor="Black" Width="500px" Height="500px"  >
                            <ItemTemplate>
                                <div draggable="true" ondragstart="drag(event)" id="drag1" >
                                    
                                        <asp:ImageButton ID="Image1" runat="server"  draggable="true" ImageUrl='<%# "ImageHandler.ashx?ImID="+ Eval("Id") %>' Height="100px" Width="100px"   OnClick="Image1_Click"  /> 
                                      
                                    <!--asp:HyperLink ID="HyperLink1" Text='>%# Bind("Image") %>' NavigateUrl='>%# Bind("Image", "~/Images/{0}") %>' runat="server" CommandName="imageClick" OnItemCommand="Dlist_ItemClick",CommandName="imageClick"
                                        Font-Underline="false" Font-Names="arial" Font-Bold="false" Font-Size="10pt" ForeColor="#336699" Width="100%" /-->
                                    <!--a href="Edit.aspx?id=<!%# "ImageHandler.ashx?ImID="+ Eval("Image") %>"> OnClick="Image11_Click" Edit</a-->
                                </div>
                            </ItemTemplate>
                        </asp:DataList>
                    </div>

                </td>
                 
                <td class="auto-style2">                    
                    <asp:UpdateProgress ID="UpdateProgress2" runat="server" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="2000" DynamicLayout ="true">
                    <ProgressTemplate>
                    <center></center>
                    </ProgressTemplate>
                    </asp:UpdateProgress>

                    <div id="div2" ondrop="drop(event)" ondragover="allowDrop(event)" style="height:522px; width:500px; margin-top:10px;">
                        <asp:Image runat="server"  ID="Image11"  Visible="false" style="margin-top: 0px"  OnClick="Image11_Click"/>
                    </div>
                                  

                    &nbsp;&nbsp;&nbsp;&nbsp;
                   
                  <asp:Button ID="GenerateXmi" runat="server" OnClick="GenerateXmi_Click" Text="Extract XMI" />

                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                  

                    <asp:Button ID="Submit" runat="server" OnClick="Submit_Click" Text="Save Image" />
                                      

                    <img id="Img1" src="~/Images/progressBar.jpg" runat="server" visible="false"   /><asp:Label ID="Label3" runat="server" Text="Label" Visible="False"></asp:Label>
                    <asp:HyperLink   
                                        ID="HyperLink1"   
                                        runat="server"  
                                        Text="xmi link"  
                                        NavigateUrl="~/XmiPage.aspx" Visible="False"  
                                        ></asp:HyperLink>  

                    <asp:Button ID="Downloadbtn" runat="server" OnClick="Downloadbtn_Click" Text="Dowload XMI" />

                </td>
                <td> 
                    
                    <br />
                    <br>
                   
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                    <center>
                    <div>
                        <br />
                     <asp:Label ID ="label2" runat ="server"></asp:Label>
                    </div>
                    </center>
                    </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            
        </table>
        
        <br />
        
    </body>
    </html>
       
                     <asp:TextBox ID="TextBox1" runat="server" Height="21px" Width="206px" TextMode="MultiLine" Visible="False"></asp:TextBox>
        
        </asp:Content>
<asp:Content ID="Content1" runat="server" contentplaceholderid="HeadContent">
    </asp:Content>

