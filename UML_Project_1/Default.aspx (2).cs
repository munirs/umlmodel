﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using AForge;
using AForge.Imaging;
using AForge.Imaging.Filters;
using System.Configuration;


namespace index
{
    public partial class _Default : Page
    {
        string filename;
        string strcon ="Data Source=.\\sqlexpress;Initial Catalog=TestDB;Integrated Security=True";// ConfigurationManager.ConnectionStrings["TestDBConnectionString"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SqlConnection connection = new SqlConnection(strcon);
                SqlCommand command = new SqlCommand("SELECT Id,Image from [SequenceDiagramTb]", connection);
                SqlDataAdapter daimages = new SqlDataAdapter(command);
                DataTable dt = new DataTable();
                daimages.Fill(dt);
                //

                //context.Response.BinaryWrite);

                DataList1.DataSource = dt;// dataManager.GetEmployees();
                DataList1.DataBind();
               // BindGridData();
            }
        }


        protected void Upload_Click(object sender, EventArgs e)
        {


            Page.Validate();
            if (Page.IsValid)
            {
                if (UMLFileUpload.HasFile)
                {
                    string FileExtension = Path.GetExtension(UMLFileUpload.PostedFile.FileName);
                    switch (FileExtension.ToLower())
                    {
                        // Only allow uploads that look like images.
                        case ".jpg":
                        case ".jpeg":
                        case ".gif":
                        case ".bmp":
                        case ".png":
                            try
                            {
                                if (UMLFileUpload.PostedFile.ContentLength < 512000)
                                {
                                    //Data Source=.\sqlexpress;Initial Catalog=TestDB;Integrated Security=True
                                    filename = Path.GetFileName(UMLFileUpload.FileName);
                                    UMLFileUpload.SaveAs(Server.MapPath("~/") + filename);
                                    Label1.Text = filename;
                                    Image1.ImageUrl = filename;
                                    //getting length of uploaded file
                                    int length = UMLFileUpload.PostedFile.ContentLength;
                                    //create a byte array to store the binary image data
                                    byte[] imgbyte = new byte[length];
                                    //store the currently selected file in memeory
                                    HttpPostedFile img = UMLFileUpload.PostedFile;
                                    //set the binary data

                                    img.InputStream.Read(imgbyte, 0, length);

                                    SqlConnection con = new SqlConnection(strcon);//"Data Source=.\\sqlexpress;Initial Catalog=TestDB;Integrated Security=True");//ConfigurationManager.ConnectionStrings["TestDBConnectionString"].ToString());
                                    //Open the database connection
                                    con.Open();
                                    //Query to insert images path and name into database
                                    SqlCommand cmd = new SqlCommand("Insert into SequenceDiagramTb (Name,Image,Image_1) VALUES (@Name, @Image,@Image_1)", con);

                                    cmd.Parameters.Add("@Name", SqlDbType.VarChar, 50).Value = filename;
                                    cmd.Parameters.Add("@Image", SqlDbType.VarChar, 50).Value = "Test_munir";
                                    cmd.Parameters.Add("@Image_1", SqlDbType.Image).Value = imgbyte;

                                    int count = cmd.ExecuteNonQuery();
                                    con.Close();
                                    if (count == 1)
                                    {
                                         BindGridData();
                                       // txtImageName.Text = string.Empty;
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alert('" + filename + " image inserted successfully')", true);
                                    }
                                    /*SqlDataSource1.InsertCommandType = SqlDataSourceCommandType.Text;
                                    SqlDataSource1.InsertCommand = "Insert into SequenceDiagramTb (Name,Image,Image_1) VALUES (@Name, @Image,@Image_1)";

                                    SqlDataSource1.InsertParameters.Add("Name", filename);
                                    SqlDataSource1.InsertParameters.Add("Image","Test1" );

                                    //SqlDataSource1.InsertParameters.Add("@imagedata", imgbyte);

                                    SqlDataSource1.Insert();
                                    */
                                   


                                }else                
                                Label1.Text = "File maximum size is 500 Kb";

                                /*string fileName = Path.GetFileName(UMLFileUpload.PostedFile.FileName);
                                string saveAsName = Path.Combine(Server.MapPath("~/Uploads/"), fileName);
                                UMLFileUpload.PostedFile.SaveAs(saveAsName);
                                Image1.ImageUrl = filename;*/
                            }
                            catch
                            {
                                // Unable to read the file dimensions. The uploaded file is probably not an image.
                                valInvalidFile.IsValid = false;
                            }
                            break;

                        default: // The uploaded file has an incorrect extension
                            valInvalidFile.IsValid = false;
                            break;
                    }
            
                    /* if (UMLFileUpload.PostedFile.ContentType == "image/jpeg")
                     {
                         if (UMLFileUpload.PostedFile.ContentLength < 512000)
                         {
                             filename = Path.GetFileName(UMLFileUpload.FileName);
                             UMLFileUpload.SaveAs(Server.MapPath("~/") + filename);
                             Label1.Text =  filename;
                             Image1.ImageUrl = filename;

                             Int32 count;

                             Int32 i;

                       

                        
                         }
                         // else                
                         //Label1.Text = "File maximum size is 500 Kb";
                     }*/
                    // else                
                    //Label1.Text = "Only JPEG files are accepted!";

                }
            }
        }

        private void show()
        {
            {
                //HttpContext context;// = new HttpContext();
                //string imageid =  HttpContext.Request.QueryString["ImID"];
                SqlConnection connection = new SqlConnection(strcon);
                connection.Open();
               /* SqlCommand command = new SqlCommand("select Image from Image where ImageID=" + imageid, connection);
                SqlDataReader dr = command.ExecuteReader();
                dr.Read();
                context.Response.BinaryWrite((Byte[])dr[0]);
                connection.Close();
                context.Response.End();*/

              string connString = "Data Source=.\\sqlexpress;Initial Catalog=TestDB;Integrated Security=True";// @"server =MUNIR-HP\sqlexpress;integrated security = true;";
                  
              string strSQL = "Select Name, Image, Image_1 from SequenceDiagramTb";
              SqlConnection conn = new SqlConnection(connString);

              try
              {
                 conn.Open();
                 SqlDataAdapter da = new SqlDataAdapter(strSQL, conn);
                 DataSet ds = new DataSet();

                 da.Fill(ds, "SequenceDiagramTb");
                                 
                conn.Close();
                //GridView4.DataSource = ds;
                //GridView4.DataBind();
               // GridView4.Attributes.Add("bordercolor", "black");
              }
              catch (Exception e)
              {
                 // Console.WriteLine("Error: " + e);
              }
            }
        }

        private void BindGridData()
        {
            SqlConnection connection = new SqlConnection(strcon);
            SqlCommand command = new SqlCommand("SELECT Id,Image from [SequenceDiagramTb]", connection);
            SqlDataAdapter daimages = new SqlDataAdapter(command);
            DataTable dt = new DataTable();
            daimages.Fill(dt);
            //
           
            //context.Response.BinaryWrite);

            DataList1.DataSource = dt;// dataManager.GetEmployees();
            DataList1.DataBind();
           

            connection.Close();
            //
              //dt.Rows[i][1].ToString();
            //GridView4.DataSource = dt;
            //GridView4.DataBind();
            //GridView4.Attributes.Add("bordercolor", "black");
        }

        protected void btnResult_Click(object sender, EventArgs e)
        {
            Image2.ImageUrl =  Label1.Text;
            
        }

       
        protected void Button1_Click(object sender, EventArgs e)
        {
            string strconn = "SERVER=localhost;DATABASE=sequencediagramdb;UID=root;";
           // MySqlConnection con = new MySqlConnection(strconn);
        }

       

        protected void GridView4_SelectedIndexChanged1(object sender, EventArgs e)
        {
            //Label2.Text = GridView3.SelectedRow.Cells[1].Text;
           // Label1.Text = GridView3.SelectedRow.Cells[2].Text;
        }

       

        
    }
}//