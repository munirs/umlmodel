﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using AForge;
using AForge.Imaging;
using AForge.Imaging.Filters;
using System.Configuration;
using UML_Project_1.Classes;
using System.Drawing;
using System.Drawing.Imaging;
using System.Xml;
using System.Text.RegularExpressions;

namespace index
{
    public partial class _Default : Page
    {
        public string glob = null;
        string filename;
        public static string imgName;
        public static string strdowload = null;
        public static bool forceDownload = false;
        public static string imgserverPath;
        public static bool userUpload;
        public static bool UploadFromDb;
        string strcon = "Data Source=.\\sqlexpress;Initial Catalog=TestDB;Integrated Security=True";// ConfigurationManager.ConnectionStrings["TestDBConnectionString"].ConnectionString;
        public string tempText = null;
        public static string SaveFile = null;
        public static HttpPostedFile imghttpPostedFile;
        public static List<Messages> allMessages;
        public static List<Lifelines> objNamesMiss;
        public List<Messages> allMessages1;
        String physicalPathimg1;
        String physicalPathimg11;
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                if (!IsPostBack)
                {
                    SqlConnection connection = new SqlConnection(strcon);
                    SqlCommand command = new SqlCommand("SELECT Id,Image from [SequenceDiagramTb]", connection);
                    SqlDataAdapter daimages = new SqlDataAdapter(command);
                    DataTable dt = new DataTable();
                    daimages.Fill(dt);

                    DataList1.DataSource = dt;// dataManager.GetEmployees();
                    DataList1.DataBind();
                    TextBox1.Text = "";

                    // BindGridData();
                }
            }
            catch { }
            //Submit.Visible = false;
            //Downloadbtn.Visible = false;

        }
        
        protected void Upload_Click(object sender, EventArgs e)
        {
            Image11.Visible = true;

            Page.Validate();
            if (Page.IsValid)
            {
                if (UMLFileUpload.HasFile)
                {
                    _Default.SaveFile = UMLFileUpload.PostedFile.FileName;

                     imgName = UMLFileUpload.FileName;
                    string imgPath = "TempImages/" + imgName;

                    _Default.imghttpPostedFile = UMLFileUpload.PostedFile;

                    Data data = new Data();
                    string root = Server.MapPath(imgPath); ;
                    string folder = root.Remove(root.LastIndexOf('\\') + 1);

                    String uniquefile = data.GetUniqueFilename(folder, imgName);
                    imgPath = uniquefile;
                    // bitmap.Save(uniquefile);//filename + ".jpeg")

                    UMLFileUpload.SaveAs(Server.MapPath("TempImages/" + imgPath));
                    Image11.ImageUrl = "~/TempImages/" + imgPath;
                    // Server.MapPath(imgPath) -delete from current directory
                    /* if (Server.MapPath(imgPath) != null || Server.MapPath(imgPath) != string.Empty)
                     {
                         if ((System.IO.File.Exists(Server.MapPath(imgPath))))
                         {
                            // System.IO.File.Delete(Server.MapPath(imgPath));
                         }

                     }*/
                    _Default.imgserverPath = Server.MapPath("TempImages/" + imgPath);
                    _Default.userUpload = true;
                    _Default.UploadFromDb = false;
                    //Response.Write("1 "+imgserverPath);

                }
            }
        }

        private void SubmitToDataBase()
        {

            string FileExtension = Path.GetExtension(SaveFile);
            switch (FileExtension.ToLower())
            {
                // Only allow uploads that look like images.
                case ".jpg":
                case ".jpeg":
                case ".gif":
                case ".bmp":
                case ".png":
                case ".tif":
                    try
                    {
                        //
                        FileInfo file2 = new FileInfo(imgserverPath);
                        long filesize2 = file2.Length;
                        // Response.Write(filesize2.ToString());
                        if (filesize2 < 512000)//UMLFileUpload.PostedFile.ContentLength
                        {
                            //Data Source=.\sqlexpress;Initial Catalog=TestDB;Integrated Security=True
                            // filename = Path.GetFileName(UMLFileUpload.FileName);
                            // UMLFileUpload.SaveAs(Server.MapPath("TempImages/") + filename);
                            // Label1.Text = SaveFile; //filename;

                            //getting length of uploaded file
                            int length = (int)filesize2;//UMLFileUpload.PostedFile.ContentLength;
                            //create a byte array to store the binary image data
                            byte[] imgbyte = new byte[length];
                            //set the binary data
                            imghttpPostedFile.InputStream.Read(imgbyte, 0, length);

                            SqlConnection con = new SqlConnection(strcon);//"Data Source=.\\sqlexpress;Initial Catalog=TestDB;Integrated Security=True");//ConfigurationManager.ConnectionStrings["TestDBConnectionString"].ToString());
                            //Open the database connection
                            con.Open();
                            //Query to insert images path and name into database
                            SqlCommand cmd = new SqlCommand("Insert into SequenceDiagramTb (Name,Image,Image_1) VALUES (@Name, @Image,@Image_1)", con);

                            cmd.Parameters.Add("@Name", SqlDbType.VarChar, 50).Value = SaveFile;
                            cmd.Parameters.Add("@Image", SqlDbType.VarChar, 50).Value = "Image name"; ///////// we have to change this
                            cmd.Parameters.Add("@Image_1", SqlDbType.Image).Value = imgbyte;

                            int count = cmd.ExecuteNonQuery();
                            con.Close();
                            if (count == 1)
                            {
                                DisplayData();
                                // txtImageName.Text = string.Empty;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "alertmessage", "javascript:alert('" + filename + " image inserted successfully')", true);
                            }
                            /*SqlDataSource1.InsertCommandType = SqlDataSourceCommandType.Text;
                            SqlDataSource1.InsertCommand = "Insert into SequenceDiagramTb (Name,Image,Image_1) VALUES (@Name, @Image,@Image_1)";

                            SqlDataSource1.InsertParameters.Add("Name", filename);
                            SqlDataSource1.InsertParameters.Add("Image","Test1" );

                            //SqlDataSource1.InsertParameters.Add("@imagedata", imgbyte);

                            SqlDataSource1.Insert();
                            */



                        }
                        else
                            Label1.Text = "File maximum size is 500 Kb";

                        /*string fileName = Path.GetFileName(UMLFileUpload.PostedFile.FileName);
                        string saveAsName = Path.Combine(Server.MapPath("~/Uploads/"), fileName);
                        UMLFileUpload.PostedFile.SaveAs(saveAsName);
                        Image1.ImageUrl = filename;*/
                    }
                    catch
                    {
                        // Unable to read the file dimensions. The uploaded file is probably not an image.
                        // valInvalidFile.IsValid = false;
                    }
                    break;

                default: // The uploaded file has an incorrect extension
                    // valInvalidFile.IsValid = false;
                    break;
            }

        }
        private void show()
        {
            {
                //HttpContext context;// = new HttpContext();
                //string imageid =  HttpContext.Request.QueryString["ImID"];
                SqlConnection connection = new SqlConnection(strcon);
                connection.Open();
                /* SqlCommand command = new SqlCommand("select Image from Image where ImageID=" + imageid, connection);
                 SqlDataReader dr = command.ExecuteReader();
                 dr.Read();
                 context.Response.BinaryWrite((Byte[])dr[0]);
                 connection.Close();
                 context.Response.End();*/

                string connString = "Data Source=.\\sqlexpress;Initial Catalog=TestDB;Integrated Security=True";// @"server =MUNIR-HP\sqlexpress;integrated security = true;";

                string strSQL = "Select Name, Image, Image_1 from SequenceDiagramTb";
                SqlConnection conn = new SqlConnection(connString);

                try
                {
                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(strSQL, conn);
                    DataSet ds = new DataSet();

                    da.Fill(ds, "SequenceDiagramTb");

                    conn.Close();
                    //GridView4.DataSource = ds;
                    //GridView4.DataBind();
                    // GridView4.Attributes.Add("bordercolor", "black");
                }
                catch (Exception e)
                {
                    // Console.WriteLine("Error: " + e);
                }
            }
        }

        private void DisplayData()
        {
            SqlConnection connection = new SqlConnection(strcon);
            SqlCommand command = new SqlCommand("SELECT Id,Image from [SequenceDiagramTb]", connection);
            SqlDataAdapter daimages = new SqlDataAdapter(command);
            DataTable dt = new DataTable();
            daimages.Fill(dt);
            //

            //context.Response.BinaryWrite);

            DataList1.DataSource = dt;// dataManager.GetEmployees();
            DataList1.DataBind();


            connection.Close();
            //
            //dt.Rows[i][1].ToString();
            //GridView4.DataSource = dt;
            //GridView4.DataBind();
            //GridView4.Attributes.Add("bordercolor", "black");
        }

        public void Image1_Click(object sender, ImageClickEventArgs e)
        {
            //int strId = Convert.ToInt32(DataList1.DataKeys[e.Item.ItemIndex].ToString());

            System.Web.UI.WebControls.Image ib = (System.Web.UI.WebControls.Image)sender;
            Image11.ImageUrl = ib.ImageUrl;

            Image11.Visible = true;
            // Response.Write(Image11.Height.ToString());
            //if ((int)Image11. < 150 && Image11.Height <150)
            {
                //Image11.Height = 500;
                Image11.Width = 480;
            }
            _Default.UploadFromDb = true;
            _Default.userUpload = false;
            //    int strId = Convert.ToInt32(DataList1.DataKeys[e.Item.ItemIndex].ToString());
            // Response.Write("image " + strId.ToString());
            // GridViewRow gvrow = imgbtn.NamingContainer as GridViewRow;
            //Find Image button in gridview
            //ImageButton imgbtntxt = (ImageButton)ds.FindControl("imgbtn");
            //Assign imagebutton url to image field in lightbox
             //Image11.ImageUrl = imgbtn.ImageUrl;
        }
        
        public void GenerateXmi_Click(object sender, EventArgs e)
        {
            //  Img1.visible = true;
            // Img1ProgressBar.Visible = true;
            try
            {
                //label2.Text = "Page refreshed at" + DateTime.Today.ToLongDateString() + "" + DateTime.Now.ToLongTimeString();
                string tr = null, str = null, str2 = null;

                String uniquefile = null;
                Detect_Rectangle dt = new Detect_Rectangle();
                Data data = new Data();
                if (userUpload)
                {

                    UploadFromDb = false;
                    Detect_Rectangle dt1 = new Detect_Rectangle(imgserverPath);
                    dt1.button4_Click();
                    String physicalPathimg1 = dt1.pathImage1();
                    String physicalPathimg11 = dt1.pathImage11();
                    List<Lifelines> objNames = dt1.getLifelines();
                    TextBox1.Text = string.Empty;
                    //foreach (Lifelines obj in objNames)
                    {
                        //Response.Write("lifelines: " + obj.objName+","+obj.pointX+","+obj.pointY+","+obj.objWidth+","+obj.objHeight+"\n");
                        //TextBox1.Text = TextBox1.Text + "\n" + obj.objName + "," + obj.pointX + "," + obj.pointY + "," + obj.objWidth + "," + obj.objHeight + "," + obj.objCenter + "," + obj.lifelineHeight + "," + obj.objID + "\n";
                    }

                    // to get the directory
                    string root = Server.MapPath("TempImages/1.jpg");
                    string folder = root.Remove(root.LastIndexOf('\\') + 1);
                    // data.CleanFiles(folder);


                    uniquefile = physicalPathimg1; //Server.MapPath(@"TempImages/" + data.GetUniqueFilename(folder, ));
                    //string str21 = @uniquefile;
                    
                    // need to filter
                    MemoryStream stream = new MemoryStream();
                    System.Drawing.Image img = System.Drawing.Image.FromFile(uniquefile);
                    byte[] image = data.imageToByteArray(img);
                    stream.Write(image, 0, image.Length);
                    using (Bitmap bitmap = new Bitmap(stream))//@"C:\\Users\\Munir\\documents\\visual studio 2012\\Projects\\UML_Project_1\\UML_Project_1\\TempImages\\Img 1 org (8)..JPG");
                    {
                        data.CleanFiles(folder);
                        //  bitmap.Save(uniquefile);//filename + ".jpeg")
                        //bitmap.Dispose();
                    }
                    // dt1.imgPath = uniquefile;//Server.MapPath(GetUniqueFilename(folder,filename+".jpg"));//Server.MapPath(filename + ".jpeg");

                    //}
                    imgserverPath = uniquefile;


                    Detect_Lines dl = new Detect_Lines(dt1.pathImage1(), dt1.pathImage11(), dt1.getImage1(), dt1.getImage11(), imgserverPath, objNames);
                    dl.detect_H_Solid_Line();
                    allMessages = dl.getMessages();
                    TextBox1.Text = string.Empty;

                    objNamesMiss = dl.getLifelineMissObj();

                    {
                        //Response.Write(myMsg.msgName + "\n");
                        //Response.Write("lifelines: " + obj.objName+","+obj.pointX+","+obj.pointY+","+obj.objWidth+","+obj.objHeight+"\n");
                        //TextBox1.Text = TextBox1.Text + "\n" + myMsg.msgName + "," + myMsg.msgStartX + "," + myMsg.msgStartY + "," + myMsg.msgEndX + "," + myMsg.msgEndY + "," + myMsg.msgWidth + "," + myMsg.msgHeight + "," + myMsg.startObjName + "," + myMsg.endObjName + "," + myMsg.msgType + "," + myMsg.msgID + "," + myMsg.startObjID + "," + myMsg.endObjID + "\n";
                    }

                    Image11.ImageUrl = "~/TempImages/" + dl.getImagePath();//dt1.Displayimage1();

                    userUpload = false;

                }
                else if (UploadFromDb)
                {
                    imgserverPath = "";
                    userUpload = false;
                    string filename = Path.GetFileName(Image11.ImageUrl);
                    //Response.Write(Server.MapPath(Image11.ImageUrl));
                    int i = filename.Length;
                    filename = filename.Substring(filename.LastIndexOf('=') + 1);

                    SqlConnection con = new SqlConnection(strcon);
                    con.Open();
                    strcon = "select Image_1 from  SequenceDiagramTb where Id = '" + Convert.ToInt32(filename) + "'";
                    SqlCommand com = new SqlCommand(strcon, con);
                    MemoryStream stream = new MemoryStream();
                    byte[] image = (byte[])com.ExecuteScalar();
                    stream.Write(image, 0, image.Length);
                    Bitmap bitmap = new Bitmap(stream);

                    // to get the directory
                    string root = Server.MapPath("TempImages/" + filename + ".jpg");
                    string folder = root.Remove(root.LastIndexOf('\\') + 1);

                    // Data data = new Data();
                    uniquefile = Server.MapPath("TempImages/" + data.GetUniqueFilename(folder, filename + ".jpg"));

                    data.CleanFiles(folder);
                    bitmap.Save(uniquefile);//filename + ".jpeg")
                    dt.imgPath = uniquefile;//Server.MapPath(GetUniqueFilename(folder,filename+".jpg"));//Server.MapPath(filename + ".jpeg");
                    //}
                    imgserverPath = dt.imgPath;
                    bitmap.Dispose();
                    con.Close();

                    stream.Close();
                    stream.Dispose();

                    Detect_Rectangle dt1 = new Detect_Rectangle(imgserverPath);

                    dt1.button4_Click();
                    physicalPathimg1 = dt1.pathImage1();
                    physicalPathimg11 = dt1.pathImage11();
                    List<Lifelines> objNames = dt1.getLifelines();
                    TextBox1.Text = string.Empty;

                    foreach (Lifelines obj in objNames)
                    {
                        //Response.Write(obj.objID + "\n" + obj.objName);
                        //Response.Write("lifelines: " + obj.objName+","+obj.pointX+","+obj.pointY+","+obj.objWidth+","+obj.objHeight+"\n");
                        // TextBox1.Text = TextBox1.Text
                        str = str + "\n" + obj.objName + "," + obj.pointX + "," + obj.pointY + "," + obj.objWidth + "," + obj.objHeight + "," + obj.objCenter + "," + obj.lifelineHeight + "," + obj.objID + "\n";
                    }

                    Detect_Lines dl = new Detect_Lines(dt1.pathImage1(), dt1.pathImage11(), dt1.getImage1(), dt1.getImage11(), imgserverPath, objNames);
                    dl.detect_H_Solid_Line();

                    allMessages = dl.getMessages();
                    TextBox1.Text = string.Empty;
                    //TextBox1.Text = "Line \n";
                    objNamesMiss = dl.getLifelineMissObj();

                    // objNames.AddRange(objNamesMiss);

                    //   objNamesMiss.AddRange(objNames);
                    foreach (Lifelines obj in objNamesMiss)
                    {
                        //Response.Write(obj.objID + "\n" + obj.objName);
                        //Response.Write("lifelines: " + obj.objName+","+obj.pointX+","+obj.pointY+","+obj.objWidth+","+obj.objHeight+"\n");
                        // TextBox1.Text = TextBox1.Text
                        str2 = str2 + "\n" + obj.objName + "," + obj.pointX + "," + obj.pointY + "," + obj.objWidth + "," + obj.objHeight + "," + obj.objCenter + "," + obj.lifelineHeight + "," + obj.objID + "\n";
                    }
                    Image11.ImageUrl = "~/TempImages/" + dl.getImagePath();//dt1.Displayimage1();
                    //System.IO.File.Delete(physicalPathimg1);
                    //Response.Write(dt1.GetText());//physicalPath.Replace(physicalPath.Substring(0,8),"~").Replace(@"\","/"));
                    UploadFromDb = false;

                }
                else
                {
                    // Image11.Visible = false;
                }
                //TextBox1.Text = str + str2;// +tr;
                //XMI

                extractXMI();
            } catch( Exception ex) {
               // Response.Write(ex.ToString());
            }   
            // Submit.Visible = true;
            // Downloadbtn.Visible = true;
            //System.Threading.Thread.Sleep(1000);
        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            SubmitToDataBase();
            //  Label3.Text = ;//"<TABLE cellspacing=0 cellpadding=0 border=1 width=200><TR><TD bgcolor=#000066 width=" + 100 + "%> </TD><TD bgcolor=#FFF7CE> _</TD></TR></TABLE>";
            //Label3.Text = "<a href="http://localhost:63320/Default.aspx/Label3.Text"</a>"; 


        }
        public void extractXMI()
        {
            
          
            Detect_Rectangle dt1 = new Detect_Rectangle(imgserverPath);
            dt1.button4_Click();

            List<Lifelines> objNames = dt1.getLifelines().OrderBy(o => o.pointX).ToList();
            objNames.AddRange(objNamesMiss);
            // 
            objNames= objNames.OrderBy(o => o.pointX).ToList();

            List<Messages> allMessages1 = allMessages.OrderBy(o => o.msgStartY).ToList();

           // List<Lifelines> objNamesMiss = objNamesMiss getLifelineMissObj();

            

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            string xmlnsNS = "href://org.omg/UML/1.3";
            string strfilename = "xmi file";
            string root = Server.MapPath("XmiFiles/" + strfilename + ".xml");
            string folder = root.Remove(root.LastIndexOf('\\') + 1);
            Data data = new Data();
            // Data data = new Data();
            string strUniquefilename = "XmiFiles/" + data.GetUniqueFilename(folder, strfilename + ".xml");
            string uniquefile = Server.MapPath(strUniquefilename);
            strdowload = strUniquefilename;
            forceDownload = true;
            //@"C:\Users\Munir\Documents\Visual Studio 2012\Projects\UML_Project_1\UML_Project_1\TempImages\employees.xml"
            using (XmlWriter writer = XmlWriter.Create(uniquefile, settings))
            {

                writer.WriteStartDocument();
                writer.WriteComment("This file is generated by the program.");
                writer.WriteStartElement("XMI");
                writer.WriteAttributeString("xmi.version", "1.1");
                writer.WriteAttributeString("xmlns", "UML", null, xmlnsNS);

                // HEADER
                writer.WriteStartElement("XMI.header");
                writer.WriteStartElement("XMI.metamodel");
                writer.WriteAttributeString("xmi.name", "UML");
                writer.WriteAttributeString("xmi.version", "1.3");
                writer.WriteEndElement();
                writer.WriteEndElement();
                // end header
                writer.WriteStartElement("XMI.content");
                writer.WriteStartElement("UML", "Model", null);
                writer.WriteAttributeString("xmi.id", "UMLProject.1");

                writer.WriteStartElement("UML", "Namespace.ownedElement", null);

                writer.WriteStartElement("UML", "Model", null);
                writer.WriteAttributeString("xmi.id", "UMLModel.2");
                writer.WriteAttributeString("name", "Design Model1"); // name of the rect
                writer.WriteAttributeString("visibility", "public");
                writer.WriteAttributeString("isSpecification", "false");
                writer.WriteAttributeString("namespace", "UMLProject.1");
                writer.WriteAttributeString("isRoot", "false");
                writer.WriteAttributeString("isLeaf", "false");
                writer.WriteAttributeString("isAbstract", "false");

                writer.WriteStartElement("UML", "Namespace.ownedElement", null);

                writer.WriteStartElement("UML", "Collaboration", null);
                writer.WriteAttributeString("xmi.id", "UMLCollaborationInstanceSet.3");
                writer.WriteAttributeString("name", "CollaborationInstanceSet1"); // name of the rect
                writer.WriteAttributeString("visibility", "public");
                writer.WriteAttributeString("isSpecification", "false");

                // if line interaction 
                writer.WriteStartElement("UML", "Collaboration.interaction", null);
                writer.WriteStartElement("UML", "Interaction", null);
                writer.WriteAttributeString("xmi.id", "UMLInteractionInstanceSet.4");
                writer.WriteAttributeString("name", "InteractionInstanceSet1"); // name of the rect
                writer.WriteAttributeString("visibility", "public");
                writer.WriteAttributeString("isSpecification", "false");
                writer.WriteAttributeString("context", "UMLCollaborationInstanceSet.3");

                // writer.WriteStartElement("UML", "Interaction.message", null);

                XmiWriter xm = new XmiWriter();

                // call1.Length;
                // foreach (long i=0; i<call1.Length;i++) //Lifelines obj in objNames)
                // {
                // Detect_Lines dl = new Detect_Lines(physicalPathimg1, physicalPathimg11, dt1.getImage1(), dt1.getImage11(), imgserverPath, objNames);
                // dl.detect_H_Solid_Line();
               
                String tempVar = null;
                string str1, str2, regx1, regx2;
                int id = 99;
                foreach (Messages myMsg in allMessages1)
                {
                    // generate id for the auto compeletion object
                    ////if (myMsg.endObjID == "Unknown" && myMsg.startObjID != "Unknown")
                    ////{
                    ////    ++id;
                    ////    myMsg.endObjID =  "UMLObject."+id;
                    ////}
                    ////else  if (myMsg.endObjID != "Unknown" && myMsg.startObjID == "Unknown")
                    ////{
                    ////    id++;
                    ////    myMsg.startObjID = "UMLObject."+id;
                    ////}  
                               
                    if (myMsg.msgType == "CallAction")
                    {
                        try 
                        { 
                            tempVar = myMsg.msgID.Substring(myMsg.msgID.LastIndexOf('.') + 1); //
                            str1 = myMsg.msgName.Substring(0, myMsg.msgName.IndexOf(':'));
                            regx1 = Regex.Replace(str1, "[^a-zA-Z]", " ");
                            str2 = myMsg.msgName.Substring(myMsg.msgName.LastIndexOf(':') + 1);
                            regx2 = Regex.Replace(str2, "[^a-zA-Z]", " ");
                            if (regx1.Length<=3)
                            {

                            }
                            else
                            {
                                regx2 = ";" + regx2;
                            }
                            myMsg.msgName = regx1 + regx2;
                        }
                        catch { }
                        /*if (myMsg.msgName.Substring(myMsg.msgName.LastIndexOf(':') + 1) != null || myMsg.msgName.Substring(myMsg.msgName.LastIndexOf(':') + 1) == null)
                        {
                            myMsg.msgName = myMsg.msgName.Substring(myMsg.msgName.LastIndexOf(':') + 1);
                        }*/

                        xm.CallAction(writer, myMsg.msgID, myMsg.msgName, myMsg.startObjID, myMsg.endObjID, "UMLCallAction." + (Int32.Parse(tempVar) + 1));
                    }
                    else if (myMsg.msgType == "ReturnAction")
                    {
                        try
                        {
                            tempVar = myMsg.msgID.Substring(myMsg.msgID.LastIndexOf('.') + 1);

                            str1 = myMsg.msgName.Substring(0, myMsg.msgName.IndexOf(':'));
                            regx1 = Regex.Replace(str1, "[^a-zA-Z]", " ");
                            str2 = myMsg.msgName.Substring(myMsg.msgName.LastIndexOf(':') + 1);
                            regx2 = Regex.Replace(str2, "[^a-zA-Z]", " ");
                            if (regx1.Length<=3) {
                             
                            } else {
                                regx2 = ";" + regx2;
                            }
                           
                            myMsg.msgName = regx1 + regx2;
                        }
                        catch { }
                       /* if (myMsg.msgName.Substring(myMsg.msgName.LastIndexOf(':') + 1) != null || myMsg.msgName.Substring(myMsg.msgName.LastIndexOf(':') + 1) == null)
                        {
                            myMsg.msgName = myMsg.msgName.Substring(myMsg.msgName.LastIndexOf(':') + 1);
                        }*/
                        xm.ReturnAction(writer, myMsg.msgID, myMsg.msgName, myMsg.startObjID, myMsg.endObjID, "UMLReturnAction." + (Int32.Parse(tempVar) + 1));

                    }
                    else if (data.SearchSubstring(myMsg.msgName.ToLower(),"create")) // search for create in string
                    {
                        try{
                        tempVar = myMsg.msgID.Substring(myMsg.msgID.LastIndexOf('.') + 1);
                        str1 = myMsg.msgName.Substring(0, myMsg.msgName.IndexOf(':'));
                        regx1 = Regex.Replace(str1, "[^a-zA-Z]", " ");
                        str2 = myMsg.msgName.Substring(myMsg.msgName.LastIndexOf(':') + 1);
                        regx2 = Regex.Replace(str2, "[^a-zA-Z]", " ");
                        if (regx1.Length<=3)
                        {

                        }
                        else
                        {
                            regx2 = ";" + regx2;
                        }
                        myMsg.msgName = regx1 + regx2;
                        }
                        catch { }
                       /* if (myMsg.msgName.Substring(myMsg.msgName.LastIndexOf(':') + 1) != null || myMsg.msgName.Substring(myMsg.msgName.LastIndexOf(':') + 1) == null)
                        {
                            myMsg.msgName = myMsg.msgName.Substring(myMsg.msgName.LastIndexOf(':') + 1);
                        }*/
                            xm.CreateAction(writer, myMsg.msgID, myMsg.msgName, myMsg.startObjID, myMsg.endObjID, "UMLCreateAction." + (Int32.Parse(tempVar) + 1));
                        

                    }
                    else if (data.SearchSubstring(myMsg.msgName.ToLower(), "destroy") || data.SearchSubstring(myMsg.msgName.ToLower(), "delete"))
                    {
                        try{
                        
                        tempVar = myMsg.msgID.Substring(myMsg.msgID.LastIndexOf('.') + 1);
                        str1 = myMsg.msgName.Substring(0, myMsg.msgName.IndexOf(':'));
                        regx1 = Regex.Replace(str1, "[^a-zA-Z]", " ");
                        str2 = myMsg.msgName.Substring(myMsg.msgName.LastIndexOf(':') + 1);
                        regx2 = Regex.Replace(str2, "[^a-zA-Z]", " ");
                        if (regx1.Length<=3)
                        {

                        }
                        else
                        {
                            regx2 = ";" + regx2;
                        }
                        myMsg.msgName = regx1 + regx2;
                        }
                        catch { }
                        /*if (myMsg.msgName.Substring(myMsg.msgName.LastIndexOf(':') + 1) != null || myMsg.msgName.Substring(myMsg.msgName.LastIndexOf(':') + 1) == null)
                        {
                            myMsg.msgName = myMsg.msgName.Substring(myMsg.msgName.LastIndexOf(':') + 1);
                        }*/
                            xm.DestroyAction(writer, myMsg.msgID, myMsg.msgName, myMsg.startObjID, myMsg.endObjID, "UMLDestroyAction." + (Int32.Parse(tempVar) + 1));
                        
                    }
                    else if (myMsg.msgType == "SendAction")
                    {
                        try
                        {
                            tempVar = myMsg.msgID.Substring(myMsg.msgID.LastIndexOf('.') + 1);
                            str1 = myMsg.msgName.Substring(0, myMsg.msgName.IndexOf(':'));
                            regx1 = Regex.Replace(str1, "[^a-zA-Z]", " ");
                            str2 = myMsg.msgName.Substring(myMsg.msgName.LastIndexOf(':') + 1);
                            regx2 = Regex.Replace(str2, "[^a-zA-Z]", " ");
                            if (regx1.Length <= 3)
                            {

                            }
                            else
                            {
                                regx2 = ";" + regx2;
                            }
                            myMsg.msgName = regx1 + regx2;
                        }
                        catch { }
                        /*if (myMsg.msgName.Substring(myMsg.msgName.LastIndexOf(':') + 1) != null || myMsg.msgName.Substring(myMsg.msgName.LastIndexOf(':') + 1) == null)
                        {
                            myMsg.msgName = myMsg.msgName.Substring(myMsg.msgName.LastIndexOf(':') + 1);
                        }*/
                        
                            xm.SendAction(writer, myMsg.msgID, myMsg.msgName, myMsg.startObjID, myMsg.endObjID, "UMLSendAction." + (Int32.Parse(tempVar) + 1));
                        
                    }

                }

                writer.WriteEndElement();
                writer.WriteEndElement();
                // end line interaction

                // objects
                writer.WriteStartElement("UML", "Namespace.ownedElement", null);
                Boolean boolclass = false;
                int countID = 99;
                string classid = null;
                List<ClassifierType> classifierType = new List<ClassifierType>();
                int i = 0;
                foreach (Lifelines obj in objNames)
                {
                    string msg1 = "";
                    string msg2 = "";
                    
                    foreach (Messages myMsg in allMessages1)
                    {
                        if (myMsg.startObjID == obj.objID)
                        {
                            msg2 = msg2 + myMsg.msgID + " ";
                        }
                        if (myMsg.endObjID == obj.objID)
                        {
                            msg1 = msg1 + myMsg.msgID + " ";
                        }
                        if (obj.objName == "Unknown" && myMsg.endObjName=="Unknown") {
                           // msg1 = msg1 + myMsg.msgID + " ";   
                        }
                    }
                    try
                    {
                   // tempVar = myMsg.msgID.Substring(myMsg.msgID.LastIndexOf('.') + 1);
                    str1 = obj.objName.Substring(0, obj.objName.IndexOf(':'));
                    regx1 = Regex.Replace(str1, "[^a-zA-Z]", " ");
                    str2 = obj.objName.Substring(obj.objName.LastIndexOf(':') + 1);
                    regx2 = Regex.Replace(str2, "[^a-zA-Z]", " ");
                        if (regx1.Length <= 3)
                        {

                        }
                        else
                        {
                            //regx2 = ";" + regx2;
                        }
                        obj.objName = regx1;// +regx2;
                        if (regx2.Length>=3 ) 
                        {
                            Boolean classExist = false;
                            if (classifierType.Count > 0)
                            {
                                foreach (ClassifierType ct in classifierType)
                                {
                                    if (regx2 == ct.className)
                                    {
                                        classid = ct.classID;
                                        boolclass = true;
                                        classExist = true;
                                    }

                                }
                                if (classExist == false)
                                {
                                    ++countID;
                                    ClassifierType type = new ClassifierType(regx2, classid = "UMLClass." + countID);
                                    classifierType.Add(type);
                                    boolclass = true;
                                }
                            }
                            else {
                                ++countID;
                                ClassifierType type = new ClassifierType(regx2, classid = "UMLClass." + countID);
                                classifierType.Add(type);
                                boolclass = true;
                            }
                               
                           /* foreach(ClassifierType ct in classifierType){
                                if (regx2 == ct.className)
                                {
                                    classid = ct.classID;  
                                    boolclass = true;
                                }
                                
                    
                            }*/
                           
                        }
                    } catch { }
                   /* if (obj.objName.Substring(obj.objName.LastIndexOf(':') + 1) != null || obj.objName.Substring(obj.objName.LastIndexOf(':') + 1) == null)
                    {
                        obj.objName = obj.objName.Substring(obj.objName.LastIndexOf(':') + 1);
                    }*/
                    xm.getObject(writer, obj.objName, msg2, msg1, obj.objID, 15 + i,boolclass, classid);
                    i = i + 2;
                    boolclass = false;
                    
                }

                writer.WriteEndElement();
                writer.WriteEndElement();
                //class name
                foreach (ClassifierType ct in classifierType)
                {
                    xm.classNameAction(writer, ct.className, ct.classID);
                }

                // dash line / Return message
                string typemsg = null;
                string typename = null;
                string asyntypemsg = null;
                string asyntypename = null;
                bool retn = false, asynch = false; ;
                foreach (Messages myMsg in allMessages1)
                {
                    if (myMsg.msgType == "ReturnAction")
                    {
                        typemsg = typemsg + myMsg.msgID + " ";
                        typename = "return";
                        retn = true;
                    }
                    if (myMsg.msgType == "SendAction")
                    {
                        asyntypemsg = asyntypemsg + myMsg.msgID + " ";
                        asyntypename = "asynchronous";
                        asynch = true;
                    }
                }
                if (retn == true)
                {
                    xm.StereotypeAction(writer, typename, typemsg, i);
                }
                if (asynch == true)

                {
                    xm.StereotypeAction(writer, asyntypename, asyntypemsg, i++);
                }
                // end dash line

                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndElement();

                // Diagram
                writer.WriteStartElement("UML", "Diagram", null);
                writer.WriteAttributeString("xmi.id", "UMLSequenceDiagram.5");
                writer.WriteAttributeString("name", "SequenceDiagram1"); // name of the rect
                writer.WriteAttributeString("diagramType", "SequenceDiagram");
                writer.WriteAttributeString("toolName", "Rational Rose 98");
                writer.WriteAttributeString("owner", "UMLInteractionInstanceSet.4");

                writer.WriteStartElement("UML", "Diagram.element", null);
                //
                // Diagram Element 
                int tempCropx = 0;
                foreach (Lifelines obj in objNames)
                {
                    tempCropx = obj.pointX + tempCropx + obj.objWidth;
                    string DiagramxmID = "UMLSeqObjectView.6";
                    string style = "LineColor.Red=128,LineColor.Green=0,LineColor.Blue=0,FillColor.Red=255,FillColor.Green=255,FillColor.Blue=185,Font.Red=0,Font.Green=0,Font.Blue=0,Font.FaceName=Tahoma,Font.Size=8,Font.Bold=0,Font.Italic=0,Font.Underline=0,Font.Strikethrough=0,AutomaticResize=0,";
                    xm.DiagramElement(writer, DiagramxmID, tempCropx + 20 + ", " + (obj.pointY + 650) + ", " + obj.objWidth + ", " + (obj.lifelineHeight + 700) + ",", style, obj.objID);
                }
                // arrow 
                tempCropx = 0;
                int tempCropy = 0;
                int j = 1;
                foreach (Messages myMsg in allMessages1)
                {

                    // tempCropx = myMsg.msgStartX + tempCropx;
                    tempCropy = tempCropy + 50;
                    string DiagramxmID = "UMLSeqStimulusView.2" + j;
                    string style = "Message,SQN=" + j + ",";
                    // string UMLStimulusID = "UMLStimulus.26";
                    xm.DiagramElement(writer, DiagramxmID, myMsg.msgStartX + 20 + ", " + (myMsg.msgStartY + tempCropy + 650) + ", " + myMsg.msgWidth + ", " + (myMsg.msgHeight + 0) + ",", style, myMsg.msgID);
                    j++;
                }
                // xm.DiagramElement(writer, "UMLSeqObjectView.7", "1609, 765, 531, 1050,");
                /*writer.WriteStartElement("UML", "DiagramElement", null);
                writer.WriteAttributeString("xmi.id", "UMLSeqObjectView.6");
                writer.WriteAttributeString("geometry", "313, 537, 555, 1050,");
                writer.WriteAttributeString("style", "LineColor.Red=128,LineColor.Green=0,LineColor.Blue=0,FillColor.Red=255,FillColor.Green=255,FillColor.Blue=185,Font.Red=0,Font.Green=0,Font.Blue=0,Font.FaceName=Tahoma,Font.Size=8,Font.Bold=0,Font.Italic=0,Font.Underline=0,Font.Strikethrough=0,AutomaticResize=0,");
                writer.WriteAttributeString("subject", "UMLObject.7");

                 writer.WriteEndElement();*/
                writer.WriteEndElement();
                writer.WriteEndElement();

                writer.WriteEndDocument();

                writer.Flush();
                writer.Close();
            }


        }

        protected void Downloadbtn_Click(object sender, EventArgs e)
        {
           // private void DownloadFile( string fname, bool forceDownload )
            //{
            if(forceDownload ==true)
            {
            string fname = strdowload;
           
              string path = MapPath( fname );
              string name = Path.GetFileName( path );
              string ext = Path.GetExtension( path );
              string type = "";
              // set known types based on file extension  
              if ( ext != null )
              {
                switch( ext.ToLower() )
                {
                case ".htm":
                case ".html":
                case ".xml":
                  type = "text/HTML";
                  break;
  
                case ".txt":
                  type = "text/plain";
                  break;
     
                case ".doc":
                case ".rtf":
                  type = "Application/msword";
                  break;
                }
              }
              if ( forceDownload )
              {
                Response.AppendHeader( "content-disposition",
                    "attachment; filename=" + name );
              }
              if ( type != "" )   
                Response.ContentType = type;
              Response.WriteFile( path );
              Response.End();    
            }
        }

        
    }
}//