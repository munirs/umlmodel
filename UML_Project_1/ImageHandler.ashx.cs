﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace UML_Project_1
{
    /// <summary>
    /// Summary description for ImageHandler
    /// </summary>
    public class ImageHandler : IHttpHandler
    {
        string strcon = "Data Source=.\\sqlexpress;Initial Catalog=TestDB;Integrated Security=True";//ConfigurationManager.AppSettings["TestDBConnectionString"].ToString();

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                string imageid = context.Request.QueryString["ImID"];
                SqlConnection connection = new SqlConnection(strcon);
                connection.Open();
                SqlCommand command = new SqlCommand("select Image_1 from  SequenceDiagramTb where Id=" + imageid, connection);

                SqlDataReader dataread = command.ExecuteReader();

                dataread.Read();

                context.Response.BinaryWrite((Byte[])dataread[0]);

                connection.Close();
                context.Response.End();
            }
            catch { }
            //context.Response.ContentType = "text/plain";
            //context.Response.Write("Hello World");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}